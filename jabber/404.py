#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

print "content-type: text/html"
print

file=open('/var/www/jabber/template')
html=file.read()
print html % "<p><img src='/images/404.jpg' width='215' height='54' border='0' usemap='#Map'>\
<map name='Map'> \
</map>\
</p>\
<p><b><font size='4'> Page introuvable</font></b></p>\
<p> La page que vous voulez consulter est introuvable.</p>\
<p>Bon.</p>\
<p>On va pas en faire un drame non plus, hein ? <br>\
C'est p't&ecirc;t de notre faute si elle n'est pas l&agrave;, mais vous &ecirc;tes \
venu ici tout seul. On ne vous a pas forc&eacute;. Donc c'est votre probl&egrave;me.\
Pas le n&ocirc;tre.</p>\
<p>... et puis, en y r&eacute;fl&eacute;chissant un peu, est-ce que vous vouliez \
<i>vraiment</i> consulter cette page ? Est-ce que vous n'avez pas cliqu&eacute; \
sur un lien, comme &ccedil;a, au hasard ? Est-ce que vous n'avez pas tap&eacute; \
votre URL avec des moufles ? Est-ce que vous savez seulement taper sur un \
clavier ?</p> \
<p></p>"
