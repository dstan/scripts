#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

import auth
import sys
import time

delattr(sys, 'argv')

#if len(sys.argv) < 2 and False:
#    print "Give me a mac !"
#    sys.exit(1)

# Machine à s'authentifier (cerveaulent)
#p=(('Calling-Station-Id', sys.argv[1]),)

auth.instantiate(())

p=(
    # Mac qui existe:
    #('Calling-Station-Id', '1c:75:08:16:dc:03'),
    # Mac qui n'existe pas, mais en se mettant sur la chambre d'un adhérent
    # possédant une machine en mac=<automatique>
    ('Calling-Station-Id', '1c:75:08:16:dc:04'),
    #('User-Name', 'moo-torola'),
    ('NAS-Identifier', 'batg-0'),
    ('NAS-Port', '20'),
)

print repr(auth.authorize_fil(p))
print "wait for 3s, tu peux aller crasher le serveur pg ou ldap"
sys.stdout.flush()

time.sleep(3)

print repr(auth.post_auth_fil(p))
