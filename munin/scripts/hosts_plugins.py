#!/usr/bin/env python
# -!- mode: python; coding: utf-8 -!-
# Correspondance hôte <-> plugin munin custom
#
# Copyright © 2009 Nicolas Dandrimont <Nicolas.Dandrimont@crans.org>
#
# Licence: MIT

import sys as _sys
_sys.path.append("/usr/scripts/gestion")
del _sys

import annuaires_pg as annuaires
import config

__all__ = ["hosts_plugins", "munin_fw", "cron_plugins", "hosts_crons"]

# Lignes cron correspondant aux plugins customs
cron_plugins = {
    # plugin: (utilisateur, commande),
    # %s est remplacé par le lien dans /etc/munin/plugins
    "audimat": ("root", "nice -n 14 %s generate 2> /dev/null"),
    "batiments": ("root", "nice -n 14 %s fichier 2> /dev/null"),
    "stats-ip": ("root", "nice -n 14 %s fichier 2> /dev/null"),
    "wiki_pages": ("www-data", "%s fichier 2> /dev/null"),
    }

# Hôtes pour lesquels le firewall est muninisé
munin_fw = sorted(["zamok", "komaz", "sable", "odlyd"])

# Les clés correspondent au nom du serveur, ou à un des groupes (bcfg2)
# possédé par le serveur (cf gestion/config/services.py )
hosts_plugins = {
        "vo": {
                "coretemp": "/usr/scripts/munin/coretemp",
        },
        "zamok": {
                "coretemp": "/usr/scripts/munin/coretemp",
                "canon_compteur": "canon_",
                "canon_papier": "canon_",
                "canon_ton-p": "canon_",
                "webalizer_intranet": "webalizer_",
                "webalizer_perso": "webalizer_",
                "who": "who",
                "whokfet": "whokfet",
        },
        "vert": {
                "slapd_bdb_cache_pages": "slapd_bdb_cache_",
                "slapd_bdb_cache_percent": "slapd_bdb_cache_",
        },
        "komaz": {
                "coretemp": "/usr/scripts/munin/coretemp",
                "machines": "machines",
                "if_sixxs2": "/usr/share/munin/plugins/if_",
                "if_err_sixxs2": "/usr/share/munin/plugins/if_err_",
                "if_crans.2": "/usr/share/munin/plugins/if_",
                "if_err_crans.2": "/usr/share/munin/plugins/if_err_",
                "if_crans.3": "/usr/share/munin/plugins/if_",
                "if_err_crans.3": "/usr/share/munin/plugins/if_err_",
                "if_crans.21": "/usr/share/munin/plugins/if_",
                "if_err_crans.21": "/usr/share/munin/plugins/if_err_",
        },
        "odlyd": {
                "if_sixxs": "/usr/share/munin/plugins/if_",
                "if_err_sixxs": "/usr/share/munin/plugins/if_err_",
                "if_crans.2": "/usr/share/munin/plugins/if_",
                "if_err_crans.2": "/usr/share/munin/plugins/if_err_",
                "if_crans.3": "/usr/share/munin/plugins/if_",
                "if_err_crans.3": "/usr/share/munin/plugins/if_err_",
                "if_crans.21": "/usr/share/munin/plugins/if_",
                "if_err_crans.21": "/usr/share/munin/plugins/if_err_",
        },
        "sable": {
                "coretemp": "/usr/scripts/munin/coretemp",
                "bind9_rndc": "/usr/share/munin/plugins/bind9_rndc",
        },
        "dhcp": {
                "dhcpd3": "/usr/share/munin/plugins/dhcpd3",
        },
        "isc": {
                "dhcpd3": "/usr/share/munin/plugins/dhcpd3",
        },
        "charybde": {
                "coretemp": "/usr/scripts/munin/coretemp",
                "bind9_rndc": "/usr/share/munin/plugins/bind9_rndc",
        },
        "routeur": {
                "bind9_rndc": "/usr/share/munin/plugins/bind9_rndc",
        },
        "thot": {
                "coretemp": "/usr/scripts/munin/coretemp",
                "wifi_auth": "wifi_auth",
        },
        "xmpp": {
                "jabber": "jabber",
                "ejabberd_connections": "ejabberd_",
                "ejabberd_users": "ejabberd_",
                "ejabberd_registrations": "ejabberd_",
                "ejabberd_statuses": "ejabberd_",
                "ejabberd_memory": "ejabberd_",
                "ejabberd_threads": "ejabberd_",
                "ejabberd_usersindays": "ejabberd_",
                "ejabberd_uptime": "ejabberd_",
        },
        "niomniom": {
                "wiki_pages": "wiki_pages",
                "wiki_themes": "wiki_themes",
                "wiki_users": "wiki_users",
        },
        "munin-server": {
                "audimat": "audimat",
                "batiments": "batiments",
                "munin": "munin",
                "stats-ip": "stats-ip",
                "ap_states": "/usr/scripts/wifi_new/munin.py",
                "wifi_clients": "/usr/scripts/wifi_new/munin.py",
                "wifi_authorized": "/usr/scripts/wifi_new/munin.py",
                "wifi_uptime": "/usr/scripts/wifi_new/munin.py",
                "wifi_idle": "/usr/scripts/wifi_new/munin.py",
                "wifi_channels": "/usr/scripts/wifi_new/munin.py",
                "hp_consommables": "consommables_hp",
# La suite plus bas...
        },
        "news": {
                "innd_total": "innd_total",
                "innd_last": "innd_last",
                "innd_connects": "innd_connects",
        },
        "zbee": {
        'nfsd' : '/usr/share/munin/plugins/nfsd',
    }
    }

# Plugins Cr@ns systématiquement rajoutés
general_plugins = {
    "check_ntp": "check_ntp",
    'apt': 'apt',
}

####
# Trucs spécifiques à munin.crans.org
# Onduleur
for mge_truc in ["batt", "hygro", "intensite", "temp", "temps", "tension"]:
    hosts_plugins["munin-server"]["mge_%s" % mge_truc] = "mge_%s" % mge_truc

# Stats de peuplement des bâtiments
for bat in annuaires.bat_switchs:
    if bat not in ('v',):
        hosts_plugins["munin-server"]["stats-batiment_%s" % bat] = "stats-batiment_"

# Ping de tous les switches de bâtiments
for switch in annuaires.all_switchs(hide=[]):
    switch = switch.replace('-', '_')
    hosts_plugins["munin-server"]["ping_%s" % switch] = "ping_bat_"

# On rajoute les stats de connexion
for host in hosts_plugins.keys():
    hosts_plugins[host].update({"sockets": "sockets"})

#########
# Muninisation des firewalls
#
for host in munin_fw:
    hosts_plugins[host].update({
        "iptables_filter": "iptables_",
        "iptables_mangle": "iptables_",
        "iptables_nat": "iptables_",
        "ipset_macip":"/usr/scripts/munin/ipset",
        "ipset":"/usr/scripts/munin/ipset",
        })

# format de la ligne cron
CRON_FORMAT = "%s-%s/%s * * * * %s %s"
# fréquence
FREQ = 15
# lignes cron par hôte
hosts_crons = {}
for host, plugins in hosts_plugins.items():
    minute = 1
    hosts_crons[host] = []
    for path, plugin in plugins.items():
        if plugin in cron_plugins:
            path = "/etc/munin/plugins/" + path
            user, line = cron_plugins[plugin]
            hosts_crons[host].append(CRON_FORMAT % (minute,
                                                    60 - FREQ + minute,
                                                    FREQ,
                                                    user,
                                                    line % path))
            minute = (minute + 4) % FREQ

    if not hosts_crons[host]:
        del hosts_crons[host]
