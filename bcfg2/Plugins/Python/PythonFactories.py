#!/usr/bin/env python2.7
# -*- mode: python; coding: utf-8 -*-
"""Ce module est prévu pour héberger des factories, stockant toute
instance d'un fichier Python déjà compilé."""

class PythonFileFactory(object):
    """Cette Factory stocke l'ensemble des fichiers Python déjà instanciés.
    Elle garantit entre autre leur unicité dans le fonctionnement du plugin"""

    #: Stocke la liste des instances avec leur chemin absolu.
    files = {}

    @classmethod
    def get(cls, path):
        """Récupère l'instance si elle existe, ou renvoit None"""
        return cls.files.get(path, None)

    @classmethod
    def record(cls, path, instance):
        """Enregistre l'instance dans la Factory"""
        cls.files[path] = instance

    @classmethod
    def flush_one(cls, path):
        """Vire une instance du dico"""
        instance_to_delete = cls.files.pop(path, None)
        del instance_to_delete

    @classmethod
    def flush(cls):
        """Vire toutes les instances du dico"""
        for path in cls.files.keys():
            cls.flush_one(path)
