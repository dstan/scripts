#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

""" Envoie un mail avec la liste des serveurs qui ne sont pas synchro avec bcfg2.
    
    Si appelé sans l'option ``--mail``, affiche le résultat sur stdout.
    
    N'affiche que ceux qui datent d'aujourd'hui, hier ou avant-hier
    (pour ne pas avoir les vieux serveurs qui traînent).
   
    """

from __future__ import print_function

import sys
import time
import datetime
import subprocess

unjour = datetime.timedelta(1)

aujourdhui = datetime.date(*time.localtime()[:3])
hier = aujourdhui - unjour
avanthier = aujourdhui - unjour*2

def get_dirty():
    """Récupère les hosts dirty récents."""
    proc = subprocess.Popen(["/usr/sbin/bcfg2-reports", "-d"], stdout=subprocess.PIPE, stderr=subprocess.PIPE) # | sort
    out, err = proc.communicate()
    if err:
        print(err, file=sys.stderr)
    if proc.returncode != 0:
        return (False, out)
    out = [l for l in out.split("\n") if any([date.strftime("%F") in l for date in [aujourdhui, hier, avanthier]])]
    out.sort()
    return True, "\n".join(out)

if __name__ == "__main__":
    success, hosts = get_dirty()
    if not success:
        print(hosts, file=sys.stderr)
        exit(1)
    debug = "--debug" in sys.argv
    if "--mail" in sys.argv:
        if hosts != "":
            import utils.sendmail
            utils.sendmail.sendmail("root@crans.org", "roots@crans.org", u"Serveurs non synchronisés avec bcfg2", hosts, more_headers={"X-Mailer" : "bcfg2-reports"}, debug=debug)
        elif debug:
            print("Empty content, no mail sent")
    else:
        print(hosts, end="")
