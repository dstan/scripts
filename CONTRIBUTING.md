## Conventions de codage

 * On essaie de respecter le plus possible <a href="http://www.python.org/dev/peps/pep-0008/">PEP8</a>.
 * Utiliser exclusivement des espaces pour indenter ses blocs : **4** espaces = **une** indentation.
 * Respecter l'<a href="http://nedbatchelder.com/text/unipain/unipain.html#1">unicode sandwich</a>
 * Pour les codages d'entrées/sorties, privilégier l'utf-8
 * Les scripts python doivent être encodés en utf-8
 * On utilise un interprêteur personnalisé `/usr/scripts/python.sh` pour l'environnement
 * Certains éditeurs n'apprécient pas, on force donc la coloration en code python
 
Un header complet ressemblera ainsi à :
`
 #!/bin/bash /usr/scripts/python.sh
 # -*- mode: python; coding: utf-8 -*-
`
On omettra le shabang lorsque le script ne possède pas de fonction main, et
n'est pas exécutable.

## python.sh
Les fichiers python marqués comme exécutables doivent posséder le shabang
`#!/bin/bash /usr/scripts/python.sh`. python.sh se charge d'appeler `testing.sh`,
si besoin, mais également de régler l'environnement `PYTHONPATH`.

Ainsi, il est inutile de manipuler soi-même `sys.path` depuis les scripts python,
cela est même déconseillé, car cela ne permet pas de travailler sur une copie
de test du dépôt ! Faîtes tous vos imports depuis la racine du dépôt
(par exemple `import gestion.affichage`).

Il est également possible de démarrer une session ipython avec l'environnement
de test défini par `testing.sh`, à l'aide de la commande suivante :

`./python.sh ipython `


Un fichier executable devrait également posséder une section
`if __name__ == '__main__':`

## testing.sh

Ce script à la racine déclare les variables d'environnement de debug à activer.
Faîtes-y un tour pour activer désactiver ce que vous souhaitez tester !

## Configuration d'une copie de test sur vo

Pour développer un nouveau script, la plupart du temps, il n'est pas nécessaire
de le faire en production. Une base `ldap` et une base pg de test sont
disponibles sur vo.

Sur vo, cloner le dépôt `/usr/scripts` et `/usr/scripts/lc_ldap` :

`
dstan@vo ~ % git clone https://gitlab.crans.org/nounous/scripts.git
Cloning into 'scripts'...
remote: Counting objects: 28404, done.
remote: Compressing objects: 100% (9098/9098), done.
remote: Total 28404 (delta 20003), reused 26928 (delta 18970)
Receiving objects: 100% (28404/28404), 6.89 MiB | 11.34 MiB/s, done.
Resolving deltas: 100% (20003/20003), done.
dstan@vo ~ % cd scripts
dstan@zamok ~/scripts (git)-[scripts/master] % git clone https://gitlab.crans.org/nounous/lc_ldap.git
Cloning into 'lc_ldap'...
remote: Counting objects: 1839, done.
remote: Compressing objects: 100% (663/663), done.
remote: Total 1839 (delta 1211), reused 1775 (delta 1170)
Receiving objects: 100% (1839/1839), 381.95 KiB, done.
Resolving deltas: 100% (1211/1211), done.
`
Vous avez maintenant un dépôt utilisable, vous pouvez le vérifier en faisant un
petit `whos.py` (par exemple) :

`
1 dstan@vo ~/scripts (git)-[scripts/master] % cd ..
1 dstan@vo ~ % scripts/gestion/whos.py aid=3570
Dépôt custom. PYTHONPATH: /home/d/dstan/scripts
Résultats trouvés parmi les adhérents :
aid=3570 [……]
`
Noter la présence de la ligne "Dépôt custom" qui indique la racine du dépôt
scripts courante. Ce mode "custom" importe également le fichier `testing.sh`
définissant un certain nombre de variable d'environnement. Il est par exemple
possible de modifier `DBG_MAIL` pour changer le destinataire de tous les mails
générés, afin de ne pas spammer de véritable adhérents.

## Configuration d'une copie de test locale

To be continued.

## Gestion des secrets
Certains scripts nécessitent des mots de passes ou autres identifiants secrets
qui ne peuvent être inclus dans le code python (qui est gitté et donc public).
Pour cela, on fait en général appel à un module appelé `secrets`. En production,
les données sont stockées dans `/etc/crans/secrets`, mais il est formellement
interdit de modifier `sys.path` ou de faire d'autres hacks pour y accéder.
Utilisez impérativement, le nouveau module `secrets_new`, celui-ci se charge
de gérer des acls plus fins ainsi qu'un environnement de test. On l'utilise de
la manière suivante :
`
    import gestion.secrets_new as secrets
    ldap_password = secrets.get("ldap_password")
    ldap_auth_dn = secrets.get("ldap_auth_dn")
`

(À terme, le module `secrets_new` sera renommé en `secrets`)## testing.sh

La racine du dépôt contient un fichier nommé `testing.sh` contenant les
réglages de test à appliquer aux scripts lancés. Ce fichier est automatiquement
lu, pour peu que le dépôt cloné ne soit pas enregistré dans `/usr/scripts` !

Par défaut, `testing.sh` définit de nombreuses variables d'environnement, qui
forcent les scripts à travailler en mode test, c'est-à-dire, sans toucher
à la vraie base ldap, ni pgsql, mais en utilisant les bases de test sur vo.
Modifiez-le au besoin (il est commenté !), notamment si vous voulez produire
un environnement de test sur votre machine personnelle, plutôt que sur vo.


## Envoi de mail
Le système de mail a été unifié, afin d'éviter les erreurs récurrentes sur
le formattage de mail.
Pour écrire un script d'envoi de mail, il faut :
 * Écrire un nouveau template dans `./gestion/mail/template/$nom_du_mail/`.
    Chaque sous-dossier correspond à un en-tête (sauf body), qui contient un
    fichier par langue (fr, en …)
 * Importer le module de mail: `from gestion import mail`
 * Se placer dans un nouveau contexte `mail.ServerConnection()`
 * Envoyer le mail à l'aide de la méthode `send_template`, prenant en argument
 le nom du template, et un dictionnaire des données à envoyer.
 * Les données à envoyer peuvent contenir le destinataire (champs `to`) ou
 directement l'adhérent (champ `adh`).


`
    import gestion.mail as mail
    with mail.ServerConnection() as conn_smtp:
        for adh in connexion_ldap.search(u'aid=*', sizelimit=4000):
            mail.send_template('mon_template', {'adh': adh, 'message': u'Coucou !'})
`


Ce fonctionnement présente plusieurs avantages :
 * À terme, il sera possible d'envoyer le mail dans la langue native de l'adhérent automatiquement
 * Il est facile d'envoyer un mail multipart
 * Le context manager assure qu'une seule connexion smtp est effectivement ouverte
 * En récupérant directement l'objet adhérent, send_template détermine automatiquement
 l'adresse de destination, voire abandonne l'envoi si l'adhérent possède une blackliste
 `mail_invalide`.
 * En environnement de test, le mail n'est pas envoyé par défaut au vrai destinataire
  (cf `DBG_MAIL` dans `testing.sh`)

## À faire

 * Expliquer l'environnement de test
 * tunnel pour apprentis
 * http://stackoverflow.com/questions/8021/allow-user-to-set-up-an-ssh-tunnel-but-nothing-else
 * snmp et les mibs ! !!