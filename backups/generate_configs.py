#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
#
# Script de génération des configs backuppc sur babar
# Doit être exécuté par root.
# Plein de trucs sont hardcodés, c'est chiant, il faudrait encore
# optimiser tout ça
#
# Boucle sur les divers serveurs pour générer leurs configs dans des fichiers perl
# Fait la même chose pour les homes, en fractionnant ceux-ci suivant la config
# donnée par backups.py
#
# Auteur : Pierre-Elliott Bécue <becue@crans.org>
# Licence : GPLv3

import sys
sys.path.append("/etc/backuppc/config/")
import backups
import os
import pwd
import grp
import socket

rootpath = "/etc/backuppc/"

if __name__ == "__main__":
    root = pwd.getpwnam('root').pw_uid
    wwwdata = grp.getgrnam("www-data").gr_gid

    if "babar" in socket.getfqdn():
        for hoteRsync in backups.RsyncHosts:
            backup_folders = backups.RsyncHostsToBackup[hoteRsync]
            backup_folders.update(backups.RsyncClassicalDirs)
            for excluded in backups.RsyncHostsToExclude[hoteRsync]:
                backup_folders.pop(excluded)
            with open(rootpath + hoteRsync + ".pl", "w") as fichier:
                fichier.write("""$Conf{RsyncShareName} = ['%(shares)s'];
$Conf{BackupFilesExclude} = {
    %(excludes)s
};

$Conf{IncrPeriod} = %(incr)s;
$Conf{FullPeriod} = %(full)s;""" % {'shares' : "', '".join(backup_folders.keys()), 'excludes' : "\n    ".join(["'%s' => ['lost+found']," % nom for nom in backup_folders.keys()]), 'incr': backups.RsyncPeriods[hoteRsync][0], 'full': backups.RsyncPeriods[hoteRsync][1]})
                if backups.DumpPreUserCmd[hoteRsync]:
                    fichier.write("""\n$Conf{DumpPreUserCmd} = '%(dp)s';""" % {'dp':backups.DumpPreUserCmd[hoteRsync],})
            os.chmod(rootpath + hoteRsync + ".pl", 0644)
            os.chown(rootpath + hoteRsync + ".pl", root, wwwdata)

    if "omnomnom" in socket.getfqdn():
        for homeDir in backups.HomeHosts:
            with open(rootpath + "home-" + homeDir + ".pl", "w") as fichier:
                fichier.write("""# Ces fichiers servent à backuper les homes en séparé.
$Conf{ClientNameAlias} = 'localhost';
$Conf{XferMethod} = 'tar';
$Conf{TarShareName} = ['/home/%(share)s'];
$Conf{TarClientCmd} = '/usr/bin/env LC_ALL=C sudo /usr/local/bin/backuppc_tar -v -f - -C $shareName --totals';

$Conf{BackupFilesExclude} = {
    '/home/%(share)s' => ['lost+found'],
};

# remove extra shell escapes ($fileList+ etc.) that are
# needed for remote backups but may break local ones
$Conf{TarFullArgs} = '$fileList';
$Conf{TarIncrArgs} = '--newer=$incrDate $fileList';
""" % {'share' : homeDir,})
            os.chmod(rootpath + "home-" + homeDir + ".pl", 0644)
            os.chown(rootpath + "home-" + homeDir + ".pl", root, wwwdata)
