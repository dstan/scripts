#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import subprocess
import sys

# TODO
# La couleur
# le recto/verso - recto
# mode paysage ?
# choix format papier
# mode sauce (première page manuelle)

DEBUG = True

DEFAULT_BAC = 7

PUNCH_DRV2PCL_OPTS = {
    "2HolePunchLeft": "LEFT_2PT_DIN",
    "2HolePunchRight": "RIGHT_2PT_DIN",
    "2HolePunchTop": "TOP_2PT_DIN",
    "2HolePunchBottom": "BT_2PT_DIN",
    "4HolePunchLeft": "LEFT_4PT_DIN",
    "4HolePunchRight": "RIGHT_4PT_DIN",
}

STAPLE_DRV2PCL_OPTS = {
    "1StapleLeftAngled": "LEFT_1PT_ANGLED",
    "1StapleRightAngled": "RIGHT_1PT_ANGLED",
    "2StaplesLeft": "LEFT_2PT",
    "2StaplesRight": "RIGHT_2PT",
    "2StaplesTop": "TOP_2PT", # à vérifier
}

def parse_opt(argv):
    """Parse les arguments à la cups depuis un argv, pour usage dans tweak."""
    backend = argv[0]
    jobid = int(argv[1])
    cupsuser = argv[2]
    jobname = argv[3]
    copies = int(argv[4])
    options = argv[5].split(' ')
    filename = (argv[6:] or ['-'])[0]

    opt = {
        'filename': filename,
        'jobname': jobname,
        'copies': copies,
        # Nombre de pages depuis le début à piocher depuis bac séparé
        'manual_feed_count': 0,
        'copies': copies,
        'finish': 'STAPLE', # TODO plus utilisé ?
        #'hold': 'STORE',
        'hold': 'OFF',
        'staple': 'NONE',
        'hole': 'NONE',
        'booklet': 'NONE',
        'color': True,
        'page_size': 'A4',
        'landscape': False,
        'sides': 'one-sided',
    }

    # Pour les options supplémentaires
    for entry in options:
        if '=' in entry:
            key, value = entry.split("=", 1)
        else:
            key = entry
        if key == "HPPunchingOptions":
            opt['hole'] = PUNCH_DRV2PCL_OPTS[value]
        if key == "HPStaplerOptions":
            if value == 'FoldStitch':
                opt['booklet'] = 'BOOKLET_MAKER'
            else:
                opt['staple'] = STAPLE_DRV2PCL_OPTS[value]
        if key == 'ColorModel':
            opt['color'] = (value != 'Grayscale')
        if key == 'PageSize':
            opt['page_size'] = value
        if key == 'landscape':
            opt['landscape'] = True
        if key == 'sides':
            opt['sides'] = value
    return opt

def build_gs_params(opt):
    """Construit la commande ghostscript de conversion en PCL"""
    if opt['color']:
        driver = 'pxlcolor'
    else:
        driver = 'pxlmono'

    # Imprimer en recto verso avec rotation par côté supérieur ?
    # Par défaut, short et portrait => tumble
    tumble = (opt['sides'] == 'two-sided-short-edge')
    if opt['landscape'] and opt['booklet'] != 'BOOKLET_MAKER':
        tumble = not tumble

    params = [
        'gs', '-dNOPAUSE', '-dBATCH',
        '-sDEVICE=' + driver,
        '-sPAPERSIZE=' + opt['page_size'],
        '-dDuplex=' + str(opt['sides'] != 'one-sided').lower(),
        '-dTumble=' + str(tumble).lower(),
        '-dMediaPosition=%d' % DEFAULT_BAC,
        '-sOutputFile=%stdout%',
        '-sstdout=/dev/null',
        '-f', opt['filename'],
    ]
    return params

def tweak(source, dest, opt):
    """Tweak un fichier pcl (source est un descripteur de fichier en lecture)
    en rajoutant les options PJL nécessaires, et en modifiant le PCL
    pour que les deux premières pages (première feuille) soient prises depuis
    un autre bac"""
    raw = False
    while not raw:
        l = source.readline()
        if l.startswith('@PJL ENTER LANGUAGE'):
            raw = True # Derniere ligne avant les trucs degueux
            dest.write("""@PJL SET JOBNAME = "%(jobname)s"
@PJL SET HOLD = "%(hold)s"
@PJL SET QTY = %(copies)d
@PJL SET PROCESSINGACTION=APPEND
@PJL SET PROCESSINGTYPE="STAPLING"
@PJL SET PROCESSINGOPTION="%(staple)s"
@PJL SET PROCESSINGBOUNDARY=MOPY
@PJL SET PROCESSINGTYPE="PUNCH"
@PJL SET PROCESSINGOPTION="%(hole)s"
@PJL SET PROCESSINGBOUNDARY=MOPY
@PJL SET PROCESSINGTYPE="BOOKLET_MAKER"
@PJL SET PROCESSINGOPTION="%(booklet)s"
@PJL SET PROCESSINGBOUNDARY=MOPY
""" % opt)
        dest.write(l)

    # Entrée manuelle sur les n premières pages. On remplace n fois
    s_string = '\xf8\x25\xc0' + chr(DEFAULT_BAC)
    r_string = '\xf8\x25\xc0\x01'
    count = opt['manual_feed_count']
    #
    while True:
        x = source.read(102400)
        #print "Read 100ko"
        #sys.stdout.flush()
        if not x:
            return
        while count:
            y = x.replace(s_string, r_string, 1)
            if x != y:
                x = y
                count -= 1
            else:
                break
        dest.write(x)


if __name__ == '__main__':
    
    sys.stderr.write("DEBUG: bonjour de crans_filter\n")
    sys.stderr.write("DEBUG: args: %r\n" % sys.argv)
    opt = parse_opt(sys.argv)

    if DEBUG:
        err = open('/tmp/lasterr', 'w')
        outpath = '/tmp/lastout'
    else:
        err=subprocess.PIPE,
        outpath = None
    
    if outpath:
        out = open(outpath, 'w')
    else:
        out = sys.stdout

    # Real algo here: on appelle gs puis on pipe dans la fonction tweak
    proc = subprocess.Popen(build_gs_params(opt),
                stdout=subprocess.PIPE,
                stderr=err,
                stdin=sys.stdin)
    tweak(proc.stdout, out, opt)

    # Si fichier temporaire, on dump le résultat juste après
    if outpath:
        out.close()
        with open(outpath, 'r') as f:
            while True:
                x = f.read(102400)
                if not x:
                    break
                sys.stdout.write(x)

