#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
"""
 etat_imprimante.py

     Récupère, filtre et formate l'état actuel de l'imprimante

 Copyright (c) 2006, 2007, 2008, 2009 by Cr@ns (http://www.crans.org)

"""

import BeautifulSoup
import requests
import HTMLParser
import sys
from gestion.hptools import snmp

STATUS_URL = 'https://imprimante.adm.crans.org/hp/device/DeviceStatus/Index'
CA = '/etc/ssl/certs/cacert.org.pem'

class ErreurCommunication(Exception):
    """Si une erreur est survenue lors de la communication avec l'imprimante"""
    pass

def etat():
    return \
     ["L'imprimante est en test. L'interfaçage n'est pas encore terminé.",
      "Une notification de fin d'impression est envoyée par mail, avec les codes nécessaires pour aller récupérer vos documents.",
      _http_status()]

def _http_status():
    try:
        req = requests.get(STATUS_URL, verify=CA)
        doc = BeautifulSoup.BeautifulSoup(req.text)
        status_tag = doc.find(attrs={'id': 'MachineStatus'})
        h = HTMLParser.HTMLParser()
        return h.unescape(status_tag.text)
    except:
        raise ErreurCommunication()

if __name__ == '__main__':
    for x in etat():
        print x
