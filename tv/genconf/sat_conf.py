#! /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Definit les transpondeurs a associer a chacune des cartes """

#NE PAS OUBLIER DE MODIFIER LE cartedesactivees.local et lancer bcfg2 apres
from sat_base_ng import *
from socket import gethostname

host = gethostname()

transpondeurs = { 'canard' : [ SAT_10773h(0),
                               SAT_10714h(2) ,
                               #Hotbird_12597(3) ,
                               #Hotbird_11604(4) ,
                               ],
                  'lapin' :  [ TNT_R1_586000(2),
                               TNT_R4_546000(3),
                               TNT_R6_562000(1),
                               ],
                  'poulet' :  [ ], # La 0 est la carte sat + decodeur #serveur de test
                  # Pas de carte TNT reconnue par poulet ?
                  'dindon' :  [ TNT_R5_530000(3) ,
                                TNT_R2_506000(1) ,
                                TNT_R3_482000(2),
                                ],
                  'oie' : [    #SAT_10773h(0),
                               SAT_11344h(0),
                               #SAT_11895v(1),
                               #Hotbird_12111(1) ,
                               #Hotbird_11137(2) ,
                               #SAT_11343v(3) ,
                               ],
                   'cochon.ferme.crans.org' : [ TNT_R1_586000(0),
                                TNT_R2_506000(1),
                                TNT_R3_482000(2),
                                TNT_R4_546000(3),
                                SAT_11344h(4),
                                SAT_10773h(5),
                                SAT_10714h(6),
                                SAT_11222h(7),
                                TNT_R5_530000(8),
                                TNT_R6_562000(9),
                                TNT_R7_642000(10),
                                TNT_L8_570000(11),
                              ]
                               }

conf = transpondeurs.get(host,[])

if __name__ == '__main__' :
    import sys
    if len(sys.argv) == 2 :
        conf = transpondeurs.get(sys.argv[1],[])
    for t in conf :
        print t, "sur la carte", t.card
        for (sap_group, chaine) in t.chaines.values() :
            print '\t%s\t: \t%s' % (sap_group,chaine)
