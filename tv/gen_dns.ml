(*
 * gen_dns.ml
 * ----------
 * Copyright : (c) 2009, Jeremie Dimino <jeremie@dimino.org>
 * Licence   : BSD3
 *)

(* Script écrit pour troller *)

let () =
  let direct = open_out "/etc/bind/generated/db.tv.crans.org"
  and reverse = open_out "/etc/bind/generated/db.239.in-addr.arpa"
  and chaines = open_in "/tmp/chaines_recup_sap.txt" in

  output_string direct "\
$ORIGIN tv.crans.org.
$TTL 86400
@ IN SOA mdr.crans.org. root.crans.org. (
    $serial   ; numero de serie
    21600     ; refresh
    3600      ; retry
    1209600   ; expire
    86400     ; TTL
)

@ IN NS mdr.crans.org.
@ IN NS charybde.crans.org.
@ IN NS freebox.crans.org.

@ IN A 138.231.136.243

";

  output_string reverse "\
$ORIGIN 239.in-addr.arpa.
$TTL 86400
@ IN SOA mdr.crans.org. root.crans.org. (
    $serial   ; numero de serie
    21600     ; refresh
    3600      ; retry
    1209600   ; expire
    86400     ; TTL
)

@ IN NS mdr.crans.org.
@ IN NS charybde.crans.org.
@ IN NS freebox.crans.org.

";

  let line_re = Str.regexp "^[^ ]+ \\(.*\\):\\(.*\\)$"
  and ipv4_re = Str.regexp "^[0-9]+\\.\\([0-9]+\\)\\.\\([0-9]+\\)\\.\\([0-9]+\\)$"
  and junk_re = Str.regexp "[ .():,\"]" in
  try
    while true do
      let line = input_line chaines in
      if Str.string_match line_re line 0 then begin
        let name = Str.matched_group 1 line and ip = Str.matched_group 2 line in
        let name = String.lowercase (Str.global_replace junk_re "" name) in
        Printf.fprintf direct "%s IN A %s\n" name ip;
        if Str.string_match ipv4_re ip 0 then
          Printf.fprintf reverse "%s.%s.%s IN PTR %s.tv.crans.org\n"
            (Str.matched_group 3 ip)
            (Str.matched_group 2 ip)
            (Str.matched_group 1 ip)
            name
        else
          Printf.ksprintf failwith "mauvaise adresse IP(%S)!" ip
      end
    done
  with End_of_file ->
    close_in chaines;
    close_out direct;
    close_out reverse
