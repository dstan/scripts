#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import sys
import socket

import utils
import base
import komaz
import zamok
import routeur

#: Nom de la machine exécutant le script
hostname = socket.gethostname()



#: Associe à un ``hostname`` la classe du pare-feu correspondant
firewall = {
    'komaz' : komaz.firewall,
    'odlyd' : komaz.firewall,
    'zamok' : zamok.firewall,
    'routeur' : routeur.firewall,
    'eap' : base.firewall,
    'pea' : base.firewall,
    'radius' : base.firewall_wifionly
}

if hostname in firewall.keys():
    #: Classe du pare-feu pour la machine ``hostname`` ou :py:class:`firewall_base` si non trouvé
    firewall = firewall[hostname]
else:
    #: Classe du pare-feu pour la machine ``hostname`` ou :py:class:`firewall_base` si non trouvé
    firewall = base.firewall

if __name__ == '__main__' :

    fw = firewall()

    # Chaînes pouvant être recontruites
    chaines = fw.reloadable.keys()

    def __usage(txt=None) :
        if txt!=None : utils.cprint(txt,'gras')

        chaines.sort()

        print """Usage:
   %(p)s start   : Construction du firewall.
   %(p)s restart : Reconstruction du firewall.
   %(p)s stop    : Arrêt du firewall.
   %(p)s <noms de chaînes> : reconstruit les chaînes
Les chaînes pouvant être reconstruites sont :
   %(chaines)s
Pour reconfiguration d'IPs particulières, utiliser generate. """ % \
{ 'p' : sys.argv[0].split('/')[-1] , 'chaines' : '\n   '.join(chaines) }
        sys.exit(-1)

    # Bons arguments ?
    if len(sys.argv) == 1 :
        __usage()
    for arg in sys.argv[1:] :
        if arg in [ 'stop', 'restart', 'start' ] and len(sys.argv) != 2 :
            __usage("L'argument %s ne peut être employé que seul." % arg)

        if arg not in [ 'stop', 'restart', 'start' ] + chaines :
            __usage("L'argument %s est inconnu." % arg)

    for arg in sys.argv[1:] :
        if arg == 'stop':
            fw.stop()
        elif arg == 'start':
            fw.start()
        elif arg == 'restart':
            fw.restart()
        else:
            fw.reload(arg)
