#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

u"""
Interface utilisateur du système de gestion des machines
et adhérents du crans

Copyright (C) Valentin Samir
Licence : GPLv3

"""
### sur le dialog (1.1) de zamok, il manque les fonctionnalité suivante présente dans la 1.2
### --default-button pour choisir le bouton sélectionner par defaut
### --not-tags pour masquer les tags quand il ne servent à rien pour l'utilisateur (mais sont utilisé comme index par le programme)

import os
import sys
import argparse
if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')

import lc_ldap.objets as objets
import lc_ldap.attributs as attributs

from dialog import adherent, club, machine, facture
from dialog.CPS import TailCall, tailcaller
from dialog.lc import main

def handle_exit_code(d, code):
    """Gère les codes de retour dialog du menu principal"""
    if code in (d.DIALOG_CANCEL, d.DIALOG_ESC):
        if code == d.DIALOG_CANCEL:
            #msg = "Vous avez choisi Annuler dans la dernière fenêtre de dialogue.\n\n" \
            #      "Voulez vous quitter le programme ?"
            os.system('clear')
            sys.exit(0)
        else:
            msg = "Vous avez appuyé sur ESC ou CTRL+C dans la dernière fenêtre de dialogue.\n\n" \
                  "Voulez vous quitter le programme ?"
        if d.yesno(msg, width=60) == d.DIALOG_OK:
            os.system('clear')
            sys.exit(0)
        return False
    else:
        return True

class GestCrans(adherent.Dialog, club.Dialog, facture.Dialog, machine.Dialog):
    @tailcaller
    def menu_principal(self, tag=None, machine=None, proprio=None):
        """Menu principal de l'application affiché au lancement"""
        a = attributs
        menu_droits = {
            'default' : [a.cableur, a.nounou],
            'aKM' : [a.nounou],
        }
        menu = {
         'aA' : {'text':"Inscrire un nouvel adhérent",           'callback': self.create_adherent,},
         'mA' : {'text':"Modifier l'inscription d'un adhérent",  'callback': self.modif_adherent,         'help':"Changer la chambre, la remarque, la section, la carte d'étudiant ou précâbler."},
         'aMA': {'text':"Ajouter une machine à un adhérent",     'callback': self.create_machine_adherent},
         #'dA' : {'text':"Détruire un adhérent",                  'callback': self.delete_adherent,        'help':"Suppression de l'adhérent ainsi que de ses machines"},
         'mM' : {'text':"Modifier une machine existante",        'callback': self.modif_machine,          'help':"Changer le nom ou la MAC d'une machine."},
         #'dM' : {'text':"Détruire une machine",                  'callback': self.delete_machine},
         'mF' : {'text':"Modifier une facture",                      'callback': self.modif_facture},
         'aC' : {'text':"Inscrire un nouveau club",              'callback': self.create_club},
         'mC' : {'text':"Modifier un club",                      'callback': self.modif_club},
         'aMC': {'text':"Ajouter une machine à un club",         'callback': self.create_machine_club},
         #'dC' : {'text':"Détruire un club",                      'callback': self.delete_club},
         'aKM': {'text':"Ajouter une machine à l'association",   'callback': self.create_machine_crans},
         '' :        {'text':"---------------------------------------",'callback': None},
        }
        ### Les clef qui n'existe pas sont toute renvoyé sur la clef ''
        #menu_order = ["aA", "mA", "aMA", "dA", "", "mM", "dM", " ", "aC", "mC", "aMC", "dC", "  ", "aKM"]
        menu_order = ["aA", "mA", "aMA", "", "mM", "", "mF", "", "aC", "mC", "aMC", "", "aKM"]
        if machine and not proprio:
            proprio = machine.proprio()
        if isinstance(proprio, objets.AssociationCrans):
            proprio = None
        if machine or proprio:
            menu_order = [''] + menu_order
        if machine:
            menu_machine = {
                'mMc' : {
                  'text':"Modifier la machine %s" % machine['host'][0],
                  'callback': TailCall(self.modif_machine, machine=machine),
                  'help':"Changer le nom ou la MAC d'une machine."
                },
            }
            menu_machine_order = ['mMc']
            menu.update(menu_machine)
            menu_order = menu_machine_order + menu_order
        if proprio:
            menu_adherent = {
                'mAc' : {
                  'text':"Modifier l'inscription de %s" % proprio.get("cn", proprio["nom"])[0],
                  'callback': TailCall(self.modif_adherent, adherent=proprio)
                 },
                'aMc' : {
                  'text':"Ajouter une machine à %s" % proprio.get("cn", proprio["nom"])[0],
                  'callback': TailCall(self.create_machine_adherent,
                                       adherent=proprio)
                },
            }
            menu_club = {
                'mCc' : {
                  'text':"Modifier l'inscription de %s" % proprio.get("cn", proprio["nom"])[0],
                  'callback': TailCall(self.modif_club, club=proprio)
                 },
                'aMc' : {
                  'text':"Ajouter une machine à %s" % proprio.get("cn", proprio["nom"])[0],
                  'callback': TailCall(self.create_machine_club, club=proprio)
                },
            }
            if 'adherent' in proprio['objectClass']:
                menu_proprio = menu_adherent
                menu_proprio_order = ['mAc', 'aMc']
            elif 'club' in proprio['objectClass']:
                menu_proprio = menu_club
                menu_proprio_order = ['mCc', 'aMc']
            else:
                raise EnvironmentError("Je ne connais que des adherents et des club comme proprio")
            menu.update(menu_proprio)
            menu_order = menu_proprio_order + menu_order

        def box(default_item=None):
            choices = []
            for key in menu_order:
                if self.has_right(menu_droits.get(key, menu_droits['default'])):
                    choices.append((key,  menu[key]['text'], menu[key].get('help', "")))
            while choices[-1][0] == '':
                choices=choices[:-1]
            while choices[0][0] == '':
                choices=choices[1:]
            return self.dialog.menu(
                "Que souhaitez vous faire ?",
                width=0,
                height=0,
                menu_height=0,
                item_help=1,
                default_item=str(default_item),
                title="Menu principal",
                scrollbar=True,
                timeout=self.timeout,
                cancel_label="Quitter",
                backtitle=self._connected_as(),
                choices=choices)

        (code, tag) = self.handle_dialog(TailCall(handle_exit_code, self.dialog, self.dialog.DIALOG_ESC), box, tag)
        self_cont = TailCall(self.menu_principal, tag=tag, proprio=proprio, machine=machine)
        callback = menu.get(tag, menu[''])['callback']
        if handle_exit_code(self.dialog, code) and callback:
                return TailCall(callback, cont=TailCall(self.menu_principal, tag=tag, machine=machine, proprio=proprio))
        else:
            return self_cont



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Interface utilisateur du système de gestion des machines et adhérents du crans')
    parser.add_argument('--test', help='Utiliser la base de test', dest='ldap_test', default=False, action='store_true')
    parser.add_argument('--debug', help='Afficher des info de débug comme les tracebacks', dest='debug_enable', default=False, action='store_true')
    parser.add_argument('login', help="Se connecter en tant qu'un autre utilisateur", type=str, default=None, nargs='?')
    args = parser.parse_args()
    main(GestCrans(ldap_test=args.ldap_test, debug_enable=args.debug_enable, custom_user=args.login))
    os.system('clear')
