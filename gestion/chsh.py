#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

""" Changement du shell de l'utilisateur lancant le programme
    DOIT ETRE LANCE PAR SUDO

    Copyright (C) Frédéric Pauget
    Licence : GPLv2
"""
import os, sys

from gestion.affich_tools import prompt

from lc_ldap import shortcuts

ldap = shortcuts.lc_ldap_admin()
uid = os.getenv('SUDO_UID')
if not uid :
    print "Impossible de déterminer l'utilisateur"
    sys.exit(1)

adh = ldap.search(u'uidNumber=%s' % uid,mode='w')

try:
    adh = adh[0]
except IndexError:
    print 'Erreur fatale lors de la consultation de la base LDAP'
    sys.exit(3)

# On vérifie que c'est pas un club
if unicode(adh.ldap_name)!=u"adherent":
    print 'Pas de changement de shell pour les clubs'
    sys.exit(2)

shell = prompt(u'Nouveau shell :')
fd=open('/etc/shells')
lines=fd.readlines()
fd.close()

shells = [line.strip() for line in lines if not (line.startswith('#') or line.endswith('/rssh'))]

if not shell in shells:
    print 'Shell invalide. Les shells valides sont :'
    print '\n'.join(shells)
    sys.exit(4)

with adh as ad:
    ad['loginShell']=shell
    ad.save()

# A cause de nscd
print "La modification sera prise en compte dans l'heure suivante."
