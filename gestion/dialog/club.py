#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

u"""
Copyright (C)

Valentin Samir
Gabriel Détraz

Licence : GPLv3

"""
import sys
import time
if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')

import lc_ldap.objets as objets
import lc_ldap.attributs as attributs
import lc_ldap.crans_utils

from gestion.config import cotisation

import proprio
from CPS import TailCall, tailcaller, Continue

class Dialog(proprio.Dialog):
    def create_club(self, cont):
        """Crée un club et potentiellement son compte crans avec lui"""
        def mycont(proprio=None, **kwargs):
            if proprio:
                # Une fois le club créé, on vois s'il adhére/prend la connexion internet
                conn_cont = TailCall(self.club_administratif, cont=cont, club=proprio)
                raise Continue(conn_cont)
            else:
                raise Continue(cont)
        return self.proprio_personnel(cont=TailCall(mycont), sorte='club')

    def delete_club(self, cont, del_cont, club):
        """Permet la suppression d'un club de la base ldap"""
        if club is None:
            club = self.select(["club"], "Recherche d'un club pour supression", cont=cont)

        def todo(club):
            if self.confirm_item(item=club, title="Voulez vous vraiement supprimer le club ?", defaultno=True):
                with self.conn.search(dn=club.dn, scope=0, mode='rw')[0] as club:
                    club.delete()
                self.dialog.msgbox("Le club a bien été supprimée", timeout=self.timeout, title="Suppression d'un club")
                raise Continue(del_cont(proprio=None))
            else:
                raise Continue(cont)

        return self.handle_dialog_result(
            code=self.dialog.DIALOG_OK,
            output="",
            cancel_cont=cont(club=club),
            error_cont=cont(club=club),
            codes_todo=[([self.dialog.DIALOG_OK], todo, [club])]
        )


    def modif_club(self, cont, club=None, proprio=None, tag=None):
        if proprio:
            club=proprio
        if club is None:
            club = self.select(["club"], "Recherche d'un club pour modification", disable_field=["Prénom", "Téléphone"], cont=cont)
	a = attributs
	menu_droits = {
            'Administratif' : [a.cableur, a.nounou],
            'Infos':[a.cableur, a.nounou, a.soi],
            'Gestion':[a.cableur, a.nounou],
            'Local':[a.cableur, a.nounou],
            'Compte':[a.cableur, a.nounou],
            'Remarques' : [a.cableur, a.nounou],
            'Blackliste':[a.bureau, a.nounou],
            'Vente':[a.cableur, a.nounou],
            'Supprimer':[a.nounou, a.bureau],
        }
        menu = {
            'Administratif' : {'text' : "Adhésion du club", "callback":self.club_administratif},
            'Infos' : {'text' : "Nom, mail de secours", "club":"proprio", 'callback':TailCall(self.proprio_personnel, sorte='club')},
            'Gestion' : {'text' : 'Responsable du club et imprimeurs', "callback":self.gestion_club},
            'Local'      : {'text' : 'Déménagement', "club":"proprio", "callback":self.proprio_chambre},
            'Compte'       : {'text' : "Gestion du compte crans", "club":"proprio", "callback":TailCall(self.proprio_compte, update_obj='club'), 'help':"Création/Suppression/Activation/Désactivation du compte, gestion des alias mails crans du compte"},
            'Remarques' : {'text':'Ajouter ou supprimer une remarque à ce club', 'attribut':attributs.info},
            'Blackliste' : {'text': 'Modifier les blacklist de ce club', 'callback':self.modif_club_blacklist},
            'Vente' : {'text':"Chargement solde crans, vente de cable ou adaptateur ethernet ou autre", "club":"proprio", "callback":self.proprio_vente},
            'Supprimer' : {'text':"Supprimer le club de la base de donnée", 'callback':TailCall(self.delete_club, del_cont=cont(proprio=None))},
        }
        menu_order = ['Administratif', 'Infos', 'Gestion', 'Local', 'Compte', 'Remarques', 'Blackliste', 'Vente', 'Supprimer']

        def box(default_item=None):
            choices = []
            for key in menu_order:
                if self.has_right(menu_droits[key]):
                    choices.append((key, menu[key]['text'], menu[key].get('help', "")))
            return self.dialog.menu(
                "Que souhaitez vous modifier ?",
                width=0,
                height=0,
                menu_height=0,
                timeout=self.timeout,
                item_help=1,
                default_item=str(default_item),
                title="Modification du club %s" % (club["nom"][0]),
                scrollbar=True,
                cancel_label="Retour",
                backtitle=self._connected_as(),
                choices=choices)

        def todo(tag, menu, club, cont_ret):
            if not tag in menu_order:
                raise Continue(cont_ret)
            else:
                if 'callback' in menu[tag]:
                    raise Continue(TailCall(menu[tag]['callback'], cont=cont_ret, **{menu[tag].get('club', 'club'):club}))
                elif 'attribut' in menu[tag]:
                    raise Continue(TailCall(self.modif_club_attributs, club=club, cont=cont_ret, attr=menu[tag]['attribut'].ldap_name))
                else:
                    raise EnvironmentError("Il n'y a ni champ 'attribut' ni 'callback' pour le tag %s" % tag)

        (code, tag) = self.handle_dialog(cont, box, tag)
        cont_ret = TailCall(self.modif_club, cont=cont, club=club, tag=tag)

        return self.handle_dialog_result(
            code=code,
            output=tag,
            cancel_cont=cont(proprio=club),
            error_cont=cont_ret,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [tag, menu, club, cont_ret])]
        )


    def modif_club_attributs(self, club, attr, cont):
        """Juste un raccourci vers edit_attributs spécifique aux clubs"""
        return self.edit_attributs(obj=club, update_obj='club', attr=attr, title="Modification du club %s" % (club["nom"][0]), cont=cont)

    def modif_club_blacklist(self, club, cont):
        """Raccourci vers edit_blacklist spécifique aux clubs"""
        return self.edit_blacklist(obj=club, title="Éditions des blacklist de %s" % ( club['nom'][0]), update_obj='club', cont=cont)

    def respo_club(self, club, cont, return_obj=False):
        """ Choix du respo club """
        adherent = self.select(["adherent"], "Recherche d'un adherent pour être reponsable du club", cont=cont)
        if not return_obj:
            with self.conn.search(dn=club.dn, scope=0, mode='rw')[0] as club:
                club['responsable'] = int(adherent['aid'][0])
                club.history_gen()
                club.save()
                self.dialog.msgbox(text="Responsable changé avec succès", title=u"Responsable changé", width=0, height=0, timeout=self.timeout)
            raise Continue(cont)
        else:
            club['responsable'] = int(adherent['aid'][0])
            return club

    def imprimeur_add(self, club, cont, update_obj='club'):
        """ Choix d'un imprimeur à ajouter """
        adherent = self.select(["adherent"], "Recherche d'un adherent pour être imprimeur du club", cont=cont)
        with self.conn.search(dn=club.dn, scope=0, mode='rw')[0] as club:
            try:
                club['imprimeurClub'].append(unicode(adherent['aid'][0]))
                club.history_gen()
                club.save()
                self.dialog.msgbox(text="Imprimeur ajouté avec succès", title=u"Imprimeur ajouté", width=0, height=0, timeout=self.timeout)
            except ValueError as e:
                self.dialog.msgbox(text=unicode(e), title=u"Erreur rencontrée", width=0, height=0, timeout=self.timeout)
        raise Continue(cont(**{update_obj:club}))

    def imprimeur_del(self, club, cont, update_obj='club'):
        """ Choix d'un imprimeur à retirer"""
        def box():
            return self.dialog.checklist(
                     text="",
                     height=0, width=0, list_height=0,
                     choices=[(unicode(impr), self.conn.search(u'aid=%s' % unicode(impr))[0]['nom'][0].value, 0) for impr in club['imprimeurClub']],
                     title="Imprimeurs du club %s" % (club['nom'][0]),
                     timeout=self.timeout
                     )
        def todo(impr_del, club, self_cont, cont, update_obj='club'):
            with self.conn.search(dn=club.dn, scope=0, mode='rw')[0] as club:
                try:
                    for imprimeur in impr_del:
                        club['imprimeurClub'].remove(imprimeur)
                    club.history_gen()
                    club.save()
                    self.dialog.msgbox(text="Imprimeur retiré avec succès", title=u"Imprimeur retiré", width=0, height=0, timeout=self.timeout)
                except ValueError as e:
                    self.dialog.msgbox(text=unicode(e), title=u"Erreur rencontrée", width=0, height=0, timeout=self.timeout)
            raise Continue(cont(**{update_obj:club}))

        (code, impr_del) = self.handle_dialog(cont, box)
        self_cont = TailCall(self.imprimeur_del, club=club, cont=cont)
        return self.handle_dialog_result(
            code=code,
            cancel_cont=cont,
            output=impr_del,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [impr_del, club, self_cont, cont, update_obj])]
        )



    def gestion_club(self, club, cont, default_item=None):
        """Menu de gestion spécifique aux clubs"""

        a = attributs
        menu_droits = {
            "Responsable": [a.cableur, a.nounou],
            'Ajouter un imprimeur': [a.cableur, a.nounou],
            "Retirer un imprimeur" : [a.nounou, a.cableur],
        }
        menu = {
            "Responsable" : {"text":"Changer le responsable du club", "help":"", "callback":self.respo_club},
            'Ajouter un imprimeur' : {'text': "Choix d'un adhérent comme nouvel imprimeur", "help":"", 'callback':self.imprimeur_add},
            "Retirer un imprimeur" : {"text" : "Enlever un imprimeur au club", "help":'', "callback":self.imprimeur_del},
        }
        menu_order = ["Responsable", 'Ajouter un imprimeur', "Retirer un imprimeur"]
        def box(default_item=None):
            return self.dialog.menu(
                "Quelle action administrative effectuer",
                width=0,
                height=0,
                timeout=self.timeout,
                item_help=1,
                default_item=str(default_item),
                title="Gestion administrative du club %s" % (club["nom"][0]),
                scrollbar=True,
                cancel_label="Retour",
                backtitle=self._connected_as(),
                choices=[(k, menu[k]['text'], menu[k]['help']) for k in menu_order if self.has_right(menu_droits[k], club)])

        def todo(tag, menu, club, self_cont):
            if not tag in menu_order:
                raise Continue(self_cont)
            elif 'callback' in menu[tag]:
                raise Continue(TailCall(menu[tag]['callback'], cont=self_cont, club=club))
            else:
                raise EnvironmentError("Il n'y a pas de champs 'callback' pour le tag %s" % tag)


        (code, tag) = self.handle_dialog(cont, box, default_item)
        self_cont = TailCall(self.gestion_club, club=club, cont=cont, default_item=tag)
        return self.handle_dialog_result(
            code=code,
            output=tag,
            cancel_cont=cont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [tag, menu, club, self_cont])]
        )

    def club_administratif(self, club, cont):
        """ Adhesion du club """
        def check_delais(club):
            now = lc_ldap.crans_utils.localized_datetime()
            try:
                delta = club.fin_adhesion().value - now
                delta = delta.days
            # Si il n'y a pas de fin adh, on renvoie 0
            except AttributeError:
                delta = 0
            if delta < cotisation.delai_readh_jour:
                return True
            else:
                return False

        def do_readhesion(club):
            dico_adh = cotisation.dico_adh_club
            fact = {
              'modePaiement': ['arbitraire'],
              'article': [u'%s~~%s~~%s~~%s' % (dico_adh['code'], dico_adh['designation'], dico_adh['nombre'], dico_adh['pu'])],
            }
            with self.conn.newFacture(club.dn, fact) as facture:
                facture.adhesion_connexion()
                fin_adh = unicode(facture['finAdhesion'][0])
                debut_adh = unicode(facture['debutAdhesion'][0])
                fid = unicode(facture['fid'][0])
            with self.conn.search(dn=club.dn, scope=0, mode='rw')[0] as club:
                club['finAdhesion'].append(fin_adh)
                club['debutAdhesion'].append(debut_adh)
                club.history_gen()
                club.save()
            self.dialog.msgbox(text=u"Adhésion effectuée avec succès, fid=%s" % fid, title=u"Adhésion club", width=0, timeout=self.timeout)
        if check_delais(club):
            if self.dialog.yesno(u"Réadhérer le club pour un an ?",
                           title=u"Adhésion du club %s" % club["nom"][0],
                           timeout=self.timeout) == self.dialog.DIALOG_OK:
                do_readhesion(club)
        else:
            self.dialog.msgbox(text=u"Merci de réadhérer le club moins d'un mois avant la fin de l'adhésion", title=u"Adhésion club", width=70, timeout=self.timeout)
        raise Continue(cont)
