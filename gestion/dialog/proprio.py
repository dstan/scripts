#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

u"""
Copyright (C)

Valentin Samir
Gabriel Détraz

Licence : GPLv3

"""
import os
import sys
import copy
import ldap
import time
import unicodedata
import subprocess
if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')

from gestion.chgpass import check_password
import gestion.config as config
import gestion.config.factures

import lc_ldap.objets as objets
import lc_ldap.attributs as attributs
import lc_ldap.crans_utils as lc_utils

from lc_ldap.attributs import UniquenessError


import machine
import blacklist
from CPS import TailCall, tailcaller, Continue

class Dialog(machine.Dialog, blacklist.Dialog):
    def modif_proprio_attributs(self, proprio, attr, cont):
        """Juste un raccourci vers edit_attributs spécifique aux proprios"""
        return self.edit_attributs(obj=proprio, update_obj='proprio', attr=attr, title="Modification de %s %s" % (proprio.get('prenom', [''])[0], proprio['nom'][0]), cont=cont)
    @tailcaller
    def proprio_compte_create(self, proprio, cont, warning=True, return_obj=False, update_obj='proprio'):
        """Permet de créer un compte crans à un proprio (club ou adhérent)"""
        def box_warning(warning, proprio, cont):
            # Affiche-t-on le warning sur la consutation de l'adresse crans
            if warning:
                if self.dialog.yesno(
                  text="\Zr\Z1AVERTISSEMENT :\Zn \nL'adhérent devra impérativement consulter l'adresse mail associée\n\n\n\ZnContinuer ?",
                  title="Création du compte de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                  defaultno=True,
                  width=70,
                  colors=True, timeout=self.timeout) != self.dialog.DIALOG_OK:
                    raise Continue(cont)

        def get_login(proprio, self_cont, cont):
            # Essaye-t-on de forcer le login à utiliser
            nom = unicode(unicodedata.normalize('NFKD', unicode(proprio['nom'][0])).encode('ascii', 'ignore')).lower().replace(' ', '-')
            login_crans=nom
            if proprio.get('prenom', [False])[0]:
                prenom = unicode(unicodedata.normalize('NFKD', unicode(proprio['prenom'][0])).encode('ascii', 'ignore')).lower().replace(' ', '-')
                baselogin = nom
                for rang,let in enumerate(prenom):
                    if self.conn.search(u'uid=%s' % login_crans) == []:
                        break
                    else:
                        login_crans = prenom[:rang] + baselogin
            (code, login) = self.dialog.inputbox(
                   text="Le login doit faire au maximum %s caractères\nIl ne doit pas être un pseudo ou prénom mais doit être relié au nom de famille\nSeuls les caractères alphabétiques et le trait d'union sont autorisés" % config.maxlen_login,
                   title="Choix du login pour %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                   init=login_crans,
                   width=60,
                   height=10, timeout=self.timeout)
            if code != self.dialog.DIALOG_OK:
                raise Continue(cont)
            return login

        def create_compte(proprio, login, self_cont, cont, return_obj=False):
            try:
                proprio.compte(login=unicode(login, 'utf-8'))
            except (ValueError, UniquenessError):
                self.dialog.msgbox(
                    text=u'Ce login est déjà utilisé',
                    title="Création du compte de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                    width=75,
                    height=12, timeout=self.timeout,
                    )
                raise Continue(self_cont(return_obj=return_obj))

            if isinstance(proprio, objets.adherent):
                titre = "Le compte ne sera créé que lors de l'enregistrement des données\n\nL'adresse mail de l'adhérent est : %s\nL'adhérent possède également l'alias :  \n%s\n" % (proprio['mail'][0], proprio['canonicalAlias'][0])
            else:
                titre = "Le compte ne sera créé que lors de l'enregistrement des données"
            self.dialog.msgbox(
               text=titre,
               title="Création du compte de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
               width=75,
               height=12, timeout=self.timeout,
            )
            return proprio

        @tailcaller
        def set_password(proprio, update_obj, cont):
            if self.dialog.yesno("Attribuer un mot de passe maintenant ? (Vous aurez la possibilité d'imprimer un ticket plus tard également ...)",
              title="Création du compte de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
              timeout=self.timeout
            ) == self.dialog.DIALOG_OK:
                proprio = self.proprio_compte_password(proprio=proprio, return_obj=True, cont=TailCall(set_password, proprio, update_obj, cont))
                if return_obj:
                    return proprio
                else:
                    raise Continue(cont(**{update_obj:proprio}))
            elif return_obj:
                return proprio
            else:
                raise Continue(cont(**{update_obj:proprio}))

        def todo(proprio, warning, return_obj, self_cont, cont):
            box_warning(warning, proprio, cont)
            login = get_login(proprio, self_cont, cont)
            if return_obj:
                proprio = create_compte(proprio, login, self_cont, cont, return_obj)
                return set_password(proprio, update_obj, cont)
            else:
                with self.conn.search(dn=proprio.dn, scope=0, mode='rw')[0] as proprio:
                    proprio = create_compte(proprio, login, self_cont, cont)
                    if not self.confirm_item(item=proprio, title="Création du compte crans pour l'adhérent ?"):
                        raise Continue(cont)
                    else:
                        proprio.validate_changes()
                        proprio.history_gen()
                        proprio.save()
                        self.dialog.msgbox(
                          text="Compte créé avec succès.",
                          title="Création du compte de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                          timeout=self.timeout
                        )
                        return set_password(proprio, update_obj, cont)

        self_cont = TailCall(self.proprio_compte_create, proprio=proprio, cont=cont, warning=warning)
        return self.handle_dialog_result(
            code=self.dialog.DIALOG_OK,
            output="",
            cancel_cont=cont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [proprio, warning, return_obj, self_cont, cont])]
        )

    @tailcaller
    def proprio_compte_password(self, proprio, cont, return_obj=False):
        """Permet de changer le mot de passe d'un compte crans"""
        def test_password(password, self_cont):
            (good, msg) = check_password(password, dialog=True)
            if not good:
                self.dialog.msgbox(
                   msg,
                   title="Erreur dans le mot de passe de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                   colors=True,
                   width=70, timeout=self.timeout)
                raise Continue(self_cont)
            else:
                return True
        def todo(passwords, proprio, return_obj, self_cont, cont):
            password = self.get_password(cont=cont,
                        title="Choix du mot de passe pour %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                        backtitle="Le mot de passe doit être assez difficile")

            if test_password(password, self_cont):
                if return_obj:
                    proprio['userPassword']=unicode(lc_utils.hash_password(password))
                    return proprio
                else:
                    with self.conn.search(dn=proprio.dn, scope=0, mode='rw')[0] as proprio:
                        proprio['userPassword']=unicode(lc_utils.hash_password(password))
                        proprio.validate_changes()
                        proprio.history_gen()
                        proprio.save()
                    self.dialog.msgbox(
                      "Mot de passe changé avec succès",
                       title="Choix du mot de passe pour %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                       width=70, timeout=self.timeout
                     )
                    raise Continue(cont(proprio=proprio))
        #(code, passwords) = self.handle_dialog(cont, box)
        (code, passwords) = (self.dialog.DIALOG_OK, "")
        self_cont = TailCall(self.proprio_compte_password, proprio=proprio, cont=cont, return_obj=return_obj)
        return self.handle_dialog_result(
            code=code,
            output=passwords,
            cancel_cont=cont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [passwords, proprio, return_obj, self_cont, cont])]
        )

    @tailcaller
    def proprio_compte_delete(self, proprio, cont, force=False):
        """Permet la suppression du compte crans d'un proprio"""
        def todo(proprio, self_cont, cont):
            if force or self.confirm_item(item=proprio, title="Voulez vous vraiement supprimer le compte de %s %s ?" % (proprio.get('prenom', [''])[0], proprio["nom"][0]), defaultno=True):
                if float(proprio.get('solde', [0])[0]) > float(0):
                    self.dialog.msgbox("Solde de l'adhérent %s€ strictement positif, impossible de supprimer le compte\nRepasser le solde à 0€ pour supprimer le compte." % proprio.get('solde', [0])[0],
                        title=u"Déménagement de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                        width=50, timeout=self.timeout)
                    raise Continue(cont)
                if isinstance(proprio, objets.adherent):
                    (code, mail) = self.dialog.inputbox(
                              text="Il faut choisir une nouvelle adresse de contact.\n(On regarde s'il y a une adresse optionnel)",
                              title="Choix d'une adresse de contact pour %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                            init=str(proprio.get("mailExt", [""])[0]),
                            width=50, timeout=self.timeout)
                    if not code == self.dialog.DIALOG_OK:
                        raise Continue(cont)
                    elif not mail:
                        raise ValueError("Il faut entrer une adresse mail")
                with self.conn.search(dn=proprio.dn, scope=0, mode='rw')[0] as proprio:
                    if isinstance(proprio, objets.adherent):
                        proprio.delete_compte(unicode(mail, 'utf-8'))
                    else:
                        proprio.delete_compte_club()
                    proprio.validate_changes()
                    proprio.history_gen()
                    proprio.save()
                self.dialog.msgbox("Le compte a bien été supprimée", timeout=self.timeout, title="Suppression du compte de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]))
                raise Continue(cont(proprio=proprio))
            else:
                raise Continue(cont)

        self_cont = TailCall(self.proprio_compte_delete, proprio=proprio, cont=cont, force=force)
        return self.handle_dialog_result(
            code=self.dialog.DIALOG_OK,
            output="",
            cancel_cont=cont(proprio=proprio),
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [proprio, self_cont, cont])]
        )

    def proprio_compte_etat(self, proprio, disable, cont):
        """Permet de d'éastiver ou activer un compte crans avec l'attribut shadowExpire"""
        with self.conn.search(dn=proprio.dn, scope=0, mode='rw')[0] as proprio:
            if disable:
                proprio["shadowExpire"]=0
            else:
                proprio["shadowExpire"]=[]
            proprio.validate_changes()
            proprio.history_gen()
            proprio.save()
        raise Continue(cont(proprio=proprio))

    def proprio_compte_shell(self, proprio, cont, choices_values=None):
        """Permet de modifier le shell d'un compte crans"""
        a = attributs
        # Seul les nounous peuvent changer les shells restrictifs
        shells_droits = {
            'default' : [a.soi, a.nounou, a.cableur],
            'rbash' : [a.nounou],
            'rssh' : [a.nounou],
            'badPassSh' : [a.nounou],
            'disconnect_shell':[a.nounou],
        }
        shell = os.path.basename(str(proprio['loginShell'][0])).lower()
        shells = config.shells_gest_crans
        shells_order = config.shells_gest_crans_order
        def box():
            # liste des shell éditables par l'utilisateur
            editable_sheels = [s for s in shells_order if self.has_right(shells_droits.get(s, shells_droits['default']), proprio)]
            # Si l'utilisateur de gest_crans peut éditer le shell courant
            # il peut le changer pour un autre shell qu'il peut éditer
            if shell in editable_sheels:
                choices=[(s, shells[s]['desc'], 1 if s == shell else 0) for s in editable_sheels]
                return self.dialog.radiolist(
                     text="",
                     height=0, width=0, list_height=0,
                     choices=choices_values if choices_values else choices,
                     title="Shell de %s %s" % (proprio.get('prenom', [""])[0], proprio['nom'][0]),
                     timeout=self.timeout
                 )
            # Sinon, on affiche un message d'erreur et on annule
            else:
                self.dialog.msgbox("Vous ne pouvez pas changer le shell de cet utilisateur",
                  title="Édition du shell impossible", timeout=self.timeout, width=0, height=0)
                return (self.dialog.DIALOG_CANCEL, None)
        def todo(output, shell, shells, proprio, self_cont, cont):
            loginShell = shells[output]['path']
            if shell and shell != output:
                with self.conn.search(dn=proprio.dn, scope=0, mode='rw')[0] as proprio:
                    proprio['loginShell']=unicode(loginShell)
                    proprio.validate_changes()
                    proprio.history_gen()
                    proprio.save()
                self.dialog.msgbox("Shell modifié avec succès.\nLa modification peut prendre une quinzaine de minutes avant d'être effective.",
                     title="Shell de %s %s" % (proprio.get('prenom', [""])[0], proprio['nom'][0]),
                     width=50, timeout=self.timeout,
                )
            raise Continue(cont(proprio=proprio))

        (code, output) = self.handle_dialog(cont, box)
        self_cont = TailCall(self.proprio_compte_shell, proprio=proprio, cont=cont, choices_values=[(s, shells[s]['desc'], 1 if s == output else 0) for s in shells_order])
        return self.handle_dialog_result(
            code=code,
            output=output,
            cancel_cont=cont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [output, shell, shells, proprio, self_cont, cont])]
        )

    def proprio_compte(self, proprio, cont, default_item=None, update_obj='proprio'):
        """Menu de gestion du compte crans d'un proprio"""
        has_compte = 'cransAccount' in proprio['objectClass']
        disabled_compte = has_compte and 0 in proprio['shadowExpire']
        a = attributs
        menu_droits = {
            "Password": [a.cableur, a.nounou],
            'MailAlias': [a.cableur, a.nounou],
            "Activer" : [a.nounou],
            "Désactiver" : [a.nounou],
            "Créer" : [a.cableur, a.nounou],
            "Supprimer" : [a.cableur, a.nounou],
            "Shell" : [a.nounou, a.soi, a.cableur],
        }
        menu = {
            "Password" : {"text":"Changer le mot de passe du compte", "help":"", "callback":self.proprio_compte_password},
            'MailAlias' : {'text': 'Créer ou supprimer des alias mail', "help":"", 'attribut':attributs.mailAlias},
            "Shell" : {"text" : "Changer le shell de cet utilisateur", "help":'', "callback":self.proprio_compte_shell},
            "Activer" : {"text" : "Activer le compte pour la connexion mail/serveur", "help":"Permet d'autoriser les connexions smtp, imap, ssh, etc… avec le compte", "callback":TailCall(self.proprio_compte_etat, disable=False)},
            "Désactiver" : {"text" : "Désactiver le compte pour la connexion mail/serveur", "help":"Permet d'interdire les connexions smtp, imap, ssh, etc… avec le compte", "callback":TailCall(self.proprio_compte_etat, disable=True)},
            "Créer" : {"text": "Créer un compte", "help":'', "callback":self.proprio_compte_create},
            "Supprimer" : {"text": "Supprimer le compte", "help":"Le home sera archivé dans le cimetière", "callback":self.proprio_compte_delete},
        }
        menu_order = []
        tag_translate = {
            "Créer":"Password",
            "Password":"Password",
            "Supprimer":"Créer",
            "Activer":"Désactiver",
            "Désactiver":"Activer",
            "Shell":"Shell",
            'MailAlias':'MailAlias',
            '':'',
        }
        if has_compte:
            if disabled_compte:
                menu_order.append("Activer")
            else:
                menu_order.append("Désactiver")
            menu_order.extend(['MailAlias', "Shell", "Password", "Supprimer"])
        else:
            menu_order.append("Créer")
        def box(default_item=None):
            return self.dialog.menu(
                "Quel action effectuer sur le compte ?",
                width=0,
                height=0,
                menu_height=0,
                timeout=self.timeout,
                item_help=1,
                default_item=str(default_item),
                title="Gestion du compte de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                scrollbar=True,
                cancel_label="Retour",
                backtitle=self._connected_as(),
                choices=[(k, menu[k]['text'], menu[k]['help']) for k in menu_order if self.has_right(menu_droits[k], proprio)])

        def todo(tag, menu, proprio, self_cont):
            if not tag in menu_order:
                raise Continue(self_cont)
            elif 'callback' in menu[tag]:
                raise Continue(TailCall(menu[tag]['callback'], cont=self_cont, proprio=proprio))
            elif 'attribut' in menu[tag]:
                raise Continue(TailCall(self.modif_proprio_attributs, proprio=proprio, cont=self_cont, attr=menu[tag]['attribut'].ldap_name))
            else:
                raise EnvironmentError("Il n'y a ni champ 'attribut' ni 'callback' pour le tag %s" % tag)


        cont(**{update_obj:proprio})
        (code, tag) = self.handle_dialog(cont, box, default_item)
        self_cont = TailCall(self.proprio_compte, proprio=proprio, cont=cont, default_item=tag_translate.get(tag, tag), update_obj=update_obj)
        return self.handle_dialog_result(
            code=code,
            output=tag,
            cancel_cont=cont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [tag, menu, proprio, self_cont])]
        )

    @tailcaller
    def proprio_vente_set(self, article, cont):
        """Permet de définir la quantité de l'article à vendre"""
        def box():
            if article['pu'] == '*':
                return self.dialog.inputbox(title="Montant pour %s ?" % article['designation'],
                    text="", init=str(article.get('nombre','')), timeout=self.timeout, width=70)
            else:
                return self.dialog.inputbox(title="Nombre de %s ?" % article['designation'],
                    text="", timeout=self.timeout, init=str(article.get('nombre','1')), width=70)
        def todo(article, output, cont):
            article['nombre']=output
            # Il faut entrer quelque chose
            if not output:
                raise ValueError("Merci d'entrer une valeur")
            # Vérification des centimes
            if article['pu'] == '*' and '.' in output:
                if len(output.split('.', 1)[1])>2:
                    raise ValueError("Les centimes, c'est seulement deux chiffres après la virgule")
            typ = float if article['pu'] == '*' else int
            # Vérification du type de l'entré
            try:
                output=typ(output)
            except ValueError:
                raise ValueError("Merci d'entrez seulement des nombres")
            # On définis le nombre d'entrée. Pour pu=* il y aura du trairement à faire
            # avant de générer la facture : mettre pu à nombre et nombre à 1
            # on le met comme ça pour pouvoir naviger aisément entre les écrans dialog
            article['nombre'] = output
            return article

        (code, output) = self.handle_dialog(cont, box)
        self_cont = TailCall(self.proprio_vente_set, article=article, cont=cont)
        return self.handle_dialog_result(
            code=code,
            output=output,
            cancel_cont=cont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [article, output, cont])]
        )

    @tailcaller
    def proprio_choose_paiement(self, proprio, cont, cancel_cont, articles=[], tag_paiment=None, comment=True, text=""):
        """Pour choisir un mode de paiement.
           Si `articles` est donné, empêche le paiement par solde si soldes est dans articles
           La continuation `cont` doit accepter en paramètre `tag_paiment` pour le mode de paiement
           et `comment_paiement` pour un commentaire.
        """
        box_paiement = {
            "liquide" : "Espèces",
            "cheque" : "Chèque",
            "carte" : "Par carte bancaire",
            "solde" : "Solde Crans (actuel : %s€)",
            "note" : "Note kfet (attention, moins tracable...)",
            "arbitraire" : "Création ou destruction magique d'argent.",
        }

        def box_choose_paiment(tag, articles):
            box_paiement_order = ["liquide", "cheque", "carte","note"]
            if "cransAccount" in proprio['objectClass']:
                if not "SOLDE" in [art['code'] for art in articles] and proprio["solde"]:
                    box_paiement_order.append("solde")
                    box_paiement["solde"] = box_paiement["solde"] % proprio["solde"][0]
                if len(articles) == 1 and "SOLDE" in [art['code'] for art in articles]:
                    box_paiement_order.append("arbitraire")

            choices = []
            for key in box_paiement_order:
                choices.append((key, box_paiement[key], 1 if key == tag else 0))
            return self.dialog.radiolist(
                  text=text,
                  title="Choix d'un mode de paiement pour %s %s" % (proprio.get("prenom", [''])[0], proprio["nom"][0]),
                  choices=choices,
                  width=70,
                  timeout=self.timeout)

        def choose_paiment(tag_paiement, proprio, self_cont, cont):
            if not tag_paiement:
                raise ValueError("Il faut choisir un moyen de paiement")
            if comment:
                code, comment_paiement = self.dialog.inputbox(text="Détails pour les espèces, nom de la note ou banque du chèque", title="Commentaire", width=70, timeout=self.timeout)
                if code != self.dialog.DIALOG_OK:
                    raise Continue(self_cont)
                if not comment_paiement:
                    raise ValueError("Commentaire nécessaire")
            raise Continue(cont(tag_paiment=tag_paiement, comment_paiement=comment_paiement))

        self_cont=TailCall(self.proprio_choose_paiement, proprio=proprio, cont=cont, cancel_cont=cancel_cont, tag_paiment=tag_paiment, comment=comment)
        (code, tag) = self.handle_dialog(cancel_cont, box_choose_paiment, tag_paiment, articles)
        self_cont=self_cont(tag_paiment=tag)
        return self.handle_dialog_result(
          code=code,
          output=tag,
          cancel_cont=cancel_cont,
          error_cont=self_cont,
          codes_todo=[([self.dialog.DIALOG_OK], choose_paiment, [tag, proprio, self_cont, cont])]
        )


    @tailcaller
    def proprio_vente(self, proprio, cont, tags=[], tag_paiment=None, to_set=[], have_set=[], comment_paiement=None):
        """Menu de vente du crans. Permet également de recharger le solde crans"""

        def box_choose_item(tags):
            choices = []
            gestion.config.factures.ITEMS.update(gestion.config.factures.ITEM_SOLDE)
            for code, article in gestion.config.factures.ITEMS.items():
                choices.append((code, u"%s%s" % (article['designation'], (u' (%s€)' % article['pu']) if article['pu'] != '*' else ""), 1 if code in tags else 0))
            return self.dialog.checklist(
                  text="",
                  title="Vente de truc à %s %s" % (proprio.get("prenom", [''])[0], proprio["nom"][0]),
                  choices=choices,
                  width=120,
                  timeout=self.timeout)

        def choose_item(proprio, tags, articles, self_cont):
            to_set=[]
            for tag in tags:
                articles[tag]['code']=tag
                to_set.append(articles[tag])
            raise Continue(self_cont(to_set=to_set, have_set=[]))

        def number_of_items(to_set, have_set, self_cont):
            # Où faut-il aller si l'utilisateur appuis sur annuler
            if not have_set:
                lcont = self_cont(to_set=[])
            else:
                lcont = self_cont(to_set=[have_set[-1]] + to_set, have_set=have_set[:-1])
            art = self.proprio_vente_set(to_set[0], cont=lcont)
            if not to_set[1:]:
                total = 0
                line=1
                text=u"Résumé :\n"
                for article in have_set + [art]:
                    if article['pu'] == '*':
                        total += article['nombre']
                        text+=u" * %s pour %s€\n" % (article['designation'], article['nombre'])
                    else:
                        total += article['nombre']*article['pu']
                        text+=u" * %dx %s à %s€\n" % (article['nombre'], article['designation'], article['pu'])
                    line+=1
                text+=u"Total à payer : %s€" % total
                self.dialog.msgbox(text=text,
                  title="Résumé de la facture à payer",
                  width=70, height=5+line, timeout=self.timeout)
            return self_cont(to_set=to_set[1:], have_set=have_set + [art])

        def paiement(have_set, tag, proprio, comment, cancel_cont, cont):
            articles = copy.deepcopy(have_set)

            # On formate les articles
            for article in articles:
                if article['pu'] == '*':
                    article['pu'] = article['nombre']
                    article['nombre'] = 1

            # En arbitraire, on accepte que le solde
            if tag == u"arbitraire":
                if len(articles) > 1 or "SOLDE" not in [art['code'] for art in articles]:
                    raise ValueError("Il n'est possible que de faire une opération de solde en mode arbitraire")

            # Les articles classiques on facture
            with self.conn.newFacture(proprio.dn, {}) as facture:
                facture['modePaiement']=unicode(tag, 'utf-8')
                facture['article']=articles
                facture['info']=unicode(comment, 'utf-8')
                if self.confirm_item(item=facture,
                    text=u"Le paiement de %s€ a-t-il bien été reçu (mode : %s) ?\n" % (facture.total(), tag),
                    title=u"Validation du paiement",
                    timeout=self.timeout):
                    # Appeler créditer va créditer ou débiter le solde, sauver le proprio et créer la facture
                    facture.crediter()
                    arts = ["%s %s" % (art['nombre'], art['designation']) for art in facture['article'] if art['code'] != 'SOLDE']
                    if arts:
                        self.dialog.msgbox(
                        text=u"Vous pouvez remettre à l'adherent les articles (si ce sont des articles) suivant :\n * %s" % '\n * '.join(arts),
                        title=u"Vente terminée",
                        width=0, height=0, timeout=self.timeout)
                    if tag == "solde":
                        self.dialog.msgbox(text=u"Le solde de l'adhérent a bien été débité", title="Solde débité", width=0, height=0, timeout=self.timeout)
                    if [a for a in facture['article'] if art['code'] == 'SOLDE']:
                        self.dialog.msgbox(text=u"Le solde de l'adhérent a bien été crédité", title="Solde crédité", width=0, height=0, timeout=self.timeout)
                else:
                    if not self.confirm(text=u"Le paiement n'a pas été reçue.\n Annuler la vente ?", title="Annulation de la vente", defaultno=True):
                        raise Continue(cancel_cont)
            raise Continue(cont)

        self_cont=TailCall(self.proprio_vente, proprio=proprio, cont=cont, tags=tags, tag_paiment=tag_paiment, to_set=to_set, have_set=have_set, comment_paiement=comment_paiement)
        # S'il y a des article dont il faut définir la quantité
        if to_set:
            return self.handle_dialog_result(
              code=self.dialog.DIALOG_OK,
              output=None,
              cancel_cont=None,
              error_cont=self_cont,
              codes_todo=[([self.dialog.DIALOG_OK], number_of_items, [to_set, have_set, self_cont])]
          )
        # Sinon, si tous les quantités de tous les articles sont définis
        elif have_set and (not comment_paiement or not tag_paiment):
            lcont = self_cont.copy()
            lcont(to_set=[have_set[-1]] + to_set, have_set=have_set[:-1])
            return self.proprio_choose_paiement(proprio=proprio, cont=self_cont, cancel_cont=lcont, articles=have_set, tag_paiment=tag_paiment)
        # Et si on a choisit le mode de paiement
        elif have_set and comment_paiement:
            cancel_cont = self_cont(comment_paiement=None)
            return self.handle_dialog_result(
              code=self.dialog.DIALOG_OK,
              output=[],
              cancel_cont=cancel_cont,
              error_cont=cancel_cont,
              codes_todo=[([self.dialog.DIALOG_OK], paiement, [have_set, tag_paiment, proprio, comment_paiement, cancel_cont, cont])]
          )

        # Sinon, on propose des articles à chosir
        else:
            (code, tags) = self.handle_dialog(cont, box_choose_item, tags)
            self_cont=self_cont(tags=tags, have_set=[], to_set=[], tag_paiment=None)
            gestion.config.factures.ITEMS.update(gestion.config.factures.ITEM_SOLDE)
            return self.handle_dialog_result(
              code=code,
              output=tags,
              cancel_cont=cont,
              error_cont=self_cont,
              codes_todo=[([self.dialog.DIALOG_OK], choose_item, [proprio, tags, copy.deepcopy(gestion.config.factures.ITEMS), self_cont])]
          )

    @tailcaller
    def proprio_chambre_campus(self, success_cont, cont, proprio, create=False):
        """Permet de faire déménager d'adhérent sur le campus"""
        def box():
            return self.dialog.inputbox(
                    "chambre ?",
                    title="%s de %s %s" % ("Création" if create else "Déménagement", proprio.get('prenom', [''])[0], proprio["nom"][0]),
                    cancel_label="Retour",
                    width=50, timeout=self.timeout,
                   )

        def expulse_squatteur(proprio, chbre):
            with self.conn.search(u"chbre=%s" % chbre, mode='rw')[0] as squatteur:
                if self.confirm_item(
                    item=squatteur,
                    title="Chambre occupée",
                    defaultno=True,
                    text=u"L'adhérent ci-dessous occupé déjà la chambre %s :\n" % output,
                    text_bottom=u"\nPasser la chambre de cet adhérent en chambre inconnue ?",
                    disp_adresse=True,
                  ):
                    squatteur['chbre'] = u'????'
                    squatteur.validate_changes()
                    squatteur.history_gen()
                    squatteur.save()
                    return True
                else:
                    return False

        def set_chambre(proprio, chbre):
            try:
                if isinstance(proprio, objets.adherent):
                    proprio['postalAddress'] = []
                proprio['chbre'] = unicode(output, 'utf-8')
            except UniquenessError:
                if expulse_squatteur(proprio, chbre):
                    # La chambre est maintenant normalement libre
                    proprio['chbre'] = unicode(output, 'utf-8')
                else:
                    raise Continue(self_cont)
            return proprio


        def todo(chbre, proprio, self_cont, success_cont):
            if not output:
                raise Continue(self_cont)
            if output == "????":
                raise ValueError("Chambre ???? invalide")
            if create:
                return set_chambre(proprio, chbre)
            else:
                with self.conn.search(dn=proprio.dn, scope=0, mode='rw')[0] as proprio:
                    proprio = set_chambre(proprio, chbre)
                    proprio.validate_changes()
                    proprio.history_gen()
                    proprio.save()
                self.display_item(item=proprio, title="Adhérent déménagé dans la chambre %s" % output, disp_adresse=True)
                raise Continue(success_cont())

        (code, output) = self.handle_dialog(cont, box)
        self_cont = TailCall(self.proprio_chambre_campus, proprio=proprio, success_cont=success_cont, cont=cont, create=create)
        return self.handle_dialog_result(
            code=code,
            output=output,
            cancel_cont=cont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [output, proprio, self_cont, success_cont])]
        )

    @tailcaller
    def proprio_chambre_ext(self, keep_machine, keep_compte, success_cont, cont, proprio, create=False):
        """Permet de faire déménager l'adhérent hors campus"""
        if keep_machine and not keep_compte:
            raise EnvironmentError("On ne devrait pas supprimer un compte crans et y laisser des machines")
        elif keep_machine and keep_compte:
            def box(values={}):
                form = [("Adresse", 40), ("Compl. adr.", 40), ("Code postal", 7), ("Ville", 16)]
                fields = [("%s :" % k, values.get(k, ""), l, 50) for (k, l) in form]
                return self.dialog.form(
                    text="",
                    timeout=self.timeout,
                    height=0, width=0, form_height=0,
                    fields=fields,
                    title="Paramètres machine",
                    backtitle="Gestion des machines du Crans")
            def todo(output, proprio, success_cont, cont):
                if not create:
                    if self.dialog.yesno("changer l'adresse de l'adhérent pour %s ?" % ", ".join([o for o in output if o]),
                           title=u"Déménagement de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                           defaultno=True, timeout=self.timeout) == self.dialog.DIALOG_OK:
                        with self.conn.search(dn=proprio.dn, scope=0, mode='rw')[0] as proprio:
                            if isinstance(proprio, objets.adherent):
                                proprio['postalAddress'] = [unicode(pa, 'utf-8') for pa in output]
                            proprio['chbre'] = u'EXT'
                            proprio.validate_changes()
                            proprio.history_gen()
                            proprio.save()
                            self.display_item(item=proprio, title="Adhérent déménégé hors campus, machines conservées")
                            raise Continue(success_cont())
                    else:
                        raise Continue(cont)
                else:
                    proprio['postalAddress'] = [unicode(pa, 'utf-8') for pa in output]
                    proprio['chbre'] = u'EXT'
                    return proprio
        elif not keep_machine and keep_compte:
            if create:
                raise EnvironmentError("On ne crée pas un adhérent en lui supprimant des machines")
            def box(values={}):
                return (self.dialog.DIALOG_OK, "")
            def todo(output, proprio, success_cont, cont):
                if self.confirm_item(
                    item=proprio,
                    text=u"Supprimer toutes les machines de l'adhérent ci-dessous ?\n\n",
                    text_bottom=u"\nLa chambre de l'adhérent passera de plus en EXT.",
                    title=u"Déménagement de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                    defaultno=True):
                    with self.conn.search(dn=proprio.dn, scope=0, mode='rw')[0] as proprio:
                        for machine in proprio.machines():
                            with machine:
                                machine.delete()
                        proprio['chbre'] = u'EXT'
                        proprio.validate_changes()
                        proprio.history_gen()
                        proprio.save()
                    self.display_item(item=proprio, title="Adhérent déménégé hors campus, machines supprimées")
                    raise Continue(success_cont())
                else:
                    raise Continue(cont)
        elif not keep_machine and not keep_compte:
            if create:
                raise EnvironmentError("On ne crée pas un adhérent en lui supprimant des machines et un compte")
            def box(values={}):
                return (self.dialog.DIALOG_OK, "")
            def todo(output, proprio, success_cont, cont):
                if proprio.get('solde', [0])[0] > 0:
                    self.dialog.msgbox("Solde de l'adhérent %s€ strictement positif, impossible de supprimer le compte\nRepasser le solde à 0€ pour supprimer le compte." % proprio.get('solde', [0])[0],
                        title=u"Déménagement de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                        width=50, timeout=self.timeout)
                    raise Continue(cont)
                elif self.confirm_item(
                    item=proprio,
                    text=u"Supprimer toutes les machines et du compte crans de l'adhérent ci-dessous ?\n\n",
                    text_bottom=u"\nLa chambre de l'adhérent passera de plus en EXT.",
                    title=u"Déménagement de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                    defaultno=True):
                    with self.conn.search(dn=proprio.dn, scope=0, mode='rw')[0] as proprio:
                        for machine in proprio.machines():
                            with machine:
                                machine.delete()
                        proprio['chbre'] = u'EXT'
                        proprio.validate_changes()
                        proprio.history_gen()
                        proprio.save()
                    # On supprime le compte crans (on essaye)
                    def post_deletion(proprio, cont):
                        if not "cransAccount" in proprio['objectClass']:
                            self.display_item(item=proprio, title="Adhérent déménagé hors campus, machines et compte crans supprimées", disp_adresse=True)
                        else:
                            self.display_item(item=proprio, title="Adhérent déménagé hors campus, machines supprimées et compte crans conservé", disp_adresse=True)
                        raise Continue(cont)
                    self.proprio_compte_delete(proprio=proprio, cont=TailCall(post_deletion, proprio=proprio, cont=success_cont()), force=True)
                else:
                    raise Continue(cont)
        else:
            raise EnvironmentError("Impossible, on a fait tous les cas, python est buggué")

        (code, output) = self.handle_dialog(cont, box)
        self_cont = TailCall(self.proprio_chambre_ext, proprio=proprio, keep_machine=keep_machine, keep_compte=keep_compte, success_cont=success_cont, cont=cont)
        return self.handle_dialog_result(
            code=code,
            output=output,
            cancel_cont=cont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [output, proprio, success_cont, cont])]
        )

    def proprio_chambre(self, proprio, cont, default_item=None):
        """Permet de faire déménager d'adhérent"""
        a = attributs
        menu_droits = {
          '0' : [a.cableur, a.nounou],
          '1' : [a.cableur, a.nounou],
          '2' : [a.cableur, a.nounou],
          '3' : [a.cableur, a.nounou],
        }
        menu = {
         "0": {'text':"Déménagement sur le campus", 'callback':self.proprio_chambre_campus, 'help': "Déménagement vers une chambre sur le campus, on ne demande que le bâtiment et la chambre"},
         "1": {'text':"Déménagement à l'extérieur en conservant les machines", "callback": TailCall(self.proprio_chambre_ext, keep_machine=True, keep_compte=True), "help": "Pour concerver ses machines, il faut donner un adresse postale complète"},
         "2": {'text':"Départ du campus en conservant son compte", "callback":TailCall(self.proprio_chambre_ext, keep_machine=False, keep_compte=True), "help":"Supprime les machines mais concerve le    compte crans, l'adresse passe en EXT sans plus d'information"},
         "3": {'text':"Départ du campus en supprimant son compte", "callback":TailCall(self.proprio_chambre_ext, keep_machine=False, keep_compte=False), "help":"Supprime les machines et le compte       crans, l'adhérent reste dans la base de donnée, il est possible de mettre une redirection du mail crans vers une autre adresse dans bcfg2"},
        }
        menu_order = [str(i) for i in range(4)]
        def box(default_item=None):
            return self.dialog.menu(
                "Quel est le type du déménagement ?",
                width=0,
                height=0,
                menu_height=0,
                timeout=self.timeout,
                item_help=1,
                default_item=str(default_item),
                title="Déménagement de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                scrollbar=True,
                cancel_label="Retour",
                backtitle=self._connected_as(),
                choices=[(k, menu[k]['text'], menu[k]['help']) for k in menu_order if self.has_right(menu_droits[k], proprio)])

        def todo(tag, menu, proprio, self_cont, cont):
            if not tag in menu_order:
                raise Continue(self_cont)
            else:
                raise Continue(TailCall(menu[tag]['callback'], cont=self_cont, proprio=proprio, success_cont=cont))

        (code, tag) = self.handle_dialog(cont, box, default_item)
        self_cont = TailCall(self.proprio_chambre, proprio=proprio, cont=cont, default_item=tag)
        return self.handle_dialog_result(
            code=code,
            output=tag,
            cancel_cont=cont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [tag, menu, proprio, self_cont, cont])]
        )

    def proprio_personnel(self, cont, proprio=None,  fields_attrs={}, sorte=None, make_compte_crans=None, force_create=False):
        """
        Permet d'éditer les nom, prénom et téléphone d'un adhérent, ou de créer un adhérent.
        Il faut encore trouver un moyen de récupérer des valeurs pour les attributs mail et chbre
        """
        a = attributs
        # Quel sont les attributs ldap dont on veut afficher et
        # la taille du champs d'édition correspondant
        to_display = [(a.nom, 30)]
        if isinstance(proprio, objets.adherent) or sorte!='club':
            to_display.extend([(a.prenom, 30), (a.tel, 30), (a.mail, 30)])
        non_empty = [a.nom, a.prenom, a.tel]
        input_type = {'default':0}

        # Quel séparateur on utilise pour les champs multivalué
        separateur = ' '

        def box(make_compte_crans):
            if force_create and proprio is None and fields_attrs and make_compte_crans is not None:
                return (self.dialog.DIALOG_OK, [fields_attrs[a] for (a, l) in to_display], make_compte_crans)
            if proprio:
                attrs = dict((k, [str(a) for a in at]) for (k, at) in proprio.items())
                if 'cransAccount' in proprio['objectClass']:
                    input_type[attributs.mail] = 2
                    to_display.append((attributs.mailExt, 30))
            else:
                attrs = {}
                if make_compte_crans is None:
                    if self.dialog.yesno("Crééra-t-on un compte crans à l'utilisateur ?", timeout=self.timeout, title="Création d'un utilisateur", width=50) == self.dialog.DIALOG_OK:
                        input_type[attributs.mail] = 2
                        make_compte_crans = True
                        to_display.append((attributs.mailExt, 30))
                    else:
                        make_compte_crans = False

            fields = [(
             "%s %s:" % (a.legend, '(optionnel) ' if a.optional else ''),
             fields_attrs.get(a, separateur.join(attrs.get(a.ldap_name, [a.default] if a.default else []))),
             l+1, l,
             input_type.get(a, input_type['default'])
            ) for a,l in to_display]


            (code, tags) = self.dialog.form(
                    text="",
                    timeout=self.timeout,
                    height=0, width=0, form_height=0,
                    fields=fields,
                    title="Création d'un utilisateur" if proprio is None else "Édition des informations de %s %s" % (proprio.get('prenom', [''])[0], proprio['nom'][0]),
                    backtitle="Gestion des adhérents du Crans")
            return (code, tags, make_compte_crans)

        def modif_proprio(proprio, attrs):
            mailmodif = False
            with self.conn.search(dn=proprio.dn, scope=0, mode='rw')[0] as proprio:
                for (key, values) in attrs.items():
                    proprio[key] = values
                    if key == 'mailExt' and any('mailExt' in element for element in proprio.get_modlist()):
                        if values:
                            mailmodif = values[0]
                # On retire les éventuelle bl mail invalide
                # Y compris si on supprime mailExt
                if any('mailExt' in element for element in proprio.get_modlist()) or any('mail' in element for element in proprio.get_modlist()):
                    for bl in proprio['blacklist']:
                        now = int(time.time())
                        if bl['type'] == u'mail_invalide' and bl['fin'] > now:
                            bl['fin'] = now
                            if bl['debut'] > now:
                                bl['debut'] = now
                            bl['comm'] += u'- mail rectifié'
                proprio.validate_changes()
                proprio.history_gen()
                proprio.save()
            if mailmodif:
                if self.dialog.yesno("L'attribut mailExt a été modifié dans la base ldap, synchroniser le .forward?",
                    title="Modification de la redirection mail %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                    timeout=self.timeout
                    ) == self.dialog.DIALOG_OK:
                    subprocess.Popen(["sudo","-n","-u","%s" % proprio['uid'][0],"/usr/scripts/utils/forward.py", "--write", "--mail=%s" % mailmodif, "--name=%s" % proprio['uid'][0]])
            self.dialog.msgbox(
                "Modification prise en compte avec succès",
                title="Modification des informations de %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                width=70, timeout=self.timeout
            )
            return proprio

        def create_proprio(attrs, make_compte_crans, force_create, self_cont, cont, sorte=None):
            if not force_create:
                if attrs.has_key('prenom'):
                    items = self.conn.search("(&(prenom=%s)(nom=%s))" % (attrs['prenom'], attrs['nom']))
                else:
                    items = self.conn.search("(&(nom=%s)(objectClass=club))" % (attrs['nom']))
                if items:
                    newproprio = self.select_one(items, title="Choisir un adhérant existant", text="Des adhérent avec les même noms et prénoms existent déjà, en utiliser un ?\n(Annuler pour continuer la création)", cont=self_cont(make_compte_crans=make_compte_crans, force_create=True, sorte=sorte))
                    raise Continue(cont())
            if sorte=='club':
                new_obj = self.conn.newClub({})
            else:
                new_obj = self.conn.newAdherent({})
            with new_obj as proprio:
                delay = {}
                for (key, values) in attrs.items():
                    try:
                        proprio[key] = values
                    # En cas d'erreur, on a peut être besoin du compte crans
                    except ValueError:
                        delay[key] = values
                print delay
                # on récupère la chambre
                proprio = self.proprio_chambre_campus(success_cont=None, cont=self_cont(make_compte_crans=make_compte_crans, sorte=sorte), proprio=proprio, create=True)
                # Si c'est EXT, on demande une adresse complète
                if 'EXT' in proprio['chbre']:
                    proprio = self.proprio_chambre_ext(keep_machine=True, keep_compte=True, success_cont=None, cont=self_cont(make_compte_crans=make_compte_crans, sorte=sorte), proprio=proprio, create=True)
                # Si compte crans à créer, on le crée.
                # On le met en dernier pour éviter de faire entrez plusieurs fois son mdp à l'adhérent
                # en cas d'erreur de la part du cableur
                if make_compte_crans:
                    proprio = self.proprio_compte_create(proprio=proprio, cont=self_cont(make_compte_crans=None, force_create=False, sorte=sorte), update_obj='adherent', return_obj=True)
                # On réeaffecte les attributs de tout à l'heure
                for (key, values) in delay.items():
                    proprio[key] = values
                # On confirme la création, si club, on affecte le respo
                if sorte=='club':
                    proprio = self.respo_club(proprio, cont, return_obj=True)
                if self.confirm_item(proprio, title="Créer l'utilisateur suivant ?", disp_adresse=True, disp_telephone=True):
                    proprio.validate_changes()
                    proprio.create()
                    if make_compte_crans:
                        if self.dialog.yesno("Imprimer un ticket avec un mot de passe attribué automatiquement ?",
                            title="Impression de ticket pour %s %s" % (proprio.get('prenom', [''])[0], proprio["nom"][0]),
                            timeout=self.timeout
                            ) == self.dialog.DIALOG_OK:
                            subprocess.call(['/usr/scripts/cransticket/dump_creds.py', '--forced', '--pass', 'uid=%s' % proprio['uid'][0]])
                            self.display_item(proprio, "Impression du ticket en cours ...")
                else:
                    proprio = None
            return proprio

        def todo(to_display, non_empty, tags, proprio, separateur, make_compte_crans, force_create, self_cont, cont, sorte=None):
            attrs = {}
            # On traite les valeurs reçues
            for ((a, l), values) in zip(to_display, tags):
                if not values and a in non_empty:
                    raise ValueError(u"%s ne devrait pas être vide" % a.legend)
                values = unicode(values, 'utf-8')
                # Si le champs n'est pas single value, on utilise separateur pour découper
                # et on ne garde que les valeurs non vides
                if not a.singlevalue:
                    values = [v for v in values.split(separateur) if v]
                attrs[a.ldap_name] = values
            if proprio:
                proprio = modif_proprio(proprio, attrs)
            else:
                proprio = create_proprio(attrs, make_compte_crans, force_create, self_cont, cont, sorte=sorte)
            raise Continue(cont(proprio=proprio))


        (code, tags, make_compte_crans) = self.handle_dialog(cont, box, make_compte_crans)
        # On prépare les fiels à afficher à l'utilisateur si une erreure à lieu
        # pendant le traitement des donnée (on n'éfface pas ce qui a déjà été entré
        # c'est au cableur de corriger ou d'annuler
        fields_attrs = dict((a, values) for ((a, l), values) in zip(to_display, tags))
        retry_cont = TailCall(self.proprio_personnel, proprio=proprio, cont=cont,  fields_attrs=fields_attrs)

        return self.handle_dialog_result(
            code=code,
            output=tags,
            cancel_cont=cont,
            error_cont=retry_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [to_display, non_empty, tags, proprio, separateur, make_compte_crans, force_create, retry_cont, cont, sorte])]
        )
