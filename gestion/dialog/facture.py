#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

u"""
Copyright (C)

Gabriel Détraz

Licence : GPLv3

"""
import sys
import time
if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')

import lc_ldap.objets as objets
import lc_ldap.attributs as attributs
import lc_ldap.crans_utils

import proprio
from CPS import TailCall, tailcaller, Continue

from gestion import config

class Dialog(proprio.Dialog):
    def delete_facture(self, cont, del_cont, facture):
        """Permet la suppression d'une facture de la base ldap"""
        if facture is None:
            facture = self.select(["facture"], "Recherche d'une facture pour supression", cont=cont)

        def todo(facture):
            if self.confirm_item(item=facture, title="Voulez vous vraiement supprimer la facture ?", defaultno=True):
                with self.conn.search(dn=facture.dn, scope=0, mode='rw')[0] as facture:
                    facture.delete()
                self.dialog.msgbox("La facture a bien été supprimée", timeout=self.timeout, title="Suppression d'une facture")
                raise Continue(del_cont(proprio=None))
            else:
                raise Continue(cont)

        return self.handle_dialog_result(
            code=self.dialog.DIALOG_OK,
            output="",
            cancel_cont=cont(facture=facture),
            error_cont=cont(facture=facture),
            codes_todo=[([self.dialog.DIALOG_OK], todo, [facture])]
        )


    def modif_facture(self, cont, facture=None, tag=None):
        if facture is None:
            facture = self.select(["facture"], "Recherche d'une facture pour modification", cont=cont)
	a = attributs
	menu_droits = {
            'Controle' : [a.nounou, a.tresorier],
            'Recu' : [a.nounou, a.cableur, a.tresorier],
            'Mode de paiement' : [a.cableur, a.nounou, a.tresorier],
            'Remarques' : [a.cableur, a.nounou, a.tresorier],
            'Supprimer':[a.cableur, a.nounou, a.bureau, a.tresorier],
        }
        menu = {
            'Controle' : {'text' : "Passer le controle à True ou False", "callback":self.facture_controle},
            'Recu' : {'text' : "Passer le paiment recu à True ou False", "callback":self.facture_recu},
            'Mode de paiement' : {'text' : "Changer le mode de paiement", "callback":self.mode_paiement},
            'Remarques' : {'text':'Ajouter ou supprimer une remarque à cette facture', 'attribut':attributs.info},
            'Supprimer' : {'text':"Supprimer la facture de la base de donnée", 'callback':TailCall(self.delete_facture, del_cont=cont(proprio=None))},
        }
        menu_order = ['Controle', 'Remarques', 'Recu', 'Mode de paiement', 'Supprimer']

        def box(default_item=None):
            choices = []
            for key in menu_order:
                if self.has_right(menu_droits[key]):
                    choices.append((key, menu[key]['text'], menu[key].get('help', "")))
            return self.dialog.menu(
                "Que souhaitez vous modifier ?",
                width=0,
                height=0,
                menu_height=0,
                timeout=self.timeout,
                item_help=1,
                default_item=str(default_item),
                title="Modification de la facture %s" % (facture["fid"][0]),
                scrollbar=True,
                cancel_label="Retour",
                backtitle=self._connected_as(),
                choices=choices)

        def todo(tag, menu, facture, cont_ret):
            if not tag in menu_order:
                raise Continue(cont_ret)
            else:
                if 'callback' in menu[tag]:
                    raise Continue(TailCall(menu[tag]['callback'], cont=cont_ret, **{menu[tag].get('facture', 'facture'):facture}))
                elif 'attribut' in menu[tag]:
                    raise Continue(TailCall(self.modif_facture_attributs, facture=facture, cont=cont_ret, attr=menu[tag]['attribut'].ldap_name))
                else:
                    raise EnvironmentError("Il n'y a ni champ 'attribut' ni 'callback' pour le tag %s" % tag)

        (code, tag) = self.handle_dialog(cont, box, tag)
        cont_ret = TailCall(self.modif_facture, cont=cont, facture=facture, tag=tag)

        return self.handle_dialog_result(
            code=code,
            output=tag,
            cancel_cont=cont(),
            error_cont=cont_ret,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [tag, menu, facture, cont_ret])]
        )


    def modif_facture_attributs(self, facture, attr, cont):
        """Juste un raccourci vers edit_attributs spécifique aux facture"""
        return self.edit_attributs(obj=facture, update_obj='facture', attr=attr, title="Modification de la facture %s" % (facture["fid"][0]), cont=cont)

    def mode_paiement(self, facture, cont, update_obj='facture'):
        """ Permet de changer le mode de paiement """
        choice = config.modePaiement
        def box():
            return self.dialog.radiolist(
                     text="",
                     height=0, width=0, list_height=0,
                     choices=[(c, "", 1 if c == facture.get('modePaiement', [''])[0] else 0) for c in choice],
                     title="Facture %s" % (facture['fid'][0]),
                     timeout=self.timeout
                     )
        def todo(mode, facture, self_cont, cont):
            if mode != facture.get('modePaiement', [''])[0]:
                with self.conn.search(dn=facture.dn, scope=0, mode='rw')[0] as facture:
                    try:
                        facture['modePaiement'] = mode
                        facture.history_gen()
                        facture.save()
                        self.dialog.msgbox(text="Mode de paiement changé", title=u"Mode de paiement", width=0, height=0, timeout=self.timeout)
                    except ValueError as e:
                        self.dialog.msgbox(text=unicode(e), title=u"Erreur rencontrée", width=0, height=0, timeout=self.timeout)
            raise Continue(cont(**{update_obj:facture}))

        (code, mode) = self.handle_dialog(cont, box)
        self_cont = TailCall(self.mode_paiement, facture=facture, cont=cont)
        return self.handle_dialog_result(
            code=code,
            cancel_cont=cont,
            output=mode,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [mode, facture, self_cont, cont])]
        )

    def facture_recu(self, facture, cont, update_obj='facture'):
        """ Recu paiment """
        choice = { "Paiement recu" : True,
                   'Invalide' : False,
        }
        def box():
            return self.dialog.radiolist(
                     text="",
                     height=0, width=0, list_height=0,
                     choices=[(c, "", 1 if choice[c] == bool(facture['recuPaiement']) else 0) for c in choice],
                     title="Facture %s" % (facture['fid'][0]),
                     timeout=self.timeout
                     )
        def todo(paiement, facture, self_cont, cont):
            if choice[paiement] != bool(facture['recuPaiement']):
                with self.conn.search(dn=facture.dn, scope=0, mode='rw')[0] as facture:
                    try:
                        if choice[paiement]:
                            facture['recuPaiement'] = lc_ldap.crans_utils.datetime_to_generalized_time_format(lc_ldap.crans_utils.localized_datetime())
                        else:
                            facture['recuPaiement'] = []
                        facture.history_gen()
                        facture.save()
                        self.dialog.msgbox(text="Etat de la facture changée", title=u"Etat de la facture", width=0, height=0, timeout=self.timeout)
                    except ValueError as e:
                        self.dialog.msgbox(text=unicode(e), title=u"Erreur rencontrée", width=0, height=0, timeout=self.timeout)
            raise Continue(cont(**{update_obj:facture}))

        (code, paiement) = self.handle_dialog(cont, box)
        self_cont = TailCall(self.facture_recu, facture=facture, cont=cont)
        return self.handle_dialog_result(
            code=code,
            cancel_cont=cont,
            output=paiement,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [paiement, facture, self_cont, cont])]
        )


    def facture_controle(self, facture, cont, update_obj='facture'):
        """ Controle de la facture """
        choice = { "Aucun": [],
                   "Controle Ok" : "TRUE",
                   'Invalide' : "FALSE",
        }
        def box():
            etat = facture.get('controle', [])
            if etat:
                etat = etat[0]
            return self.dialog.radiolist(
                     text="",
                     height=0, width=0, list_height=0,
                     choices=[(c, "", 1 if choice[c] == etat or not etat and not choice[c] else 0) for c in choice],
                     title="Facture %s" % (facture['fid'][0]),
                     timeout=self.timeout
                     )
        def todo(controle, facture, self_cont, cont):
            with self.conn.search(dn=facture.dn, scope=0, mode='rw')[0] as facture:
                try:
                    facture['controle'] = choice[controle]
                    facture.history_gen()
                    facture.save()
                    self.dialog.msgbox(text="Etat de controle changé", title=u"Controle de facture", width=0, height=0, timeout=self.timeout)
                except ValueError as e:
                    self.dialog.msgbox(text=unicode(e), title=u"Erreur rencontrée", width=0, height=0, timeout=self.timeout)
            raise Continue(cont(**{update_obj:facture}))

        (code, controle) = self.handle_dialog(cont, box)
        self_cont = TailCall(self.facture_controle, facture=facture, cont=cont)
        return self.handle_dialog_result(
            code=code,
            cancel_cont=cont,
            output=controle,
            error_cont=self_cont,
            codes_todo=[([self.dialog.DIALOG_OK], todo, [controle, facture, self_cont, cont])]
        )


