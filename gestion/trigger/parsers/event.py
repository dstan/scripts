#!//usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
#
# Parser for event service. Contains nothing.
#
# Author    : Pierre-Elliott Bécue <becue@crans.org>
# License   : GPLv3
# Date      : 19/11/2014
"""
This parser is not usefull, but still will be imported by service event.py
on civet.
"""
