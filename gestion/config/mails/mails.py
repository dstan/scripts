#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Templates de mails. """


#: Mail envoyé à un adhérent qui obtient de nouveaux droits.
txt_ajout_droits = u"""From: Crans <%(From)s>
To: %(To)s
Subject: [CRANS] Ajout des droits %(Droit)s
Content-Type: text/plain; charset="utf-8"

Bonjour,

Tu viens d'être doté des droits %(Droit)s au Cr@ns.
Une description de ces droits et de la manière
de les utiliser est disponible sur la page :

%(Page)s

Si tu as des questions, n'hésite pas à les poser
à d'autres câbleurs ou aux nounous.

--\u0020
Les nounous"""

#: Pages wiki explicatives sur les droits
pages_infos_droits = { "Cableur"   : "https://wiki.crans.org/%C3%8AtreC%C3%A2bleur",
    "Imprimeur"  : "https://wiki.crans.org/%C3%8AtreC%C3%A2bleur/%C3%8AtreImprimeur",
    "Bureau"     : "https://wiki.crans.org/%C3%8AtreC%C3%A2bleur/%C3%8AtreLeBureau",
    "Moderateur" : "https://wiki.crans.org/CransTechnique/ModerationDesNews"
    }

#: Mail de rappel envoyé à un adhérent ayant des droits mais n'ayant pas encore signé sa charte membre actifs
txt_charte_MA = u"""From: Crans <%(From)s>
To: %(To)s
Subject: [CRANS] Charte des membres actifs
Content-Type: text/plain; charset="utf-8"

Bonjour,

Il semble que tu es membre actif du CRANS et que tu n'as pas
signé la charte des membres actifs. Si tu n'es pas membre actif
ou si tu as signé la charte des membres actifs, merci de nous le
signaler. Sinon, il faudrait signer la charte et nous la rendre
rapidement. Tu peux l'imprimer à partir du fichier suivant :

https://wiki.crans.org/CransAdministratif?action=AttachFile&do=get&target=charte_ma.pdf

Merci par avance,

--\u0020
Le bureau du CRANS"""

#: Mail envoyé à un adhérent à qui on a retiré ses droits.
txt_menage_cableurs = u"""From: Crans <%(From)s>
To: %(To)s
Subject: [CRANS] Suppression de droits
Content-Type: text/plain; charset="utf-8"

Bonjour,

Les droits que tu avais sur ton compte CRANS ont été retirés
car tu ne cotises pas cette année et que tes droits ne semblent
pas être utiles pour une utilisation à distance. Si nous avons
commis une erreur, nous te prions de nous en excuser. Si tu
souhaites par la suite retrouver des droits, nous serons bien sûr
heureux de te les remettre.

Cordialement,

--\u0020
Le bureau du CRANS"""
