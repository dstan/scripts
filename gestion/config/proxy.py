#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Variables de config pour les proxy """


#### Conf nginx des proxy gérées à la main

non_sites_auto = {
    u"discourse.crans.org",
}

max_upload = {
    u"intranet.crans.org" : "160M",
    u"owncloud.crans.org" : "10G",
}

#: Redirection "host": "url"
sites_redirect = {
    "impression.crans.org": "https://intranet.crans.org/impressions",
    "factures.crans.org": "https://intranet.crans.org/factures",
    "accounts.crans.org": "https://intranet.crans.org/compte",
    "intranet2.crans.org": "https://intranet.crans.org",
    "autostatus.crans.org": "https://www.crans.org/CransNounous/AutoStatus",
    "wikipedia.crans.org": "https://wiki.crans.org",
    "crans.org": "https://www.crans.org",
    "install-party.ens-cachan.fr": "https://install-party.crans.org",
    "www.install-party.ens-cachan.fr": "https://install-party.crans.org",
    "adopteunpingouin.crans.org": "https://install-party.crans.org",
    "i-p.crans.org": "https://install-party.crans.org",
    "hostnames-a-m.crans.org": "https://proxy.crans.org",
    "hostnames-n-z.crans.org": "https://proxy.crans.org",
    "task.crans.org": "https://phabricator.crans.org",
}


def server_name_to_cert_name(serveur):
    """
        A un nom de domain, on associe le certificat correspondant.
        Retourne None si le certificat n'est pas trouvé.
    """
    if serveur.endswith(".ens-cachan.fr") or serveur == "ens-cachan.fr":
        return "crans.ens-cachan.fr"
    elif serveur.endswith(".crans.org") or serveur == "crans.org":
        if serveur[0] <= 'm' and serveur != "hostnames-n-z.crans.org":
            return "hostnames-a-m.crans.org"
        else:
            return "hostnames-n-z.crans.org"
    elif serveur.endswith(".crans.fr") or serveur == "crans.fr":
        if serveur[0] <= 'm' and serveur != "hostnames-n-z.crans.fr":
            return "hostnames-a-m.crans.fr"
        else:
            return "hostnames-n-z.crans.fr"
    elif serveur.endswith(".crans.eu") or serveur == "crans.eu":
        if serveur[0] <= 'm' and serveur != "hostnames-n-z.crans.eu":
            return "hostnames-a-m.crans.eu"
        else:
            return "hostnames-n-z.crans.eu"


site_template = """server {
    server_name %(serveur)s;
    include "snippets/proxy-common.conf";

    return 302 https://$host$request_uri;
}

server {
    include "snippets/proxy-common-ssl.conf";
    server_name %(serveur)s;

    ssl_certificate /etc/letsencrypt/live/%(cert_name)s/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/%(cert_name)s/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/%(cert_name)s/chain.pem;
    %(max_body_size)s
    location / {
        proxy_redirect off;
        proxy_pass http://%(proxy_pass)s;
        proxy_set_header Host %(serveur)s;
        proxy_set_header P-Real-IP $remote_addr;
    }

}
"""

site_redirect_template = """server {
    server_name %(serveur)s;
    include "snippets/proxy-common.conf";

    return 302 %(redirect)s$request_uri;
}

server {
    include "snippets/proxy-common-ssl.conf";
    server_name %(serveur)s;

    ssl_certificate /etc/letsencrypt/live/%(cert_name)s/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/%(cert_name)s/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/%(cert_name)s/chain.pem;

    return 302 %(redirect)s$request_uri;
}
"""
