#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Définitions des variables pour le contrôle d'upload. """

#: Intervalle en heures pour le comptage
interval = 24

#: liste des exemptions générales
exempt = [ ['138.231.136.0/21', '138.231.0.0/16'],
           ['138.231.148.0/22', '138.231.0.0/16'] ]

#: Limite en nombre de lignes pour analyse2
analyse_limit = "3000"

#: Template fichier d'analyse
analyse_file_tpl = "/usr/scripts/var/analyse/%s_%s_%s.txt"

#: Période de surveillance pour le max de décos
periode_watch = 30 * 86400

#: limite soft
soft = 2048 # Mio/24h glissantes

#: limite hard
hard = 12288 # Mio/24h glissantes

#: max déconnexions
max_decos = 7

#: expéditeur des mails de déconnexion
expediteur = "disconnect@crans.org"

#: Seuil pour report dans statistiques.py
stats_upload_seuil = 200 * 1024 * 1024 # Mio
pretty_seuil = "%s Mio" % (stats_upload_seuil/1024/1024,)

#: Delta entre deux blacklists (6 minutes)
delta_two_bl = 360
