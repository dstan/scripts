#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Définitions des variables pour la détection des virus. """

#: Nombre de flood par heure pour déclencher une entrée dans la base
flood = 100
#: Nombre d'entrées dans la base pour détecter un virus
virus = 10
