#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
"""Déclaration des items accessibles à la vente (prix coûtant) et générant
une facture.
items est un dictionnaire, dont chaque entrée est composée d'un dictionnaire
ayant une désignation, un prix unitaire, et indique si l'item n'est accessible
qu'aux imprimeurs (par défaut, non)."""

# Les clef sont un code article
ITEMS = {
    'CABLE': {
        'designation': u'Cable Ethernet 5m',
        'pu': 3.,
    },
    'ADAPTATEUR_TrendNet': {
        'designation': u'Adaptateur 10/100 Ethernet/USB-2',
        'pu': 17.,
    },
    'ADAPTATEUR_UGreen': {
        'designation': u'Adaptateur 10/100/1000 Ethernet/USB-3',
        'pu': 14.,
    },
    'RELIURE': {
        'designation': u'Reliure plastique',
        'pu': 0.12,
    },
    'PULL_ZIP_MARK': {
        'designation': u'Zipper marqué',
        'pu': 39.18,
    },
    'PULL_ZIP': {
        'designation': u'Zipper non marqué',
        'pu': 35.8,
    },
    'PULL_MARK': {
        'designation': u'Sweat à capuche marqué',
        'pu': 32.28,
    },
    'PULL': {
        'designation': u'Sweat à capuche non marqué',
        'pu': 28.92,
    },
}

# Utilisé par gest_crans_lc, contient également le rachargement de solde

ITEM_SOLDE = {'SOLDE': {'designation': u'Rechargement de solde', 'pu': u'*'}}

# Dico avec les modes de paiement pour modification du solde

SOLDE = {
    'liquide' : u'Espèces',
    'cheque' : u'Chèque',
    'carte': u'Carte bancaire',
    'note': u'Note Kfet',
    'arbitraire': u'Modification arbitraire du solde',
}

# Dico avec les modes de paiement pour la vente

VENTE = {
    'liquide' : u'Espèces',
    'cheque' : u'Chèque',
    'carte': u'Carte bancaire',
    'note': u'Note Kfet',
    'solde': u'Vente à partir du Solde',
}
