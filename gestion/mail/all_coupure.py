#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

import sys
from gestion.affich_tools import cprint
from gestion import mail
import time
import lc_ldap.shortcuts
import lc_ldap.crans_utils as crans_utils

# Attention, si à True envoie effectivement les mails
SEND=('--do-it' in sys.argv)

ldap_filter=u"(&(finConnexion>=%(date)s)(aid=*)(!(chbre=????)))" % {'date': crans_utils.to_generalized_time_format(time.time())}
#ldap_filter=u"(uid=oudin)"

conn = lc_ldap.shortcuts.lc_ldap_readonly()
dest = conn.search(ldap_filter, sizelimit=2000)

From = 'respbats@crans.org'
print "%d destinataires (Ctrl + C pour annuler l'envoi)" % len(dest)
raw_input()
with mail.ServerConnection() as smtp:
    for adh in dest:
        print "Envoi du mail à %s" % adh.dn
        if SEND:
            smtp.send_template('all_coupure', {'adh': adh, 'From': From,
                                                'lang_info': "English version below"})
            cprint(" Envoyé !")
        else:
            cprint(" (simulé)")

