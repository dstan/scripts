# -*- mode: python; coding: utf-8 -*-
# Source:
# http://stackoverflow.com/questions/18593661/how-do-i-strftime-a-date-object-in-a-different-locale
import locale
import threading

from datetime import datetime
from contextlib import contextmanager


LOCALE_LOCK = threading.Lock()

@contextmanager
def setlocale(name):
    with LOCALE_LOCK:
        saved = locale.setlocale(locale.LC_ALL)
        try:
            current_val = locale.setlocale(locale.LC_ALL, name)
        except:
            current_val = saved
            print "Warning: Failed setting locale %r" % name

        try:
            yield current_val
        finally:
            locale.setlocale(locale.LC_ALL, saved)

