#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

import sys

from gestion import config
from gestion.affich_tools import cprint
from gestion import mail
import lc_ldap.shortcuts
import gestion.config
import gestion.mail as mail_module

# Attention, si à True envoie effectivement les mails
SEND=False
# Tous les gens adhérents l'année dernière, qui ont toujours une chambre sur le
# campus (les autres ont potentiellement répondu à chambres_invalides, donc
# on ne va pas les respammer). On considère aussi les membres actifs,
# invariablement.
ldap_filter=u'(&(|(droits=*)(&(!(chbre=EXT))(!(chbre=????))(chbre=*)(!(chbre=EXT))))(aid=*))'

conn=lc_ldap.shortcuts.lc_ldap_readonly()
mailaddrs=set()
for adh in conn.search(ldap_filter, sizelimit=2000):
    mailaddr = adh.get_mail()
    if not mailaddr:
        print "Skipping %r (no valid mail)" % adh
        continue
    mailaddrs.add(mailaddr)

print "Va envoyer le message à %s personnes." % len(mailaddrs)
if not SEND:
    print "(Simulation only) Mettre la variable SEND à True effectuer l'envoi"
print "Appuyer sur une touche pour continuer."
raw_input()

echecs=[]
with mail_module.ServerConnection() as conn_smtp:
    for To in mailaddrs:
        cprint(u"Envoi du mail à %s" % To)
        mailtxt=mail.generate('install-party', {'To':To}).as_string()
        try:
            if SEND:
                conn_smtp.sendmail("install-party@crans.org", (To,), mailtxt)
        except:
            cprint(u"Erreur lors de l'envoi à %s " % To, "rouge")
            echecs.append(To)

if echecs:
    print "\nIl y a eu des erreurs :"
    print echecs
