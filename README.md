## Sous-dépôts
À cloner pour faire marcher certains scripts

 * `./lc_ldap`
 * `./wifi_new`

## Paquets Debian
 
Ce dépôt est conçu pour fonctionner sous Debian jessie, avec les paquets suivants
installés. Il est probablement possible de le faire marcher sous d'autres
distribution, mais cela n'a pas été testé. Les paquets Debian suivants
sont nécessaires :

 * python-ldap
 * python-netifaces
 * python-psycopg2
 * python-netsnmp
 * python-pyparsing
 * python-markdown
 * python-jinja2
 * python-beautifulsoup
 * python-ipaddr
 * python-passlib
 * python-dateutil
 * python-tz
 * python-netaddr

Pour une installation facile, copier-coller cette liste dans la commande :
`sudo apt-get install $(sed 's/ \* //')`

Les indications d'installation d'une copie de test ainsi que les conventions
de programmation sont explicitées sur la page <a href="CONTRIBUTING.md">de
contribution</a>.
