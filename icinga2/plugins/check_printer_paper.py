#!/usr/bin/python
#-*- coding: utf-8 -*-

"""
    Plugin Nagios vérifiant l'état des bacs de feuilles de l'imprimante
    via SNMP.
    Nécéssite python-netsnmp.
"""

from __future__ import print_function, unicode_literals

import os, sys
from argparse import ArgumentParser

import re

import nagiosplugin

from netsnmp import snmpwalk, Varbind

# Définition des arguments que mange le script
parser = ArgumentParser(description=__doc__)
parser.add_argument('-H', '--hostname', type=unicode, required=True, help="Adresse IP ou nom de l'imprimante")
parser.add_argument('-w', '--warning', type=int, nargs='?', default=100, help="Seuil pour la génération d'un avertissement")
parser.add_argument('-c', '--critical', type=int, nargs='?', default=50, help="Seuil pour la génération d'une alerte")
parser.add_argument('-C', '--community', type=unicode, nargs='?', default="public", help="Paramètre 'community' utilisé pour l'authentification en SNMPv1/SNMPv2c")

if __name__ == "__main__":
    args = parser.parse_args()

# OIDs pour le monitoring de l'imprimante

PAPER_BASE_OID = ".1.3.6.1.2.1.43.8.2.1"

PAPER_OIDs = {
    "description" : PAPER_BASE_OID + ".18",
    "UOM" : PAPER_BASE_OID + ".8",
    "MaxCapacity" : PAPER_BASE_OID + ".9",
    "CurrentLevel" : PAPER_BASE_OID + ".10",
    "Length" : PAPER_BASE_OID + ".4",
    "Height" : PAPER_BASE_OID + ".5",
}

# Définition des UOM

UOMs = {
    '4' : "µm",
    '7' : "impressions",
    '8' : "",
    '11' : "hrs",
    '12' : "oz/1000",
    '13' : "gr/10",
    '14' : "gr/100",
    '15' : "mL",
    '16' : "feet",
    '17' : "meters",
    '18' : "",
    '19' : "%",
}

# Définition des formats de papier

FORMATS = {
    '210000x297000' : 'A4',
    '297000x210000' : 'A4R',
    '420000x297000' : 'A3',
}

def get_status_from_OID(oid, hostname, community):
    """
        Procède à un snmpwalk pour l'hôte spécifié et l'OID donné
    """
    return snmpwalk(
        Varbind(oid),
        Version=2,
        DestHost=hostname,
        Community=community
        )

def clean_snmp_output(iterable):
    """
        Retire les caractères indésirables de la sortie de NetSNMP
    """

    return [ x.replace(b"\x00", "") for x in iterable ]

class Paper(nagiosplugin.Resource):
    """
        Ressource représentant les niveaux de papier dans l'imprimante
    """

    def __init__(self, hostname, community):
        self.hostname = hostname
        self.community = community

    def _get(self, what):
        return clean_snmp_output(get_status_from_OID(PAPER_OIDs[what], self.hostname, self.community))

    def get_uoms(self):
        return ( UOMs[n] for n in self._get("UOM") )

    def get_paper_descriptions(self):
        return [ x.replace("Tray", "Bac") for x in self._get("description") ]

    def get_max_capacities(self):
        return self._get("MaxCapacity")

    def get_current_levels(self):
        return self._get("CurrentLevel")

    def get_paper_formats(self):
        length = self._get("Length")
        height = self._get("Height")

        return [ FORMATS.get("%sx%s" % (l, h), "Inconnu") for l, h in zip(length, height) ]

    def probe(self):
        descs = self.get_paper_descriptions()
        formats = self.get_paper_formats()
        uoms = self.get_uoms()
        max_capacities = self.get_max_capacities()
        curr_levels = self.get_current_levels()

        descs = [ "%s (%s)" % (desc, fmt) for desc, fmt in zip(descs, formats) ]

        for desc, curr_level, uom, max_cap in zip(descs, curr_levels, uoms, max_capacities):
            if int(curr_level) < 0:
                yield nagiosplugin.Metric(desc, curr_level, uom=uom, min=0, max=max_cap, context="uncertain")
            else:
                yield nagiosplugin.Metric(desc, curr_level, uom=uom, min=0, max=max_cap, context="paper")

@nagiosplugin.guarded
def main():
    check = nagiosplugin.Check(
        Paper(args.hostname, args.community),
        nagiosplugin.ScalarContext('paper', "%d:" % args.warning, "%d:" % args.critical),
        nagiosplugin.ScalarContext('uncertain', "~:-3", "~:-2")
    )

    check.main()

if __name__ == '__main__':
    main()
