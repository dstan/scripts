#!/usr/bin/python
#-*- coding: utf-8 -*-

"""
    Plugin Nagios vérifiant l'état des consommables de l'imprimante
    via SNMP.
    Nécéssite python-netsnmp.
"""

from __future__ import print_function, unicode_literals

import os, sys
from argparse import ArgumentParser

import re

import nagiosplugin

from netsnmp import snmpwalk, Varbind

# Définition des arguments que mange le script
parser = ArgumentParser(description="Script vérifiant l'état des consommables d'une imprimante.")
parser.add_argument('-H', '--hostname', type=unicode, required=True, help="Adresse IP ou nom de l'imprimante")
parser.add_argument('-w', '--warning', type=int, nargs='?', default=10, help="Niveau d'avertissement des consommables")
parser.add_argument('-c', '--critical', type=int, nargs='?', default=5, help="Niveau critique des consommables")
parser.add_argument('-C', '--community', type=unicode, nargs='?', default="public", help="Paramètre 'community' utilisé pour l'authentification en SNMPv1/SNMPv2c")
parser.add_argument('--staplers', action='store_true', help="Vérifier l'état des agraffes")
parser.add_argument('--toner', action='store_true', help="Vérifier l'état du toner")

if __name__ == "__main__":
    args = parser.parse_args()

# OIDs pour le monitoring de l'imprimante
COLOR_BASE_OID = ".1.3.6.1.2.1.43.11.1.1"

COLOR_OIDs = {
    "description" : COLOR_BASE_OID + ".6",
    "UOM" : COLOR_BASE_OID + ".7",
    "MaxCapacity" : COLOR_BASE_OID + ".8",
    "CurrentLevel" : COLOR_BASE_OID + ".9",
}

# Définition des UOM

UOMs = {
    '4' : "µm",
    '7' : "impressions",
    '8' : "pages",
    '11' : "hrs",
    '12' : "oz/1000",
    '13' : "gr/10",
    '14' : "gr/100",
    '15' : "mL",
    '16' : "feet",
    '17' : "meters",
    '18' : "",
    '19' : "%",
}

def get_status_from_OID(oid, hostname, community):
    """
        Procède à un snmpwalk pour l'hôte spécifié et l'OID donné
    """
    return snmpwalk(
        Varbind(oid),
        Version=2,
        DestHost=hostname,
        Community=community
        )

def clean_snmp_output(iterable):
    """
        Retire les caractères indésirables de la sortie de NetSNMP
    """
    return [ x.replace(b"\x00", "") for x in iterable ]

class Supplies(nagiosplugin.Resource):
    """
        Ressource représentant les niveaux de toner et d'agrafes dans l'imprimante
    """

    def __init__(self, hostname, community):
        self.hostname = hostname
        self.community = community

    def _get(self, what):
        return clean_snmp_output(get_status_from_OID(COLOR_OIDs[what], self.hostname, self.community))

    def get_uoms(self):
        return ( UOMs[n] for n in self._get("UOM") )

    def get_supply_descriptions(self):
        return self._get("description")

    def get_max_capacities(self):
        return self._get("MaxCapacity")

    def get_current_levels(self):
        return self._get("CurrentLevel")

    def probe(self):
        descs = self.get_supply_descriptions()
        uoms = self.get_uoms()
        max_capacities = self.get_max_capacities()
        curr_levels = self.get_current_levels()

        for desc, curr_level, uom, max_cap in zip(descs, curr_levels, uoms, max_capacities):
            if "Stapler" in desc:
                if args.staplers:
                    pass
                else:
                    continue
                pass

            elif args.toner:
                pass
            else:
                continue

            if "Stapler" in desc:
                yield nagiosplugin.Metric(desc, curr_level, uom=uom, min=0, max=max_cap, context="staplers")
            else:
                if int(curr_level) < 0:
                    curr_level = '0'
                yield nagiosplugin.Metric(desc, curr_level, uom=uom, min=0, max=max_cap, context="toner")

@nagiosplugin.guarded
def main():
    check = nagiosplugin.Check(
        Supplies(args.hostname, args.community),
        nagiosplugin.ScalarContext('toner', "%d:" % args.warning, "%d:" % args.critical),
        nagiosplugin.ScalarContext('staplers', "~:-3", "~:-3"),
    )

    check.main()

if __name__ == '__main__':
    main()
