# -*- mode: python; coding: utf-8 -*-
#
# SHOWCATEGORIES.PY--
#
# Copyright (C) 2008 Antoine Durand-Gasselin
# Author: Antoine Durand-Gasselin <adg@crans.org>
#

def execute (macro, text):
    f = macro.formatter
    code = [f.heading(1,2), "Liste des liens", f.heading(0,2)]
    code += macro.request.page.getPageLinks(macro.request)
    code += [f.heading(1,2), u"Liste des Catégories", f.heading(0,2)]
    code += macro.request.page.getCategories(macro.request)

    return "\n".join(code)
