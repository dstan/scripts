# -*- mode: python; coding: utf-8 -*-
"""
    MoinMoin - ShowAcl macro

    @copyright: 2014 Vincent Le Gallic
    @license: GNU GPL v3

    Pour montrer les ACL effectif de l'utilisateur courant.
"""

from MoinMoin import wikiutil
from MoinMoin.Page import Page

def macro_ShowAcl(macro, args):
    request = macro.request
    _ = macro.request.getText

    pagename = request.themedict["page_name"]
    page = Page(request, pagename)
    acl = page.getACL(request)
    permissions = ["read", "write", "delete", "revert", "admin"]
    permissions = [x for x in permissions if acl.may(request, request.user, x)]
    sRet = "Les ACL de la page : %s\n" % (acl.__dict__,) + "Vos ACL : " + ", ".join(permissions)
    return macro.formatter.text(sRet)


def execute(macro, args):
    try:
        return wikiutil.invoke_extension_function(
                   macro.request, macro_ShowAcl, args, [macro])
    except ValueError, err:
        return macro.request.formatter.text(
                   "<<ShowAcl: %s>>" % err.args[0])

