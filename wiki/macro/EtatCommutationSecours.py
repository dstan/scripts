# -*- mode: python; coding: utf-8 -*-

import sys
sys.path.append('/usr/scripts/secours')
import secours

def Cellule(texte, couleur, f) :
    """
    Retourne le code HTML d'une cellule formattée aver le formatter f
    """
    code  = f.table(1)
    code += f.table_row(1)
    code += f.table_cell(1,{'style':'background-color:%s' % couleur })
    code += f.text(texte)
    code += f.table_cell(0)
    code += f.table_row(0)
    code += f.table(0)
    return code

def execute(macro, text) :
    try :
        f = open(secours.ETAT_MAITRE)
        f.readline()
        if f.readline().strip() == 'auto' :
            return "" #Cellule('La commutation automatique de l\'état est activée.','lime',macro.formatter)
        else :
            return Cellule(u'La commutation automatique de l\'état a été désactivée.','red',macro.formatter)
    except :
        return Cellule(u'Impossible de déterminer le type de commutation.','yellow',macro.formatter)
