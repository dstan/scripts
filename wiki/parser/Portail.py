# -*- mode: python; coding: utf-8 -*-
"""
                                            ..
                       ....  ............   ........
                     .     .......   .            ....  ..
                   .  ... ..   ..   ..    ..   ..... .  ..
                   .. .. ....@@@.  ..  .       ........  .
              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....
         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....
       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    ..
     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  .....
    ...@@@.... @@@    .@@.......... ........ .....        ..
   . ..@@@@.. .         .@@@@.   .. .......  . .............
  .   ..   ....           ..     .. . ... ....
 .    .       ....   ............. .. ...
 ..  ..  ...   ........ ...      ...
  ................................

MoinMoin - Portail parser

    PURPOSE:
        Pour afficher un portail a la wikipedia.

    CALLING SEQUENCE:
      {{{
      #!Portail
      image1.png @@ title1 @@ description 1
      image2.png @@ title2 @@ description 2
      image3.png @@ title3 @@ description 3
      }}}

    Copyright 2008,2009   Antoine Durand-Gasselin <adg@crans.org>

"""
import urllib
from MoinMoin import wikiutil
from MoinMoin.action import AttachFile

#####################################################################
# Parser :  classe principale, c'est elle qui fait tout
#######
class Parser:
    def __init__(self, raw, request, **kw):
        self.request = request
        self.form = request.form
        self.parser = wikiutil.searchAndImportPlugin(request.cfg, "parser", request.page.pi['format'])
        self.raw = raw

    def format(self, formatter):
        entries = self.parse(formatter)
        self.request.write(self.request.formatter.rawHTML(self.crans_portal(entries)))

    def parse(self, formatter):
        for line in self.raw.split('\n'):
            items=line.split('@@',2)
            if len(items) >2:
                pic = items[0].strip()
                title = wikiutil.renderText(self.request, self.parser, items[1])
                body = wikiutil.renderText(self.request, self.parser, items[2])

                yield (( pic, title, body ))

    def crans_portal(self, entries):
        html = [ '\n\n<div class="portal" style="padding: 0 0 2em 0;">' ]

        format_pne = '''
    <div style="padding: 0 20px 0 0; float: left; clear: none;
                display: table; height: 5em; width: 46%%;">
       <div style="padding: 0; overflow: hidden ; display: table-cell; width: 60px; vertical-align: middle;">
          %s </div>
       <div style="padding: 0; display: table-cell; vertical-align: middle;">
         %s %s </div>
    </div>
'''

        for pic0, tit0, bod0 in entries:
            pic = wikiutil.renderText(self.request, self.parser, '{{attachment:%s}}' % pic0)
            html += [ format_pne % (pic, tit0, bod0) ]

        html += [ '\n</div>\n<hr style="visibility:hidden; clear: both">\n\n' ]
        return('\n'.join(html))
