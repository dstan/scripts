#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
"""
    MoinMoin monobook theme.

    Author: Antoine Durand-Gasselin <adg@crans.org>
"""

import datetime
from MoinMoin.theme import ThemeBase

class Theme(ThemeBase):

    name = "crans"
    Name = name.capitalize()
    today = datetime.date.today()
    _ = lambda x: x
    icons = {
        # key         alt                        icon filename      w   h
        # ------------------------------------------------------------------
        # navibar
        'help':        ("%(page_help_contents)s", "moin-help.png",   16, 16),
        'find':        ("%(page_find_page)s",     "moin-search.png", 16, 16),
        'diff':        (_("Diffs"),               "moin-diff.png",   16, 16),
        'info':        (_("Info"),                "moin-info.png",   16, 16),
        'edit':        (_("Edit"),                "moin-edit.png",   16, 16),
        'unsubscribe': (_("Unsubscribe"),         "moin-unsubscribe.png", 16, 16),
        'subscribe':   (_("Subscribe"),           "moin-subscribe.png", 16, 16),
        'raw':         (_("Raw"),                 "moin-raw.png",    16, 16),
        'xml':         (_("XML"),                 "moin-xml.png",    20, 13),
        'print':       (_("Print"),               "moin-print.png",  16, 16),
        'view':        (_("View"),                "moin-show.png",   16, 16),
        'home':        (_("Home"),                "moin-home.png",   16, 16),
        'up':          (_("Up"),                  "moin-parent.png", 16, 16),
        # FileAttach
        'attach':      ("%(attach_count)s",       "moin-attach.png", 16, 16),
        'attachimg':   ("",                       "attach.png",      32, 32),
        # RecentChanges
        'rss':         (_("[RSS]"),               "moin-rss.png",    16, 16),
        'deleted':     (_("[DELETED]"),           "moin-deleted.png", 16, 16),
        'updated':     (_("[UPDATED]"),           "moin-updated.png", 16, 16),
        'renamed':     (_("[RENAMED]"),           "moin-renamed.png", 16, 16),
        'conflict':    (_("[CONFLICT]"),          "moin-conflict.png", 16, 16),
        'new':         (_("[NEW]"),               "moin-new.png",    16, 16),
        'diffrc':      (_("[DIFF]"),              "moin-diff.png",   16, 16),
        # General
        'bottom':      (_("[BOTTOM]"),            "moin-bottom.png", 16, 16),
        'top':         (_("[TOP]"),               "moin-top.png",    16, 16),
        'www':         ("[WWW]",                  "moin-www.png",    16, 16),
        'mailto':      ("[MAILTO]",               "moin-email.png",  16, 16),
        'news':        ("[NEWS]",                 "moin-news.png",   16, 16),
        'telnet':      ("[TELNET]",               "moin-telnet.png", 16, 16),
        'ftp':         ("[FTP]",                  "moin-ftp.png",    16, 16),
        'file':        ("[FILE]",                 "moin-ftp.png",    16, 16),
        # search forms
        'searchbutton': ("[?]",                   "moin-search.png", 16, 16),
        'interwiki':   ("[%(wikitag)s]",          "moin-inter.png",  16, 16),

        # smileys (this is CONTENT, but good looking smileys depend on looking
        # adapted to the theme background color and theme style in general)
        #vvv    ==      vvv  this must be the same for GUI editor converter
        'X-(':         ("X-(",                    "angry.png",       16, 16),
        ':D':          (":D",                  "biggrin.png",     16, 16),
        '<:(':         ("<:(",                 "frown.png",       16, 16),
        ':o':          (":o",                  "redface.png",     16, 16),
        ':(':          (":(",                  "sad.png",         16, 16),
        ':)':          (":)",                  "smile.png",       16, 16),
        'B)':          ("B)",                  "smile2.png",      16, 16),
        ':))':         (":))",                 "smile3.png",      16, 16),
        ';)':          (";)",                  "smile4.png",      16, 16),
        '/!\\':        ("/!\\",                "alert.png",       16, 16),
        '<!>':         ("<!>",                 "attention.png",   16, 16),
        '(!)':         ("(!)",                 "idea.png",        16, 16),

        # copied 2001-11-16 from http://pikie.darktech.org/cgi/pikie.py?EmotIcon
        ':-?':         (":-?",                 "tongue.png",      16, 16),
        ':\\':         (":\\",                 "ohwell.png",      16, 16),
        '>:>':         (">:>",                 "devil.png",       16, 16),
        '|)':          ("|)",                  "tired.png",       16, 16),

        # some folks use noses in their emoticons
        ':-(':         (":-(",                 "sad.png",         16, 16),
        ':-)':         (":-)",                 "smile.png",       16, 16),
        'B-)':         ("B-)",                 "smile2.png",      16, 16),
        ':-))':        (":-))",                "smile3.png",      16, 16),
        ';-)':         (";-)",                 "smile4.png",      16, 16),
        '|-)':         ("|-)",                 "tired.png",       16, 16),

        # version 1.0
        '(./)':        ("(./)",                "checkmark.png",   16, 16),
        '{OK}':        ("{OK}",                "thumbs-up.png",   16, 16),
        '{X}':         ("{X}",                 "icon-error.png",  16, 16),
        '{i}':         ("{i}",                 "icon-info.png",   16, 16),
        '{1}':         ("{1}",                 "prio1.png",       15, 13),
        '{2}':         ("{2}",                 "prio2.png",       15, 13),
        '{3}':         ("{3}",                 "prio3.png",       15, 13),

        # version 1.3.4 (stars)
        # try {*}{*}{o}
        '{*}':         ("{*}",                 "star_on.png",     16, 16),
        '{o}':         ("{o}",                 "star_off.png",    16, 16),    
    }

    def navibar(self, d):
        """ Assemble the navibar

        @param d: parameter dictionary
        @rtype: unicode
        @return: navibar html
        """
        request = self.request
        found = {} # pages we found. prevent duplicates
        items = [] # navibar items
        item = u'<li class="%s">%s</li>'
        current = d['page_name']

        # Process config navi_bar
        if request.cfg.navi_bar:
            for text in request.cfg.navi_bar:
                pagename, link = self.splitNavilink(text)
                if pagename == current:
                    cls = 'wikilink current'
                else:
                    cls = 'wikilink'
                items.append(item % (cls, link))
                found[pagename] = 1

        # Add current page at end of local pages
        if not current in found:
            title = d['page'].split_title()
            title = self.shortenPagename(title)
            link = d['page'].link_to(request, title)
            cls = 'current'
            items.append(item % (cls, link))

        # Assemble html
        items = u''.join(items)
        html = u'''
<ul id="navibar">
%s
</ul>
''' % items
        return html



    def wikipanel(self, d):
        """ Create wiki panel """
        _ = self.request.getText
        html = [
            u'<div class="sidepanel">',
            u'<h1>%s</h1>' % _("Wiki"),
            self.navibar(d),
            u'</div>',
            ]
        return u'\n'.join(html)

    def pagepanel(self, d):
        """ Create page panel """
        _ = self.request.getText
        if self.shouldShowEditbar(d['page']):
            html = [
                u'<div class="sidepanel">',
                u'<h1>%s</h1>' % _("Page"),
                self.editbar(d),
                u'</div>',
                ]
            return u'\n'.join(html)
        return ''

    def userpanel(self, d):
        """ Create user panel """
        _ = self.request.getText

        html = [
            u'<div class="sidepanel">',
            u'<h1>%s</h1>' % _("User"),
            self.username(d),
            u'</div>'
            ]
        return u'\n'.join(html)

    def header(self, d):
        """
        Assemble page header

        @param d: parameter dictionary
        @rtype: string
        @return: page header html
        """
        _ = self.request.getText

        if self.today.month == 10 and self.today.day == 31:
            x_class = 'class="halloween"'
        elif self.today.month == 12 and self.today.day == 24:
            x_class = 'class="noel"'
        elif self.today.month == 4 and self.today.day ==1:
            x_class = 'class="april_fool"'
        else: x_class = ''

        html = [
            # Custom html above header
            self.emit_custom_html(self.cfg.page_header1),

            u'<div id="titleBar">',
            u'<h1 id="title" %s>' % x_class, self.title(d), u'</h1>',
            u'</div>',
            self.msg(d),
            self.startPage()
            ]

        return u'\n'.join(html)

    editorheader = header

    def footer(self, d, **keywords):
        """ Assemble wiki footer

        @param d: parameter dictionary
        @keyword ...:...
        @rtype: unicode
        @return: page footer html
        """

        page = d['page']

        html = [
            self.pageinfo(page),
            self.endPage(),

            # Sidebar
            u'<div id="rightSidebar">',
            self.userpanel(d), self.wikipanel(d), self.pagepanel(d),
            self.searchform(d), self.credits(d), self.showversion(d, **keywords),
            u'</div>',

            # # Pre footer custom html (not recommended!)
            # self.emit_custom_html(self.cfg.page_footer1),
            # 
            # # Footer
            # u'<div id="footer">',
            # u'</div>',
            # 
            # # Post footer custom html
            # self.emit_custom_html(self.cfg.page_footer2),
            ]
        return u'\n'.join(html)

def execute(request):
    """ Generate and return a theme object

    @param request: the request object
    @rtype: MoinTheme
    @return: Theme object
    """
    return Theme(request)
