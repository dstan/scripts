# -*- coding: utf8 -*-
""" Cr@ns logging : logging utilities for cr@ns scripts
"""
import logging
import os

LOG_FOLDER = "/var/log/crans/"
__version__ = "0.1"

def getFileLogger(name):
    LOGGER.warning("getFileLogger is deprecated, use CransFileHandler instead.")
    logger = logging.getLogger(name)
    hdlr = CransFileHandler(name)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    return logger

class CransFileHandler(logging.FileHandler):
    def __init__(self, name):
        filepath = os.path.join(LOG_FOLDER, name + ".log")
        logging.FileHandler.__init__(self, filepath)
        formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')
        self.setFormatter(formatter)

### Un peu de configuration
# On log systematiquement les warning, error, exception sous "crans"
# sur l'ecran.
CRANSLOGGER = logging.getLogger("crans")
CRANSLOGGER.setLevel(logging.WARNING)
streamhdlr = logging.StreamHandler()
streamhdlr.setLevel(logging.ERROR)
streamhdlr.setFormatter(logging.Formatter('%(levelname)s: %(message)s'))
CRANSLOGGER.addHandler(streamhdlr)
CRANSLOGGER.addHandler(CransFileHandler("crans"))

LOGGER = logging.getLogger("crans.logging")
