#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
# #############################################################
#                                            ..
#                       ....  ............   ........
#                     .     .......   .            ....  ..
#                   .  ... ..   ..   ..    ..   ..... .  ..
#                   .. .. ....@@@.  ..  .       ........  .
#              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....
#         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....
#       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    ..
#     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  .....
#    ...@@@.... @@@    .@@.......... ........ .....        ..
#   . ..@@@@.. .         .@@@@.   .. .......  . .............
#  .   ..   ....           ..     .. . ... ....
# .    .       ....   ............. .. ...
# ..  ..  ...   ........ ...      ...
#  ................................
#
# #############################################################
"""
files.py
 
    Fonction de base sur les fichiers
     
Copyright (c) 2006 by www.crans.org
"""

import datetime, time, os

def ageOfFile(pathToFile):
    """renvoie l'age d'un fichier en secondes"""
    pathToFile = os.path.expanduser(pathToFile)
    return int(time.time()) - os.path.getmtime(pathToFile)

def fileIsOlderThan(pathToFile, days=0, hours=0, minutes=0, seconds=0):
    """teste si un fichier est plus vieux on non que la valeur donnée"""
    time= (((days*24) + hours) * 60 + minutes) * 60 + seconds
    return ageOfFile(pathToFile) > time

def dirIsEmpty(pathToDir):
    """teste : répond True si le dossier est vide."""
    return os.listdir(pathToDir) == []

