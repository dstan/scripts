import re
import unicodedata

def __init__():
    pass

def QuoteForPOSIX(string):
    '''quote a string so it can be used as an argument in a  posix shell
    
    According to: http://www.unix.org/single_unix_specification/
    2.2.1 Escape Character (Backslash)
    
    A backslash that is not quoted shall preserve the literal value
    of the following character, with the exception of a <newline>.
    
    2.2.2 Single-Quotes
    
    Enclosing characters in single-quotes ( '' ) shall preserve
    the literal value of each character within the single-quotes.
    A single-quote cannot occur within single-quotes.
    
    source : http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/498202
    '''
    
    return "\\'".join(["'" + p + "'" for p in string.split("'")])


def suppression_diacritics(s):
    """source : http://wikipython.flibuste.net/moin.py/JouerAvecUnicode#head-1213938516c633958921591439c33d202244e2f4
    """
    def remove(char):
        deco = unicodedata.decomposition(char)
        if deco:
            return unichr(int(deco.split()[0],16))
        return char
    return ''.join([remove(a) for a in s])

supprimer_accents = suppression_diacritics
desaccentuer = suppression_diacritics
            
