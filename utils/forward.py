#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf8 -*-
# Script utilisé par l'intranet et autres pour ecrire le .forward dans le home
# Executé avec un sudo
# detraz@crans.org

import argparse
import os

def getforward(user):
    """Récupère le contenu du .forward d'un adhérent si celui-ci existe."""

    homedir = os.path.expanduser("~%s" % (user,))
    if homedir == "~%s" % (user,):
        raise IOError("Le home de %r n'existe probablement pas." % (user,))

    fwd = os.path.join(homedir, ".forward")

    try:
        with open(fwd, 'r') as forwardfile:
            mailredirect = forwardfile.readline().strip()
    except IOError:
        mailredirect = ""
    print mailredirect

def writeforward(user, txt):
    """Écrit dans le .forward d'un adhérent si celui-ci existe."""

    homedir = os.path.expanduser("~%s" % (user,))
    if homedir == "~%s" % (user,):
        raise IOError("Le home de %r n'existe probablement pas." % (user,))

    fwd = os.path.join(homedir, ".forward")

    with open(fwd, 'w') as forwardfile:
        forwardfile.write(txt)

def supprforward(user):
    """Supprime le .forward d'un adhérent."""

    homedir = os.path.expanduser("~%s" % (user,))
    if homedir == "~%s" % (user,):
        raise IOError("Le home de %r n'existe probablement pas." % (user,))

    fwd = os.path.join(homedir,".forward")
    os.remove(fwd)


if __name__=="__main__":
    parser = argparse.ArgumentParser(
            description="Script qui, éventuellement appelé avec sudo,\
                    permet d'écrire dans le .forward de quelqu'un,\
                    ou bien de supprimer le .forward de quelqu'un",
            add_help=False)
    meh = parser.add_mutually_exclusive_group()
    parser.add_argument(
            "-h",
            "--help",
            help="Affiche cette aide et quitte.",
            action="store_true"
            )
    parser.add_argument(
            "-m",
            "--mail",
            help="Mail d'utilisateur",
            type=str,
            action="store"
            )
    parser.add_argument(
            "-n",
            "--name",
            help="Nom d'utilisateur",
            type=str,
            action="store"
            )
    meh.add_argument(
            "-r",
            "--read",
            help="Lire le .forward",
            action="store_true"
            )
    meh.add_argument(
            "-w",
            "--write",
            help="Écrire le .forward",
            action="store_true"
            )
    meh.add_argument(
            "-s",
            "--delete",
            help="Supprimer le .forward",
            action="store_true"
            )

    args = parser.parse_args()
    if args.read:
        getforward(args.name)
    elif args.write:
        writeforward(args.name, args.mail)
    elif args.delete:
        supprforward(args.name)
