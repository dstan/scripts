#!/bin/bash
# Petit script qui surveille les dépôts git de /usr/scripts
# En cas de dépôt non pullé, ou si des commits n'ont pas
# été envoyés, une alerte est lancée.

# Pour être sûr que tout marche, on passe en LANG=C
export LANG=C

# Paths des dépôts à surveiller
GIT_REPOS="/usr/scripts /usr/scripts/lc_ldap"

# Intervalle entre deux fetchs
PERIOD=5

check_repo () {
  echo "vérification de $1"
  cd $1
  ( git status | grep "# Your branch" -q ) && {
    echo "...et dépôt pas à jour"
    exit 42
  }
}

fetch_updates () {
  cd $1
  if test ! "`find .git/FETCH_HEAD -mmin +$PERIOD`"; then
    return
  fi
  umask 002
  echo "fetching $1"
  git fetch origin > /dev/null
}

try_ff () {
  cd $1
  if git status | grep "^Your branch is behind.*can be fast-forwarded.$" -q; then
    echo "Fast forward..."
    git pull
  else
    echo "Nothing to fast forward"
  fi
}

# Et on check que les repos sont ok
for dir in $GIT_REPOS; do
  fetch_updates $dir
  check_repo $dir
  try_ff $dir
done


exit 0
