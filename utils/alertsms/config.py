# -*- mode: python; coding: utf-8 -*-

import pika
import sys

if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')

from gestion import secrets_new as secrets

CREDS = pika.credentials.PlainCredentials('sms', secrets.get('rabbitmq_sms'), True)

PARAMS = pika.ConnectionParameters(host='rabbitmq.crans.org',
    port=5671, credentials=CREDS, ssl=True)

QUEUE = "SMS"
