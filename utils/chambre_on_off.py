#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
"""chambre_on_off.py: Affiche le statut, active ou désactive une prise

 syntaxe : chambre-on-off.py chbre <cmd>
 chbre de la forme A101d
 <cmd> peut être :
   - on : activer la prise
   - off : désactiver la prise
   - absent : affiche le statut de la prise

Script importé par ra2.py

"""

import sys
from gestion import annuaires_pg
from gestion import hptools2

def chambre_on_off(chambre, cmd = None):
    """Active ou désactive une prise

    chambre: numéro de chambre
    cmd: si fourni, "on" ou "off" pour activer ou désactiver la prise
    si cmd n'est pas fourni, ne fait qu'afficher le statut de la prise
    """

    # On formatte
    bat = chambre[0]
    chbre = chambre[1:]

    # On cherche
    prise = annuaires_pg.chbre_prises(batiment=bat,chambre=chbre)
    switch_name = 'bat'+bat.lower()+'-'+prise[0]+'.adm.crans.org'

    # connexion au switch
    chbre_sw = hptools2.HPSwitch(switch_name)

    # affichage des infos
    print "chbre  : %s" % chambre
    print "switch : %s" % switch_name
    print "prise  : %s" % prise

    print "statut : %s" % chbre_sw.is_enabled(int(prise[1:]))

    if cmd:
        cmd = cmd.lower()

        # modification du statut
        if cmd == "off":
            print "action : disable"
            chbre_sw.set_enabled(int(prise[1:]),enabled=False)
        elif cmd == "on":
            print "action : enable"
            chbre_sw.set_enabled(int(prise[1:]),enabled=True)

        # affichage du nouveau statut
        print "statut : %s" % chbre_sw.is_enabled(int(prise[1:]))

if __name__ == '__main__':
    if len(sys.argv) == 2:
        chambre_on_off(sys.argv[1])
    elif len(sys.argv) == 3:
        chambre_on_off(sys.argv[1], sys.argv[2])
    else:
        print __doc__
        sys.exit(0)

