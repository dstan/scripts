#!/usr/bin/env python
#-*- mode: python; coding: utf-8 -*-
PIDFILE = '/var/run/arpwatch_sendmail.pid'
USER='arpwatch'
GROUP='adm'
SOCKET_FILE='/var/run/arpwatch_sendmail.sock'
