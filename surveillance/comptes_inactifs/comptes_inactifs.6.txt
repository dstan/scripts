Encoding: utf-8
Subject: [CRANS] Compte inactif depuis plus d'un mois

Bonjour %(nom)s,

Si tu t'es connecté(e) (par ssh, pop, imap ou webmail) dans les trois
dernières heures, tu peux ignorer ce message.

Tu reçois ce mail parce que tu as un compte sur le serveur des adhérents du
CRANS (en tant qu'ancien adhérent), et que tu ne t'es pas connecté(e) depuis
le %(date)s (ou tu ne t'es jamais connecté(e)), ce qui fait plus d'un
mois.

Tu as %(mail)d nouveau(x) mail(s) dans ta boîte de reception. Si tu n'utilises
plus ce compte, merci de nous le signaler afin que nous libérions les
ressources qui te sont allouées.

Si tu lis ce mail, c'est probablement que tu t'es connecté(e) récemment.
Dans ce cas, tu peux ignorer ce message.

Si tu veux arrêter de recevoir ces avertissements, tu dois te connecter au
moins une fois par mois, en utilisant l'un des protocoles suivants :
 - webmail (http://webmail.crans.org)
 - pop (pop.crans.org)
 - imap (imap.crans.org)
 - ssh (ssh.crans.org)

Au bout de six mois d'inactivité (et d'au moins cinq avertissements
similaires), ton compte sera automatiquement supprimé.

Nous te prions de ne pas répondre à ce mail, sauf pour nous signaler une
anomalie ou confirmer un abandon définitif du compte.

-- 
Script de détection des comptes inactifs
