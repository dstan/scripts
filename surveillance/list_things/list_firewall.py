#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
#
# list_firewall.py --- Liste les machines disposant de droits
# particuliers en ce qui concerne leurs ports accessibles.
"""Liste les machines dont certains ports sont whielistés par
le parefeu"""

from lc_ldap import shortcuts
from config.encoding import out_encoding

def make_output(ldap):
    """Génère un texte à afficher"""
    machines_speciales = ldap.search(u"(|(portTCPin=*)(portTCPout=*)(portUDPin=*)(portUDPout=*))")

    output = []

    for machine in machines_speciales:
        # On se fiche des machines crans
        if machine['objectClass'][0] not in ["machineFixe", "machineWifi"]:
            continue

        # texte pour la machine
        txt = u''
        txt += u'Propriétaire  : %s\n' % machine.proprio()
        txt += u'Machine       : %s\n' % machine['host'][0]
        if machine['portTCPin']:
            txt += u'ports TCP in  : %s\n' % ' '.join([unicode(port) for port in machine['portTCPin']])
        if machine['portTCPout']:
            txt += u'ports TCP out : %s\n' % ' '.join([unicode(port) for port in machine['portTCPout']])
        if machine['portUDPin']:
            txt += u'ports UDP in  : %s\n' % ' '.join([unicode(port) for port in machine['portUDPin']])
        if machine['portUDPout']:
            txt += u'ports UDP out : %s\n' % ' '.join([unicode(port) for port in machine['portTCPout']])

        output.append(txt.strip())
    return output

if __name__ == '__main__':
    LDAP = shortcuts.lc_ldap_readonly()
    OUTPUT = make_output(LDAP)

    print u'\n- - - - - - = = = = = = # # # # # # # # = = = = = = - - - - - -\n'.join(OUTPUT).encode(out_encoding)
