#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
#
# list_exempt.py --- Fournit la liste des machines avec une exemption.
"""Fournit une liste des machines d'adhérents disposant d'une exemption"""

from lc_ldap import shortcuts
from config.encoding import out_encoding

def make_output(ldap):
    """Génère le texte à afficher"""
    machines_avec_exemption = ldap.search(u"exempt=*")
    output = []

    for machine in machines_avec_exemption:
        # texte pour la machine
        txt = u''
        txt += u'Propriétaire : %s\n' % machine.proprio()
        txt += u'Machine      : %s\n' % machine['host'][0]
        txt += u'destination  : %s\n' % ', '.join([unicode(i) for i in machine['exempt']])

        output.append(txt.strip())
    return output

if __name__ == '__main__':
    LDAP = shortcuts.lc_ldap_readonly()
    OUTPUT = make_output(LDAP)

    print u'\n- - - - - - = = = = = = # # # # # # # # = = = = = = - - - - - -\n'.join(OUTPUT).encode(out_encoding)
