#!/bin/sh
# Pas de licence

SUMMARY='/usr/scripts/surveillance/mac_prises/mac_prise_analyzer.py --summary --reperage'

python $SUMMARY
psql -U crans mac_prises -c "DELETE FROM correspondance WHERE date <= timestamp 'now' - interval '2 days';" 1>/dev/null
psql -U crans mac_prises -c "DELETE FROM spotted WHERE date <= timestamp 'now' - interval '2 days';" 1>/dev/null
