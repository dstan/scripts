-----------------------------------------------------------------
-- Maintenance de la base pgsql filtrage sur odlyd, lancé par cron
-----------------------------------------------------------------

-- effacement des vieux enregistrements
DELETE FROM upload where stamp_updated < timestamp 'now' - interval '5 days';
