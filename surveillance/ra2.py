#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

# Réecriture complète de ra.py
# On fait tourner ramond. Dès qu'une mac non autorisée fait une ra,
# ce script s'execute et pose une blackliste sur la machine.
# Prise en compte du nouveau binding.
# Envoie de mails au propriétaire et à disconnect
# Ecrit par Gabriel Détraz <detraz@crans.org>
# Avec l'aide importante de Pierre-Elliott Bécue <becue@crans.org>

import os
import sys
import time
import subprocess
from time import sleep

# On importe les scripts Crans
import lc_ldap.shortcuts
from cranslib import clogger
from gestion import mail
from utils.sendmail import actually_sendmail
from utils.chambre_on_off import chambre_on_off

# On règle le nombre de RA admissibles par jour par machine:
TOL = 3

# Chemin du fichier des logs
LOGGER = clogger.CLogger('ra2', level='info')

# On ouvre une connexion LDAP à la base admin/de test une fois pour toute.
CONN = lc_ldap.shortcuts.lc_ldap_admin()

def ra_blacklist(Mac_ra):
    thetime = time.strftime('%Y-%m-%d', time.localtime())

    # Logs des macs capturées, avec la date
    LOGGER.info("La mac %s fait des RA" % (Mac_ra,))

    # On cherche la machine correspondante
    machines = CONN.search(u'(macAddress=%s)' % Mac_ra,mode="rw")

    # On agit que si la machine est connu
    if machines:
        machine  = machines[0]
#        print machine

        p = subprocess.Popen(['grep','-c', thetime + '.*.' + Mac_ra, LOGGER.get_file_handler_path()],stdout=subprocess.PIPE)
        stdout, stderr = p.communicate()
#        print stdout
        if int(stdout) <= TOL:
            sys.exit(1)

        # Sécurité : on évite de poser une bl à un serveur
        if isinstance(machine.proprio(), lc_ldap.objets.AssociationCrans):
            sys.exit(1)

        # Cohérence : si il y a déjà un bl, on arrète
        bl_actif = machine.blacklist_actif()
        #print bl_actif
        for blacklist in bl_actif:
            #print x['type']
            if blacklist['type'] == 'ipv6_ra':
                sys.exit(1)

        # On pose la blackliste sur la machine incriminée
        machine.blacklist('ipv6_ra', u'auto ra.py : router advertisement', debut='now', fin='-')
        machine.save()
        send_mail(machine)

        # Si machine filaire et si adh sur le campus, on-off sur la prise
        if machine.ldap_name == 'machineFixe':
            adh = machine.proprio()
            chambre = unicode(adh['chbre'][0])
            if chambre != "EXT":
                chambre_on_off(chambre,"off")
                time.sleep(5)
                chambre_on_off(chambre,"on")

    # On envoie une notification à disconnect et à la personne :
def send_mail(machine):
    adh = machine.proprio()
    From = 'disconnect@lists.crans.org'
    To = adh.get_mail()
    Cc = 'disconnect@lists.crans.org'
    name = adh.ldap_name
    mach = unicode(machine['host'][0])
    if isinstance(adh, lc_ldap.objets.adherent):
        tname = unicode(adh['prenom'][0]) + " " + unicode(adh['nom'][0])
    elif isinstance(adh, lc_ldap.objets.club):
        tname = unicode(adh['nom'][0])
    mailtxt = mail.generate('deconnex_ra', {
            'To': To,
            'Cc': Cc,
            'From': From,
            'tname': tname,
            'mach': mach,
    })

    actually_sendmail(From, (To,Cc), mailtxt)

## Si testing :
if __name__ == "__main__":
    # On  récupère la mac dans l'env :
    Mac_ra = os.getenv('SOURCE_MAC')
#    Mac_ra = sys.argv[1]
#    print Mac_ra
    ra_blacklist(Mac_ra)
