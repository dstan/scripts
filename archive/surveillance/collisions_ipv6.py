#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

""" Pour détecter et signaler les collisions d'adresses IPv6 """

import psycopg2
import sys
import netaddr
import itertools
import gestion.ipt as ipt
from gestion.config import prefix as crans_prefixes

## Construit un filtre sql succint des réseaux à vérifier

# (crans_prefixes est un dictionnaire de listes de networks, mais on se fout des
#  clés, donc on concatène méchamment)
prefixes = itertools.chain(['fe80::/64'], *crans_prefixes.values())
# Et on ne garde que le plus utile (histoire de faire moins de tests plus tard)
prefixes = netaddr.cidr_merge(prefixes)
prefixes = " OR \n".join( "a.ip <<= inet '%s'" % str(cidr) for cidr in prefixes)

# Connection à la base sql via pgsql
pgsql = psycopg2.connect(database='filtrage', user='crans')
# Il faudra remplacer la ligne ci-dessous par pgsql.set_session(autocommit = True) sous wheezy
pgsql.set_isolation_level(0)
curseur = pgsql.cursor()

# On regarde s'il y a deux ipv6 identiques avec des mac non identiques
collision_mac_ip_request = """SELECT DISTINCT
    a.date as date1, a.mac as mac1, a.ip as ip1,
    b.date as date2, b.mac as mac2, b.ip as ip2
FROM mac_ip as a, mac_ip as b
WHERE a.ip = b.ip AND
      (%s) AND
      a.mac != b.mac AND
      a.date >= b.date AND
      a.date - b.date < interval '3 day'
ORDER BY a.date;""" % prefixes
curseur.execute(collision_mac_ip_request)
collision_mac_ip = curseur.fetchall()

if collision_mac_ip != []:
    print "Collision d'addresses ipv6 : "
for (date1, mac1, ip1, date2, mac2, ip2) in collision_mac_ip:
    print "%s %s %s" % (date1, ipt.mac_addr(mac1), ip1)
    print "%s %s %s" % (date2, ipt.mac_addr(mac2), ip2)
