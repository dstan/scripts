#! /usr/bin/env python
# -*- coding: iso8859-15 -*-

"""
Configuration de base des diff�rents transpondeurs disponibles
Ce fichier contient la liste des chaines pour chaque transpondeur.

Pour chaque chaine, on doit indiquer le nom de la chaine et au minimum :
- Le pid audio
- Le pid video
- Le pid PMT, ce pid contient la liste des differents flux associes a une chaine
VLC a besoin de ce PID pour lire le flux

Les pids facultatifs sont les suivants :
- Le PID PCR qui indique la base de temps (Program Clock Reference)
et permet de mieux reconstituer les flux un peu defecteux, et sert
aussi pour synchroniser les informations teletexte.
Il est conseille de le mettre, et il est souvent present avec le
flux video (cas de la TNT)
- Les PIDs de teletexte
"""

#Auteur : Fr�d�ric Pauget
#Maintenance et adaptations : DUBOST Brice
#Licence : GPLv2

from commands import getoutput
from time import sleep
import os, socket

IP = socket.gethostbyaddr(socket.gethostname())[-1][0]

class CarteOqp(Exception) :
    """ La carte est d�ja utilis�e """

class NotRunning(Exception) :
    """ La carte ne diffuse rien """
    
class carte :
    """ Classe parent de toute classe de transpondeur """
    # Niveux de verbosite :
        # 0 : ne dit rien
        # 1 : messages � caract�res informatifs
        # 2 : messages de debug
        # 3 : ne permet pas � mumudvb de daemonizer
    verbose = 3
    
    CONF_FILE = "/etc/sat/carte%i.conf" # %i : numero de la carte
    
    timeout_accord=20 #en secondes
    timeout_no_diff=60 #en secondes
    
    entete_conf = """### Fichier g�n�r�, NE PAS EDITER
autoconfiguration=1
sap=1
sap_organisation=crans
common_port=1234
freq=%(freq)i
pol=%(pol)s
srate=%(srate)i
card=%(card)i
timeout_accord=%(timeout_accord)i
timeout_no_diff=%(timeout_no_diff)i
#-----
"""

    entete_conf_TNT = """### Fichier g�n�r�, NE PAS EDITER
autoconfiguration=1
sap=1
sap_organisation=crans
common_port=1234
freq=%(freq)i
qam=%(qam)s
trans_mode=%(trans_mode)s
bandwidth=%(bandwidth)s
guardinterval=%(guardinterval)s
coderate=%(coderate)s
card=%(card)i
timeout_accord=%(timeout_accord)i
timeout_no_diff=%(timeout_no_diff)i
"""

    chaine_template = """ip=%(ip)s
sap_group=%(sap_group)s
name=%(name)s
pids=%(pids)s
#-----
"""

    autoconf2_template = """common_port=1234
autoconfiguration=2
autoconf_ip_header=%(ip)s
sap_default_group=various
#-----
"""

    pid_file = "/var/run/mumudvb/mumudvb_carte%i.pid" # % num carte
    mumudvb = "/usr/local/bin/mumudvb_sap "
    
    def __cmp__(a,b) :
        for attr in ( 'card', 'freq', 'chaines' ) :
            if getattr(a,attr) != getattr(b,attr) :
                return -2
        return 0
    
    def __init__(self,card) :
        """ Initalisation card est le num�ro (entier) de la carte 
        correspondante """
        try :
            self.freq = int(str(self.__class__).split('_')[-1])
        except :
            # On ne pourra pas faire grand chose � part killer le flux de la carte
            self.freq = ''
            pass
        self.card = card
                
    def gen_conf(self) :
        """ G�n�re le fichier de conf """
        if not self.freq : 
            if self.verbose > 1 : print "Instance ne permettant pas la g�n�ration de la conf"
            return
        
        fd = open(self.CONF_FILE % self.card,'w')
        # Ent�te du fichier
        try:
            fd.write( self.entete_conf_TNT % 
                      { 'qam' : self.qam, 'trans_mode' : self.trans_mode ,
                        'bandwidth' : self.bandwidth, 'guardinterval' : self.guardinterval ,
                        'coderate' : self.coderate,
                        'freq' : self.freq , 'card' : self.card ,
                        'timeout_accord' : self.timeout_accord ,
                        'timeout_no_diff' : self.timeout_no_diff } )
        except:
            fd.write( self.entete_conf % 
                      { 'pol' : self.pol, 'srate' : self.srate ,
                        'freq' : self.freq , 'card' : self.card ,
                        'timeout_accord' : self.timeout_accord ,
                        'timeout_no_diff' : self.timeout_no_diff } )

        # Chaines
        n = 0
        for pids, (sap_group, name) in self.chaines.items() :
            ip = '239.%s.20%i.2%02i' % ( IP.split('.')[-1], self.card, n)
            n += 1
            fd.write(self.chaine_template % vars())

	#Si pas de chaines, on passe en autoconfiguration=2
    	if not n :
            ip = '239.%s' % ( IP.split('.')[-1])
            fd.write(self.autoconf2_template % { "ip" : ip})

        fd.close()

    def get_pid(self) :
        """ Retourne le pid associ� � la carte """
        try:
            pid = int(open(self.pid_file % self.card).readline().strip())
            if self.verbose > 1 :
                print 'pid : %i' % pid ,
            return pid
        except :
            raise NotRunning
            
    def is_running(self) :
        """ V�rifie si le process correspondant � la carte toune """
        if self.verbose > 1 :
            redir = ''
        else :
            redir = '>/dev/null 2>&1'
        try :
            if not os.system('ps %i %s' % (self.get_pid() , redir) ) :
                # Il tourne
                return True
        except NotRunning :
            pass
        return False
    
    def start(self) :
        """ Lance la diffusion """
        if not self.freq : 
            if self.verbose > 1 : print "Instance ne permettant pas le lancement d'un flux"
            return
        
        if self.verbose >0 :
            print "Lancement de %s sur la carte %i" % (str(self.__class__).split('.')[-1], self.card)
            
        if self.is_running() :
            raise CarteOqp
        
        if self.verbose >0 : print "\tG�n�ration de la conf...",
        self.gen_conf()
        if self.verbose >0 : print "OK"
        
        cmd = '%s -c %s' % ( self.mumudvb, self.CONF_FILE % self.card )
        if self.verbose > 2 : cmd += ' -v -v -d -s'
        if self.verbose > 1 :
            print "\tCommande : %s" % cmd
        for i in range(2*self.timeout_accord) :
            if not i%5 and i <= self.timeout_accord :
                if self.verbose > 0 and i : print "ATTENTE/ERREUR"
                # On fait une tentative de lancement toutes les 5s (en cas de pb de diseq)        
                if self.verbose > 0 : print "\tTentative %i" %(i/5+1) ,
                os.system(cmd)
            sleep(1)
            if self.is_running() :
                if self.verbose > 0 : print 'OK'
                break
        sleep(1)
        if not self.is_running() :
            if self.verbose > 0 : print 'ABANDON'
            raise NotRunning
            
    def stop(self) :
        """ Arr�te la diffusion de la carte """
        if self.verbose >0 : 
            print "Arret diffusion carte %i..." % self.card ,
            
        try : 
            # Ca tourne au moins ?
            if not self.is_running() : 
                if self.verbose >0 : print "carte d�ja arr�t�e"
                return
            
            os.kill(self.get_pid(),15)
            sleep(1)
            if not self.is_running() : 
                if self.verbose >0 : print "OK"
                return
            
            # Cr�ve !!
            if not self.is_running() : 
                if self.verbose >0 : print "SIGKILL"
                return
            
            os.kill(self.get_pid(),9)
            # Salloperie
            raise CarteOqp
        except NotRunning :
            # Parfait, c'�tait le but
            pass
        
    def restart(self) :
        """ Red�marre le flux """
        self.stop()
        self.start()
    
#class Hotbird_10796(carte) :
#    pol='v'
#    srate=27500
#    chaines = {
#       '3336 3306' : 'radios' : 'rad fra Radio FG'),
#       '3534 1001' : 'radios' : 'rad fra France Inter'),
#       '3535 1002' : 'radios' : 'rad fra France Info' }

#Format : "'pids' : ( 'groupe sap' : 'nom' ),"

class Hotbird_10853(carte) :
    pol='h'
    srate=27500
    chaines = {}

class Hotbird_11137(carte) :
    pol='h'
    srate=27500
    chaines = {
       '717' : ('fra' , 'fra TV5MONDE FBS'),
       '719' : ('fra' , 'fra TV5MONDE Europe'),
       '262' : ('ara' , 'ara ANN'),
       '263' : ('ara' , 'ara Kurdistan TV'),
       '264' : ('ita' , 'ita Videolina')}

class Hotbird_11200(carte) : #A mettre a jour
    pol='V'
    srate=27500
    chaines = {
       '366 367 2560' : ('ita' , 'ita Elite shopping TV'),
       '386 387 512' : ('x-ero' , 'x-ero All Sex'),
       '397 398 399' : ('ita' , 'ita StarSat'),
       '394 395 4864' : ('ita' , 'ita Play TV'),
       '400 404 402' : ('ita' , 'ita People TV'),
       '405 406 407' : ('ita' , 'ita Roma Sat')}

class Hotbird_11240(carte) :
    pol='v'
    srate=27500
    chaines = {
       '244' : ('fra' , 'fra France 24'),
       '247' : ('eng' , 'eng France 24'),
       '401' : ('fra' , 'fra RTBF sat')}

class Hotbird_11604(carte) :
    pol='h'
    srate=27500
    chaines = {
       '500' : ('ita' , 'ita TLA'),
       '600' : ('ger' , 'ger Das Erste'),
       '700' : ('ger' , 'ger DW TV'),
       '800' : ('divers' , 'divers DW tv arabic (ara eng ger)'),
       '900' : ('ger' , 'ger RTL2 Schweiz'),
       '3500' : ('eng' , 'eng Cool TV'),
       '2600' : ('ger' , 'ger Erde Mensch'),
       '1600' : ('x-ero' , 'x-ero hot arab tv'),
       '3200' : ('x-ero' , 'x-ero xxx Action TV')}

class Hotbird_11623(carte) : 
    pol='v'
    srate=27500
    chaines = {}

class Hotbird_11642(carte) :
    pol='h'
    srate=27500
    chaines = {
       '1360 1320 5003' : ('eng' , 'eng Bloomberg Europe'),
       '1460 1420 5004' : ('ger' , 'ger Bloomberg TV Deutschland'),
       '1560 1520 5005' : ('eng' , 'eng Bloomberg U.K.'), 
       '2101 2111 256 128 2121' : ('gr' , 'gr ERT Sat')}

class Hotbird_11727(carte) :
    pol='v'
    srate=27500
    chaines = {
       '2711 2712 257 2710' : ('fra' , 'fra La Locale'),
       '2791 2792 269' : ('ita' , 'ita Sicilia International (SET)'),
       '2751 2752 266' : ('ita' , 'ita Sardegna Uno Sat'),
       '6507 6662' : ('radios' , 'rad eng BBC English (Europe)'),
       '6510 6692' : ('radios' , 'rad eng BBC English News'),
       '7302 6571' : ('radios' , 'rad eng BBC Feed 1'),
       '7312 6572' : ('radios' , 'rad eng BBC Feed 2'),
       '7322 6573' : ('radios' , 'rad eng BBC Feed 3'),
       '7332 6574' : ('radios' , 'rad eng BBC Feed 4'),
       '7342 6575' : ('radios' , 'rad eng BBC Feed 5'),
       '7352 6576' : ('radios' , 'rad eng BBC Feed 6')}

# Pour remplacer 10796
class Hotbird_11766(carte) :
	pol='v'
	srate=27500
	chaines = {
		'1104' : ('ita' , 'ita Rai Uno'),
		'1105' : ('ita' , 'ita Rai Duo'),
		'1106' : ('ita' , 'ita Rai Tre'),
		'260' : ('ita' , 'ita Rai Med'),
                '1107' : ('ita' , 'ita eng Senato italiano'),
                '1107' : ('eng' , 'eng ita Senato italiano'),
		'261' : ('ita' , 'ita Rai Edu2')}
       
class Hotbird_11727(carte) :
    pol='v'
    srate=27500
    chaines = {}

class Hotbird_11054(carte) :
    pol='h'
    srate=27500
    chaines = {}

class Hotbird_12111(carte) :
    pol='v'
    srate=27500
    chaines = {
       '1028' : ('ara' , 'ara Al Jazeera Documentary'),
       '1029' : ('ara' , 'ara Al Jazeera Mubasher'),
       '1030' : ('ita' , 'ita ACM - Architecture Construction Materials'),
       '1031' : ('ara' , 'ara Al Jazeera Satellite Channel'),
       '1032' : ('ita' , 'ita Silicia Channel'),
       '1034' : ('ita' , 'ita High life TV'),
       '1040' : ('ita' , 'ita Radio Radio TV'),
       '1042' : ('ita' , 'ita MediterraneoSat 1')}

class Hotbird_12476(carte) :
    pol='h'
    srate=27500
    chaines = {
	#2M don't support autoconfiguration
       '600 601 602' : ('ara' , 'ara fra 2M'),
       '600 603' : ('ara' , 'ara radio 2M'),
       '550' : ('x-ero' , 'x-ero X Stream')}

class Hotbird_12558(carte) : #outdated
    pol='v'
    srate=27500
    chaines = {
       '6660 6661 6659' : ('ita' , 'ita Administra.it'),
       '6916 6917 6915 6930' : ('ita' , 'ita 24 Ore')}

class Hotbird_12597(carte) :
    pol='v'
    srate=27500
    chaines = {
       '1024' : ('rus' , 'rus sport planeta'),
       '1027' : ('eng' , 'eng BBC World'),
       '1031' : ('rus' , 'rus ORT International'),
       '1032' : ('eng' , 'eng CNN'),
       '1034' : ('fra' , 'fra autres Euronews'),
       '1034 2221 2232 768' : ('eng' , 'eng Euronews')}

class Hotbird_12673(carte) : #outdated
    pol='v'
    srate=27500
    chaines = {
       '308 309 306 307' : ('ara' , 'ara Al Maghribiyah'),
       '35 36 33 34 38' : ('ara' , 'ara TVM inter l'),
       '46 47 44 45' : ('ara' , 'ara Arabiaa')}

class TNT_base(carte) :
    qam="auto"
    trans_mode="auto"
    guardinterval="auto"
    coderate="auto"
    bandwidth="8MHz"
    
class TNT_R1_586000(TNT_base) :
    chaines = {
       '110' : ('fra' , 'fra TNT02 France 2'),
       '210' : ('fra' , 'fra TNT03 France 3'),
       '310' : ('fra' , 'fra TNT05 France 5'),
       '510' : ('fra' , 'fra TNT07 Arte'),
       '510 520 531 542' : ('ger' , 'ger Arte'),
       '610' : ('fra' , 'fra TNT13 LCP Public Senat'),
       '710' : ('fra' , 'fra TNT20 France O')}

class TNT_R2_474000(TNT_base) :
    chaines = {
       '1280' : ('fra' , 'fra TNT08 Direct 8'),
       '1282' : ('fra' , 'fra TNT15 BFM TV'),
       '1283' : ('fra' , 'fra TNT16 i tele'),
       '1284' : ('fra' , 'fra TNT17 Virgin17'),
       '1286' : ('fra' , 'fra TNT14 France 4'),
       '1285' : ('fra' , 'fra TNT18 Gulli')}

#les pids ont tendance a changer souvent pour canal, on en met le plus possible
class TNT_R3_522000(TNT_base) :
    chaines = {
       '1280 80 81 83 32 33 170 120 121 123 62 52' : ('fra' , 'fra TNT04 Canal')}

class TNT_R4_498000(TNT_base) :
    chaines = {
       '110' : ('fra' , 'fra TNT06 M6'),
       '210' : ('fra' , 'fra TNT09 W9'),
       '310' : ('fra' , 'fra TNT11 NT1'),
       '710' : ('fra' , 'fra TNT57 Arte HD')}

class TNT_R5_538000(TNT_base) :
    chaines = {
       '110' : ('fra' , 'fra TNT51 TF1 HD'),
       '210' : ('fra' , 'fra TNT52 France 2 HD'),
       '310' : ('fra' , 'fra TNT56 M6 HD')}

class TNT_R6_562000(TNT_base) :
    chaines = {
       '100' : ('fra' , 'fra TNT01 TF1'),
       '600' : ('fra' , 'fra TNT10 TMC'),
       '200' : ('fra' , 'fra TNT12 NRJ12')}

class TNT_R7_490000(TNT_base) :
    chaines = {
       '4321' : ('fra' , 'fra TNT21 Canal 21 : Cinaps TV, BDM TV, Bocal TV, Demain TV'),
       '4322' : ('fra' , 'fra TNT22 IDF1'),
       '4323' : ('fra' , 'fra TNT23 NRJ Paris'),
       '4324' : ('fra' , 'fra TNT24 Cap 24')}


