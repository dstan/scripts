#! /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import sys

print("Vieux script qui ne marche plus. À réécrire. Anyway ce script était moche")
sys.exit(1)

sys.path.append('/usr/scripts/gestion')
from whos import borne_etat, borne_clients_canal
from ldap_crans import crans_ldap
from time import sleep
from affich_tools import coul
import threading
from os import getuid

# il faut être root
if getuid() :
    print 'Il faut être root !'
    sys.exit(1)

# connexion à la base de données
db = crans_ldap()

# décompostion des arguments
try :
    mac = ":".join([i.zfill(2) for i in sys.argv[1].split(":")]).upper()
except :
    mac = None

# dit si une mac a déja été traitée
mac_done=[]
def done (mac) :
    global mac_done
    if mac in mac_done :
        return True
    else :
        mac_done.append(mac)
        return False

# liste des résultats
results = []
    
# classe d'interrogation des bornes
class interroge_borne (threading.Thread) :
    def __init__ (self, db, borne, mac = None) :
        threading.Thread.__init__(self)
        self.borne = borne
        self.mac   = mac
        self.db    = db
        self.start()

    def aff_client (self, mac, rssi) :
        if done(mac) :
            return
        
        res = db.search("mac=%s" % mac)['machine']
        if not res:
            client_nom = '????'
            coul_rssi = 'rouge'
            rssi = 0
        else:
            # On va choisir la bonne couleur pour le RSSI
            if rssi > -88:
                coul_rssi = 'vert'
            elif rssi > -93:
                coul_rssi = 'jaune'
            else:
                coul_rssi = 'rouge'
        global results
        results.append('%-10s %-30s (%-15s, RSSI: %s)' % (self.borne.nom().split('.')[0], res[0].proprietaire().Nom(), res[0].nom().split('.')[0],coul("%d" % rssi, coul_rssi)))

    def run (self) :
        nom = self.borne.nom()
        if not borne_etat(nom) :
            return
        clients = borne_clients_canal(nom)['mac-rssi']
        for (mac, rssi) in clients :
            if not self.mac.upper() or self.mac == mac.upper() :
                self.aff_client(mac,rssi)

# on onterroge trois fois car il donne pas toujours les clients
for i in range(0,3) :
    # on interroge les bornes
    resultat = {}
    bornes = db.search('canal=*')['machine']
    for borne in bornes :
        interroge_borne(db, borne, mac)

# on attend la fin de threads
while len(threading.enumerate()) > 1 :
    sleep(1)

results.sort()
print '\n'.join(results)
