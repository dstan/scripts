#! /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

# Stats sur les historiques

import sys
sys.path.append("/usr/scripts/gestion")

import types
from ldap_crans import crans_ldap
from pysqlite2 import dbapi2 as sqlite
import re
from time import mktime, strptime

def plat(chose):
    """Applatit une liste de liste. Hautement récursif."""
    if type(chose) != types.ListType:
        return [chose]
    return sum(map(lambda x: plat(x), chose), [])


def hist():
    """Récupère l'historique dans une base SQLite dont la connexion est returnée."""
    # On récupèr les adhérents
    adherents = crans_ldap().search("nom=*")['adherent']
    # Et les historiques rattachés à eux et à leurs machines
    historiques = map(lambda x: [x.historique(), map(lambda y: y.historique(), x.machines())],
                      adherents)
    historiques = plat(historiques)
    # On va maintenant coller les historiques dans une structure plus
    # sympa, style une base SQL que l'on garde en mémoire
    con = sqlite.connect("historiques")
    cur = con.cursor()
    cur.execute("CREATE TABLE historique (date INTEGER, nom TEXT, action TEXT)")
    # On doit maintenant mettre ce qu'il faut dans la table...
    regex = re.compile("([^,]*), ([^ ]*) : (.*)")
    for h in historiques:
        mo = regex.match(h)
        if not mo:
            print "Hummm ? Ligne bizarre : %s" % h
        else:
            date = int(mktime(strptime(mo.group(1), "%d/%m/%Y %H:%M")))
            utilisateur = mo.group(2)
            # Il peut y avoir plusieurs raisons !
            raisons = mo.group(3).split(", ")
            # On garde les raisons courtes
            raisons = map(lambda x: x.split(" ")[0].strip(), raisons)
            for r in raisons:
                cur.execute("INSERT INTO historique VALUES (?, ?, ?)",
                            (date, utilisateur, r))
    con.commit()
        

    return con
    
hist()

# Exemple de requete :

# select nom, count(nom) as combien from historique
# where date>1093950403 group by nom order by combien ;

# select action,count(action) from historique where nom='bernat' and
# date>1093950403 group by action order by count(action);
