#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

'''Ce script permet de savoir s'il y a du monde au 2B.'''

import sys
from socket import gethostname
from os import system

from gestion.ldap_crans import crans_ldap
from gestion.hptools import hpswitch, ConversationError
from gestion.affich_tools import coul, cprint
from gestion.whos import aff
from gestion.mail import generate
from utils.sendmail import actually_sendmail

def missing_tpe():
    dst = 'respbats@crans.org'
    mail = generate('missing_tpe', {'to': dst})
    actually_sendmail('respbats@crans.org', [dst], mail)

def liste_2b(warn_mail=[]):
    '''Cette fonction permet de savoir s'il y a du monde au 2B.'''

    sw = hpswitch('backbone.adm')
    db = crans_ldap()

    ## Rappel (faut bien le mettre quelque part ...) :
    # Quand ça marche pas, c'est sûrement quelque chose parmi :
    # -snmpwalk n'est pas installé (paquet Debian snmp)
    # -les MIBs ne sont pas installés (/etc/snmp/mibs/hp)
    # -snmpwalk ne sait pas qu'il faut aller les chercher dans ce dossier
    #   (/etc/snmp/snmp.conf)
    macs = sw.show_prise_mac(33) #B9, ex-(D9->87)

    # Machines habituellement présentes, s'inquiéter si on ne les voit plus:
    expected = {
        "00:07:cb:b1:99:4e": "Freebox",
        "00:1c:2e:56:1a:20": "minigiga (switch)",
        "00:40:8c:7f:4a:b5": "tinybrother",
        "00:24:8c:44:3b:70": "vo",
        "54:7f:54:77:ca:be": "terminal",
	"04:18:d6:02:2c:87": "Borneo (borne)",
    }

    # Machines branchée sur la prise
    # (on voit aussi les machines branchées via une borne WiFi du 2B)
    machines = []
    if macs:
        machines_crans = []
        for mac in macs:
            fm = db.search("mac=%s" % mac)
            if len(fm['machine']) != 0:
                m = fm['machine'][0]
                # Machine personnelle ou machine Cr@ns ?
                if len(fm['borneWifi']) == 0 and len(fm['machineCrans']) == 0:
                    try:
                        p = m.proprietaire()
			if p.idn == "cid" and p.id() == "35":
                            # Machine du *club* Cr@ns
                            machines_crans.append(m)
                            continue
                        if len(p.droits()) != 0 or len(fm['machineFixe']) != 0:
                            # On filtre en Wifi sur ceux qui ont des droits
                            machines.append(m)
                    except:
                        pass
                else:
                    machines_crans.append(m)
            elif mac not in expected:
                    cprint("Machine inconnue: %s" % mac, 'rouge')
        for expectedMac in expected.iterkeys():
            if expectedMac not in macs:
                cprint("Machine %s manquante !" % expected[expectedMac], 'rouge')
                if expected[expectedMac] in warn_mail:
                    missing_tpe()
                    print("envoi d'un mail")
        # Affichage des machines
        cprint('---=== Machines du Crans ===---', 'bleu')
        aff(machines_crans, mtech=True)
        cprint('---=== Autres machines ===---', 'bleu')
        aff(machines)
    ttyfound = 1

    # Utilisateurs connectés sur vo sur place
    if gethostname() == 'vo':
        cprint('---=== W(ho) sur vo ===---', 'bleu')
        ttyfound = system("/usr/bin/w -s | grep ?xdm?")
    print ''

    # Conclusion
    if len(machines) != 0 or ttyfound == 0:
        cprint("---=== Il y a du monde au 2B ! ===---", 'vert')
    else:
        cprint("---=== Il semble n'y avoir personne au 2B ... ===---", 'rouge')

if __name__ == '__main__':
    if '--warn-tpe' in sys.argv:
        liste_2b(warn_mail=["terminal"])
    else:
        liste_2b()

