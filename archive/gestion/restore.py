#! /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

"""
Restauration d'un objet précédement détruit dans la base.

Copyright (C) Frédéric Pauget
Licence : GPLv2

Attention, ce fichier est osbolète
"""

import cPickle, sys
if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')
from cranslib.deprecated import module as dep_module
dep_module('ressucite or ressucite_lc')

import config
from whos import aff
from affich_tools import prompt
from gest_crans import modif_adher, set_machine, modif_club
from ldap_crans import mailexist

def load(file) :
    """ Charge l'objet (adhérent, machine ou club contenu dans le fichier fourni """
    try :
        fd=open(file,'rb')
    except :
        print "Impossible d'ouvrir le fichier demandé."
        sys.exit(1)

    obj = cPickle.load(fd)

    try :
        # Si machine vérif si le proprio est encore dans la base
        test_proprio = obj.proprietaire()
        if test_proprio.Nom() != obj.proprio :
            raise
        # Propriétaire encore dans la base => on récupère les infos de la base
        del obj.proprio
    except :
        pass

    ### Modifs pour permettre une restauration
    # On supprime les infos de aid, mid ou cid
    obj.dn = obj.dn.split(',',1)[1]

    # On supprime les infos du init_data
    obj._init_data={}

    return obj

if '-h' in sys.argv or '--help' in sys.argv or len(sys.argv) != 2 :
    print "%s <fichier>" % sys.argv[0].split('/')[-1].split('.')[0]
    print "Restauration ou visualisation d'un objet précédement détruit dans la base."
    print "Les fichiers de sauvegarde sont dans %s" % config.cimetiere
    sys.exit(255)

obj = load(sys.argv[1])
aff(obj)

def restore_adher(adh) :
    if adh.compte() and mailexist(adh.compte()) :
        print "AVERTISSEMENT : le login %s à déja été réattribué." % adh.compte()
        print "                il faudra recréer un compte avec un login différent"
        prompt(u'Appuyez sur ENTREE pour continuer')
        adh._data['mail'] = []
    modif_adher(adh)

def restore_machine(machine) :
    try :
        obj.proprio # crash si l'adhérent est encore dans la base
        # L'adhérent est plus dans la base
        t = prompt(u"Ratacher la machine à un [C]lub ou un [A]dhérent ?")
        t = t.lower()
        if t in 'ac' :
            i = prompt(u"Entrez l'%sid auquel ratacher la machine : %sid =" % (t,t) )
            machine.dn = '%sid=%s,%s' % (t, i, machine.dn.split(',',1)[1] )
    except :
        pass
    set_machine(machine)

def restore_club(club) :
    modif_club(club)

# Restauration ?
q = prompt(u'Restaurer cette entrée ? [O/N]')
if q not in 'oO' :
    sys.exit(0)

obj.connect() # Reconnection à la base LDAP
if obj.idn == 'aid' :
    restore_adher(obj)
elif obj.idn == 'mid' :
    restore_machine(obj)
elif obj.idn == 'cid' :
    restore_club(obj)
