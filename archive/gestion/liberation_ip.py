#! /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-


import sys
sys.path.append('/usr/scripts/gestion')
import config
from ldap_crans import crans_ldap, decode
db = crans_ldap()

annee = config.ann_scol


#suppression des machines

adherents = db.search( 'paiement=%d&paiement!=%d' % (annee - 1, annee), 'w' )['adherent']
for adherent in adherents:
        print "suppression des machines de: %(nom)s " % {'nom': adherent.Nom()}     
        machines = adherent.machines()
        for machine in machines:
                print "suppression machine: %(nom)s " % {'nom': machine.nom()}     
                machine.delete('liberation de l\'adresse')

# regeneration du pool d'ip

print "Regeneration du pool d'ip"
import numeros_disponibles

ip_occupees = numeros_disponibles.lister_ip_utilisees()
for net in config.NETs.keys():
    numeros_disponibles.update_ip(net, ip_occupees)
print "Fini !"
