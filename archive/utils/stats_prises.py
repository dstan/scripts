#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

"""Affiche des statistiques sur le câblage des prises des chambres
(CRANS ou CROUS)."""

import sys
import psycopg2
sys.path.append('/usr/scripts/gestion')
from affich_tools import tableau

conn = psycopg2.connect("user=crans dbname=switchs host=pgsql.adm.crans.org")

def compte_prises(reseau, batiment=''):
    """Compte les prises câblées sur reseau (crans ou crous)."""

    reseau = reseau.lower()
    cur = conn.cursor()
    if batiment == '':
        cur.execute("SELECT COUNT(prises) FROM prises WHERE " + reseau + "=True")
        n = cur.fetchone()[0]
    else:
        batiment = batiment.lower()
        if batiment == "g":
            cur.execute("SELECT COUNT(prises) FROM prises WHERE (batiment, " + reseau + ")=(%s, True) AND substring(chambre for 1) <> '0'", (batiment, ))
        else:
            cur.execute("SELECT COUNT(prises) FROM prises WHERE (batiment, " + reseau + ")=(%s, True)", (batiment, ))
        n = cur.fetchone()[0]

    return n

tot_crans = 0
tot_crous = 0

data = []
for bat in ['A', 'B', 'C', 'G', 'H', 'I', 'J', 'M']:
    n_crans = compte_prises('crans', bat)
    n_crous = compte_prises('crous', bat)
    tot_crans += n_crans
    tot_crous += n_crous
    frac_crans = 1.*n_crans/(n_crous + n_crans)
    data.append([bat, n_crans, n_crous, '%0.2f' % (100*frac_crans)])

data.append(["Total", tot_crans, tot_crous, '%0.2f' % (100.*n_crans/(n_crous+n_crans))])

print tableau(data, [u'Bât.', u'CRANS',
                     u'CROUS', u'% CRANS'],
              [7, 7, 7, 9], ['c', 'd', 'd', 'd'])
