#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
# #############################################################
#                                            ..
#                       ....  ............   ........
#                     .     .......   .            ....  ..
#                   .  ... ..   ..   ..    ..   ..... .  ..
#                   .. .. ....@@@.  ..  .       ........  .
#              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....
#         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....
#       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    ..
#     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  .....
#    ...@@@.... @@@    .@@.......... ........ .....        ..
#   . ..@@@@.. .         .@@@@.   .. .......  . .............
#  .   ..   ....           ..     .. . ... ....
# .    .       ....   ............. .. ...
# ..  ..  ...   ........ ...      ...
#  ................................
#
# #############################################################
"""
 cout.py

     Fonctions pour calculer le prix d'une impression
       (ne calcul que le prix de l'encre.
        retourne le prix pour une copie en A4)

 Copyright (c) 2006 by www.crans.org
"""

# Début : Ajout log pour réestimer les coûts
import time
# Fin 
import sys
import tempfile
import os
import commands
import shutil
import syslog
import stat
if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')
from cranslib.deprecated import module as deprecated_module
deprecated_module()
from cranslib.utils import QuoteForPOSIX as escapeForShell
def __init__():
    pass

# ########################################################### #
#                        CONSTANTES                           #
# ########################################################### #
#
#
from gestion import config
import gestion.config.impression
COUT_UNITE_COULEUR = config.impression.c_coul
COUT_UNITE_NOIRE = config.impression.c_noir
COUT_PASSAGE_TAMBOUR_NOIR = config.impression.c_tambour_noir
COUT_PASSAGE_TAMBOUR_COULEUR = config.impression.c_tambour_coul

# Début : Ajout log pour réestimer les coûts
FICHIER_LOG="/var/log/log_couts/estimations"
# Fin 

# ########################################################### #
#                          ERREURS                            #
# ########################################################### #
#
#
class FichierInvalide(Exception):
    """
    Si le fichier est invalide
    """
    def __str__(self):
        return self.args[0]
    def file(self):
        try:
            return self.args[1]
        except:
            return "n/a"

def try_command(cmd, tmp_rep, error_msg):
    u""" Execute a command, log output and raise exception if it fails. """

    (status, rep) = commands.getstatusoutput(cmd)
    if status:
        syslog.openlog('impression')
        syslog.syslog(syslog.LOG_ERR, 'command failed (%d): %s' % (status, cmd))
        for l in rep.split('\n'):
            syslog.syslog(syslog.LOG_ERR, 'output: %s' % l)
        syslog.closelog()
        shutil.rmtree(tmp_rep)
        raise ValueError, error_msg % status

# ########################################################### #
#                       PRIX                                  #
# ########################################################### #
def base_prix(path_pdf_file, color=False):
    u""" Calcul le prix d'une impression couleur ou noir et blanc sur papier A4 """

    # nom_rep seras le dossier dans tmp ou tous les fichier créé par
    #     convert seront entreposé
    nom_rep = tempfile.mkdtemp(prefix='tmpimpr')
    nom_png = "%s/convert.png" % nom_rep # Nom prefixe et chemin des png créé par convert
    escaped_path_pdf_file = escapeForShell(path_pdf_file)
    escaped_path_ps_file = escapeForShell(path_pdf_file + ".ps")
    error_msg = "ERREUR %%d : Fichier invalide. Aucun %s cree."
    if color:
        error_msg += " (couleurs)"
        gs_device = "png16m"
        nb_composante = 4 # Cyan, Magenta, Jaune, Noir
        percent_program = "percentcolour"
    else:
        gs_device = "pnggray"
        nb_composante = 1 # Noir
        percent_program = "percentblack"

    if not os.access(path_pdf_file + ".ps",os.F_OK) or \
        os.stat(path_pdf_file)[stat.ST_MTIME] > os.stat(path_pdf_file + ".ps")[stat.ST_MTIME]:
            # Convertit les pdf en ps
            try_command("nice -n 5 pdftops %s %s" % (escaped_path_pdf_file,
                                                     escaped_path_ps_file),
                        nom_rep,
                        error_msg % "ps")

    # Convertit les ps en png (il est néscessaire de passer par un ps
    # car ghostscript refuse certain pdf)
    try_command("nice -n 5 gs -sDEVICE=%s -r30 -dBATCH -dNOPAUSE -dSAFER -dPARANOIDSAFER -dGraphicsAlphaBits=4 -dTextAlphaBits=4 -dMaxBitmap=50000000 -sOutputFile=%s%%d -q %s" %
                (gs_device, nom_png, escaped_path_ps_file),
                nom_rep,
                error_msg % "png")

    # Récupère la liste des fichiers
    list_filepng=os.listdir(nom_rep)
    # Calcule le nombre de pixel
    remplissage = [0] * (nb_composante + 1) # couleurs (N ou CMJN), nombre de pages
    for fichier in list_filepng:
        resultats = commands.getoutput("nice -n 5 /usr/scripts/impression/%s %s/%s" % (percent_program, nom_rep, fichier))
        l_resultats = resultats.split(":")
        for i in range(nb_composante):
            remplissage[i] += float(l_resultats[i])*float(l_resultats[nb_composante])
        remplissage[nb_composante] += float(l_resultats[nb_composante])

    # suppression des fichiers temporaires
    shutil.rmtree(nom_rep)

    total_noir = remplissage[nb_composante-1]
    total_couleur = sum(remplissage[0:nb_composante-1])
    faces = int(remplissage[nb_composante])

    if total_couleur > 0:
       c_total = ((COUT_PASSAGE_TAMBOUR_COULEUR + COUT_PASSAGE_TAMBOUR_NOIR) * faces  # passage dans les toners
                  + COUT_UNITE_NOIRE * total_noir                                     # cout encre noire
                  + COUT_UNITE_COULEUR * total_couleur)                               # cout encre couleur
    else: # Pas de couleur, malgre l'indication
        c_total = (COUT_PASSAGE_TAMBOUR_NOIR * faces  # passage dans les toners
                   + COUT_UNITE_NOIRE * total_noir)   # cout encre noire

    # Début : Ajout log pour réestimer les coûts
    fichier_log_est=open(FICHIER_LOG,"a")
    fichier_log_est.write("%d %d %3d %10.3f %10.3f %s\n" % (time.time(), color, faces, total_noir, total_couleur, path_pdf_file) )
    fichier_log_est.close()
    # Fin 

    return (float(c_total)/100, faces)

# ########################################################### #
#                       PRIX COULEURS                         #
# ########################################################### #
#
# Clacul le prix d'une impression couleur
#
def base_prix_couleurs(path_pdf_file):
    """
    Clacul le prix d'une impression couleur   sur papier A4 pour le fichier path_pdf_file
    """
    return base_prix(path_pdf_file, color=True)

# ########################################################### #
#                          PRIX N&B                           #
# ########################################################### #
#
# calcul le prix d'une impression en noir et blanc
#
def base_prix_nb(path_pdf_file):
    """
    Clacul le prix d'une impression noire et blanc sur papier A4 pour le fichier path_pdf_file
    """
    return base_prix(path_pdf_file, color=False)
