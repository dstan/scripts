# Sourcé par python.sh en environnement de test, à modifier suivant vos envies
# (et les tests que vous souhaitez faire)

echo "Dépôt custom. PYTHONPATH: $CPATH. Environnement de test en chargement."

# Pour ldap et la base postgres: il est possible de forwarder les connexions
# vers une base distante (celle de vo) pour éviter d'avoir à en configurer
# une locale. Les exemples donnés (en commentaire) ci-dessous permettent
# de se connecter avec le forward ssh suivant:
# $ ssh vo.crans.org -L 3899:localhost:389 \
#                    -L 5432:localhost:5432 \
#                    -L 5433:upload.v4.crans.org:5432

# Utiliser base ldap de test sur vo ?
export DBG_LDAP=vo.adm.crans.org
# En cas de tunnel, il faudra utiliser localhost
# export DBG_LDAP=localhost:3899

# Utiliser l'annuaire pgsql local
#export DBG_ANNUAIRE=1
export DBG_ANNUAIRE=localhost
# ne pas indiquer de port ici (pas supporté)
# ou 1 pour localhost

# Utiliser quel serveur (pg) pour les quota upload ?
export DBG_UPLOAD=localhost:5433

# Trigger est-il en mode débug ?
export DBG_TRIGGER=1

# Mails auto, plusieurs valeurs:
# * print: affiche le mail au lieu de l'envoyer
# * une adresse mail: envoie tous les mails à cette adresse mail au lieu de
#   la vraie
# NB: noter que pour le moment, cela ne marche pas avec tous les scripts.
# Attention à ne pas envoyer de mails aux adhérents par erreurs !
export DBG_MAIL=`whoami`+test@crans.org
export DBG_MAIL=print

# clogger produit des fichiers de logs
export DBG_CLOGGER_PATH=/tmp/`whoami`/clogs
mkdir -p $DBG_CLOGGER_PATH

# Serveur freeradius de test ?
export DBG_FREERADIUS=1

# Imprime pour de vrai ? 1 = Non. Tout autre valeur doit correspondre au
# nom d'une imprimante accessible avec la commande lp
export DBG_PRINTER=1

# Un dossier où trouver une version alternative des secrets (fichier par
# fichier)
export DBG_SECRETS=/etc/crans/dbg_secrets/`whoami`/

# Pour la wifimap
export DBG_WIFIMAP_DB=$CPATH/var/wifi_xml

# Addresse vers l'intranet (version 2)
export DBG_INTRANET=http://localhost:8080

# BDD (PgSQL) à utiliser pour l'intranet en test (défaut: localhost sur vo)
#export DBG_DJANGO_DB=localhost
export DBG_DJANGO_DB="" # Laisser vide (mais défini) pour une base SQLite

# Comnpay, pour payer pour de faux sur l'app solde intranet
# utiliser cranspasswords comnpay-test pour avoir un login@mdp permettant
# d'accéder à l'interface (celui ci-dessous ne le permet psa)
export DBG_COMNPAY=DEMO@DEMO

# Utiliser le CAS (par défaut, oui)
#export DBG_CAS=1
export DBG_CAS=0

source $CPATH/run.sh
