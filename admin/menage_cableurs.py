#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-


# Copyright (C) Stéphane Glondu, Alexandre Bos, et autres
# Licence : GPLv2

__doc__ = u"""Ce script permet de faire le menages parmis les câbleurs qui ne
sont plus sur le campus, ie ceux qui ne sont plus à jour de cotisation.

Utilisation :
%(prog)s {lister|radier} [--debug <adresse>]

Les commandes sont :
  * lister              afficher la liste des câbleurs succeptibles d'être
                        radiés
  * radier              selectionner, parmis eux, les cableurs que l'on
                        souhaite radier
"""


import sys, os, re
import gestion.config

# Fonctions d'affichage
from gestion.affich_tools import coul, tableau, prompt, cprint

# Importation de la base de données
from lc_ldap import shortcuts
ldap = shortcuts.lc_ldap_admin()

def _controle_interactif_adherents(liste):
    """
    Demande ce qu'il faut faire à chaque fois
    """

    restant = len(liste)
    if restant == 0:
        return 0, 0

    cprint(u'\nRadiation des câbleurs fantômes' , 'cyan')
    cprint(u"Pour chaque entrée, il faut taper 'o' ou 'n' (défaut=n).")
    cprint(u"Une autre réponse entraîne l'interruption du processus.")
    cprint(u"Le format est [nb_restant] Nom, Prénom (aid).")
    cprint(u"")

    nb = 0
    for a in liste:
        ok = prompt(u'[%3d] %s, %s (%s) ?'
                    % (restant, a['nom'][0], a['prenom'][0], a['aid'][0]), 'n', '').lower()
        restant -= 1
        if ok == 'o':
            modifiable = ldap.search(u'aid=%s' % a['aid'][0], mode='rw')[0]
            try:
                with modifiable as adh:
                    adh['droits'].remove(u'Cableur')
                    adh.history_gen()
                    adh.save()
                cprint(u'Droits cableurs retirés', 'rouge')
            except:
                cprint(u'Adhérent %s locké, réessayer plus tard' % modifiable['nom'][0], 'rouge')
        elif ok != 'n':
            cprint(u'Arrêt du contrôle %s des membres actifs' % explicite, 'rouge')
            break

def candidats():
    todo_list1 = ldap.search(u'droits=cableur')
    todo_list  = []
    for adh in todo_list1:
        if not adh.paiement_ok():
            todo_list.append(adh)
    return todo_list

def lister():
    """
    Afficher les câbleurs fantômes potentiels.
    """
    todo_list = candidats()
    print "Liste des câbleur dont la cotisation n'est pas à jour."
    print
    for adh in todo_list:
        print unicode(adh['prenom'][0]) + u" " + unicode(adh['nom'][0])
    print
    print "total : " + str(len(todo_list))

def controle_interactif():
    """
    Procédure interactive de radiations des câbleurs fantômes.
    """
    todo_list = candidats()

    # Zou !
    _controle_interactif_adherents(todo_list)

def __usage(message=None):
    """ Comment ça marche ? """

    cprint(__doc__ % { 'prog': sys.argv[0] })
    if message:
        cprint(message)
    sys.exit(1)


if __name__ == '__main__' :

    if len(sys.argv) <= 1:
        __usage()

    elif sys.argv[1] == 'lister':
        if len(sys.argv) != 2:
            __usage(u'Mauvaise utilisation de lister')
        lister()

    elif sys.argv[1] == 'radier':
        if len(sys.argv) != 2:
            __usage(u'Mauvaise utilisation de radier')
        controle_interactif()
    else:
        __usage(u'Commande inconnue : %s' % sys.argv[1])

    sys.exit(0)


# pydoc n'aime pas l'unicode :-(
