#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

"""
Script de déconnexion pour mail invalide

Copyright (C) 2009 Michel Blockelet
  inspiré de fiche_deconnexion.py par :
  Xavier Pessoles, Étienne Chové, Vincent Bernat, Nicolas Salles
Licence : GPL v2
"""

import os, sys, time
import subprocess
from lc_ldap import shortcuts
from gestion.config import upload
# logging tools
import syslog
def log(x):
    if type(x) == unicode:
        x = x.encode("utf-8")
    syslog.openlog('GENERATE_MAIL_INVALIDE_NOTICE')
    syslog.syslog(x)
    syslog.closelog()

sys.path.insert(0, '/usr/scripts/cranslib')
import utils.exceptions

import locale
locale.setlocale(locale.LC_TIME, 'fr_FR.UTF-8')

# On blackliste 14 jours après que le script ait été éxécuté
DELAY = 14

help = """Script de déconnexion pour mail invalide.
Une fiche sera générée pour chaque adhérent.
Chaque adhérent sera déconnecté 2 semaines plus tard si son adresse mail
n'a pas changé.

Usage: mail_invalide.py [adresse mail]...

L'adresse mail peut aussi être un aid précédé d'un tiret.
Exemple:
  mail_invalide.py toto@example.com -42
va s'occuper de l'adhérent ayant toto@example.com comme adresse mail et
l'adhérent ayant l'aid 42."""



def generate_ps(proprio, mail):
    """On génère la feuille d'avertissement et on retourne son emplacement."""
    barcode = "/usr/scripts/admin/mail_invalide/barcode.eps"
    name = unicode(proprio['prenom'][0]) + u" " + unicode(proprio['nom'][0])
    try:
        log(u'Generate invalid mail notice for %s' % name)
        # Dossier de génération du ps
        dossier = '/usr/scripts/var/mails_invalides'

        # Base pour le nom du fichier
        fichier = time.strftime('%Y-%m-%d-%H-%M') + '-mail-%s' % (name.
            lower().replace(' ', '-'))

        # Création du fichier tex
        format_date = '%A %d %B %Y'
        with open('%s/mail_invalide.tex' % os.path.dirname(__file__), 'r') as tempfile:
            template = tempfile.read()
        template = template.replace('~prenom~', proprio['prenom'][0].encode('utf-8'))
        template = template.replace('~nom~', proprio['nom'][0].encode('utf-8'))
        template = template.replace('~chambre~', proprio['chbre'][0].encode('utf-8'))
        template = template.replace('~mail~', mail.encode('utf-8').replace('_', '\\_'))
        template = template.replace('~fin~',
            time.strftime(format_date, time.localtime(time.time()+14*86400)))

        with open('%s/%s.tex' % (dossier, fichier), 'w') as outtex:
            outtex.write(template)

        # Compilation du fichier latex
        subprocess.check_call(['/usr/bin/pdflatex',
                               '-output-directory='+ dossier,
                               '-interaction', 'nonstopmode',
                               fichier + '.tex',
                              ])
        return '%s/%s.pdf' % (dossier, fichier)

    except Exception, e:
        log('Erreur lors de la génération du ps : ')
        log(str(e))
        log("Values : adherent:%s" % name)
        log(utils.exceptions.formatExc())
        raise

def set_mail_invalide(adherent, mail, a_verifier, a_imprimer):
    name = unicode(adherent['prenom'][0]) + u" " + unicode(adherent['nom'][0])
    if adherent['chbre'][0] in ['????', 'EXT']:
        print u"Chambre de %s : %s, générer la fiche ? [Yn]" % (name, adherent['chbre'][0])
        read = ''
        while read not in ['y', 'n']:
            read = raw_input().lower()
        if read == 'n':
            print u"Chambre de %s : %s, impossible de générer la fiche." % (name, adherent['chbre'][0])
            a_verifier.append(mail)
            return

    print u"Génération de la fiche pour %s :" % name
    fiche = generate_ps(adherent, mail)
    print fiche
    a_imprimer.append(fiche)
    with adherent as adh:
        adh.blacklist('mail_invalide','Mail Invalide - Script',debut=int(time.time()) + DELAY * 24 * 3600)
        adh.history_gen()
        adh.save()

if __name__ == "__main__":
    if '--help' in sys.argv or '-h' in sys.argv or len(sys.argv) < 2:
        print help
        sys.exit(0)

    ldap = shortcuts.lc_ldap_admin()

    # On fait la liste des .forwards dans les homes
    print " * Lecture des .forward ..."
    forwards = {}
    for uid in os.listdir('/home'):
        # Certains homes ne sont pas accessibles
        try:
            files = os.listdir('/home/%s' % uid)
        except OSError as e:
            #print "Home non-accessible : %s" % e
            pass
        else:
            if ".forward" in files and uid != "lost+found":
                # Même si le home n'est pas inaccessible, le .forward peut l'être
                try:
                    forwards.update({
                        open('/home/%s/.forward' % uid, 'r').readline().strip():
                        ldap.search(u'uid=%s' % uid)[0]['uid'][0]
                        }
                        )
                except IOError as e:
                    #print ".forward non-accessible : %s" % e
                    pass
                except IndexError as e:
                    print "Home %s existant mais pas d'adhérent ldap : %s" % (uid, e)

    a_imprimer = []
    a_verifier = []

    for adresse in sys.argv[1:]:
        # Est-ce un aid ?
        if adresse[0] == '-':
            print " * Recherche de aid=%s ..." % adresse[1:]
            res = ldap.search(u"aid=%s" % adresse[1:], mode='rw')
            if len(res) == 0:
                print "*** Erreur : aucun résultat pour aid=%s" % adresse[1:]
                a_verifier.append(adresse)
            elif len(res) > 1:
                print "*** Erreur : plusieurs résultats pour aid=%s :" % adresse[1:]
                for adh in res:
                    print unicode(adh['prenom'][0]) + u" " + unicode(adh['nom'][0])
                a_verifier.append(adresse)
            else:
                adherent = res[0]
                if adherent['uid'][0] in forwards.itervalues():
                    adresse = forwards.keys()[forwards.value().index(adherent[0]['uid'][0])]
                    set_mail_invalide(
                            adherent,
                            forward[adherent['uid'][0]],
                            a_verifier,
                            a_imprimer
                            )
                else:
                    set_mail_invalide(
                            adherent,
                            adherent['mail'][0],
                            a_verifier,
                            a_imprimer
                            )
            continue

        print " * Recherche de %s ..." % adresse
        # Est-ce un .forward ?

        if adresse in forwards.iterkeys():
            res = ldap.search(u"uid=%s" % forwards[adresse], mode='rw')
            if len(res) == 0:
                print "*** Erreur : aucun résultat pour uid=%s" % forwards[adresse]
                a_verifier.append(adresse)
            else:
                adherent = res[0]
                set_mail_invalide(adherent, adresse, a_verifier, a_imprimer)
                continue

        # Est-ce une adresse mail sans compte Cr@ns ?
        res = ldap.search(u"(|(mail=%s)(mailExt=%s))" % (adresse,adresse), mode='rw')
        if len(res) == 0:
            print "*** Erreur : aucun résultat pour %s" % adresse
            a_verifier.append(adresse)
        elif len(res) > 1:
            print "*** Erreur : plusieurs résultats pour %s :" % adresse
            for adh in res:
                print unicode(adh['prenom'][0]) + u" " + unicode(adh['nom'][0])
            a_verifier.append(adresse)
        else:
            adherent = res[0]
            set_mail_invalide(adherent, adresse, a_verifier, a_imprimer)

    if len(a_verifier) + len(a_imprimer) > 0:
        print ''
        print '***** Résultats *****'
        if len(a_verifier) > 0:
            print ' * Adresses mail à vérifier :'
            print ','.join(a_verifier)
        if len(a_imprimer) > 0:
            print ' * Fiches à imprimer :'
            for fiche in a_imprimer:
                print fiche
