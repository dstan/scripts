#! /bin/sh
# index-news : Cree les index des messages de news
# par Benoit

# Repertoire des news
Rep_News=/var/spool/news/articles/crans/

# Repertoire d'indexage
Rep_Ind=/var/www/search_news/index/crans.

# Repertoire d'id-xage
Rep_Id=/var/www/search_news/mid/crans.

# Indexage des articles
for forum in $(find $Rep_News -type d -mindepth 1 -printf '%P \n') ; do
  if (($(du -S $Rep_News$forum --max-depth=0 | awk '{print $1}') )) ; then
    index=($Rep_Ind$(echo $forum | tr "/" "."))
    nice -10 /usr/bin/index++ -v0 -i $index -r -mFrom -mSubject -mMessage-ID -e 'mail:*' $Rep_News$forum

    # Indexage des Id
    id=($Rep_Id$(echo $forum | tr "/" "."))
    if test -f $id ; then
      number=$(tail -n 1 $id | awk '{print $1}')
    else
      number="nexistepas"
    fi
    fichier=$Rep_News$forum/$number
    if test -f $fichier ; then
      for article in $(find $Rep_News$forum -type f -maxdepth 1 -newer $fichier -printf '%P \n') ; do
        mid=$(awk '/^Message-ID: / {print $2}' $Rep_News$forum/$article)
        ref=$(awk 'BEGIN{RS="";FS=":"} /References: / {i=1;while($i !~ /References$/) i++;print $(i+1)}' $Rep_News$forum/$article | grep "^ <" | tr -d '\n')
        $(echo $article $mid $ref >> $id )
      done
    else # si le dernier message avant le dernier id-xage est annule ou modere
         # ou si le fichier des mid n'existe pas :
         # on est bourrin : on re-indexe l'ensemble
      $(echo -n > $id)
      for article in $(find $Rep_News$forum -type f -maxdepth 1 -printf '%P \n') ; do
        mid=$(awk '/^Message-ID: / {print $2}' $Rep_News$forum/$article)
        ref=$(awk 'BEGIN{RS="";FS=":"} /References: / {i=1;while($i !~ /References$/) i++;print $(i+1)}' $Rep_News$forum/$article | grep "^ <" | tr -d '\n')
        $(echo $article $mid $ref >> $id )
      done
    fi
  fi
done
