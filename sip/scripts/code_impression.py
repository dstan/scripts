#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
import sys

import lc_ldap.shortcuts
from lc_ldap.attributs import imprimeur, nounou
from impression import digicode

conn = lc_ldap.shortcuts.lc_ldap_readonly()

try:
    aid=int(sys.argv[1][1:])

    adh = conn.search(u"aid=%s" % aid)[0]
    login = str(adh.get('uid',['NONE'])[0])
    codes = digicode.get_codes(login)
    if codes:
        sys.stdout.write(codes[0])
    else:
        if imprimeur in adh['droits'] or nounou in adh['droits']:
            code = digicode.gen_code(login)
            sys.stdout.write(str(code))
        else:
            sys.stdout.write('NONE')

except ValueError, IndexError:
    sys.stdout.write('NONE')
